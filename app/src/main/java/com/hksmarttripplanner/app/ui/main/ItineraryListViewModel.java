package com.hksmarttripplanner.app.ui.main;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.HttpException;

public class ItineraryListViewModel extends BaseViewModel {
    private final MutableLiveData<Itinerary> selectedItinerary = new MutableLiveData<>();

    public ItineraryListViewModel(@NonNull Application application) {
        super(application);
    }

    public void makeItinerarySelection(Itinerary itinerary) {
        selectedItinerary.setValue(itinerary);
    }

    public LiveData<Itinerary> getSelectedItinerary() {
        return selectedItinerary;
    }

    public Single<List<Itinerary>> getMyItineraries() {
        return getAppRepository().getUserItineraries(getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {
                }).doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Completable deleteItienraryById(int id) {
        return getAppRepository().deleteItineraryById(id);
    }

    public Single<Boolean> checkIsExistSharedItinerary() {
        return getAppRepository().checkIsExistSharedItinerary(getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {
                }).doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ItineraryListViewModel(application);
        }
    }
}
