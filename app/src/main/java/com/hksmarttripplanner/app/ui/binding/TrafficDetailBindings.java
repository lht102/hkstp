package com.hksmarttripplanner.app.ui.binding;

import android.databinding.BindingAdapter;
import android.os.Build;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.Line;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;
import com.hksmarttripplanner.app.data.source.remote.api.response.TransitDetail;

public class TrafficDetailBindings {
    @BindingAdapter("maneuver")
    public static void maneuver(ImageView imageView, String maneuverText) {
        if (maneuverText != null) {
            switch (maneuverText) {
                case "turn-sharp-left":
                    imageView.setImageResource(R.drawable.ic_turn_sharp_left);
                    break;
                case "uturn-right":
                    imageView.setImageResource(R.drawable.ic_uturn_right);
                    break;
                case "turn-slight-right":
                    imageView.setImageResource(R.drawable.ic_turn_slight_right);
                    break;
                case "merge":
                    imageView.setImageResource(R.drawable.ic_merge);
                    break;
                case "roundabout-left":
                    imageView.setImageResource(R.drawable.ic_roundabout_left);
                    break;
                case "roundabout-right":
                    imageView.setImageResource(R.drawable.ic_roundabout_right);
                    break;
                case "uturn-left":
                    imageView.setImageResource(R.drawable.ic_uturn_left);
                    break;
                case "turn-slight-left":
                    imageView.setImageResource(R.drawable.ic_turn_sharp_left);
                    break;
                case "turn-left":
                    imageView.setImageResource(R.drawable.ic_turn_left);
                    break;
                case "ramp-right":
                    imageView.setImageResource(R.drawable.ic_ramp_right);
                    break;
                case "turn-right":
                    imageView.setImageResource(R.drawable.ic_turn_right);
                    break;
                case "fork-right":
                    imageView.setImageResource(R.drawable.ic_fork_right);
                    break;
                case "straight":
                    imageView.setImageResource(R.drawable.ic_straight);
                    break;
                case "fork-left":
                    imageView.setImageResource(R.drawable.ic_fork_left);
                    break;
                case "ferry-train":
                    imageView.setImageResource(R.drawable.ic_ferry_train);
                    break;
                case "turn-sharp-right":
                    imageView.setImageResource(R.drawable.ic_turn_sharp_right);
                    break;
                case "ramp-left":
                    imageView.setImageResource(R.drawable.ic_ramp_left);
                    break;
                case "ferry":
                    imageView.setImageResource(R.drawable.ic_ferry);
                    break;
                case "keep-left":
                    imageView.setImageResource(R.drawable.ic_turn_left);
                    break;
                case "keep-right":
                    imageView.setImageResource(R.drawable.ic_turn_right);
                    break;
            }
        }
    }

    @BindingAdapter("distanceTime")
    public static void distanceTime(TextView textView, Step step) {
        String distanceText = step.getDistance().getText();
        String durationText = step.getDuration().getText();

        String str = distanceText + " (" + durationText + ")";
        textView.setText(str);
    }

    @BindingAdapter("trafficDescription")
    public static void trafficDescription(TextView textView, Step step) {
        if (step != null) {
            String str = step.getHtmlInstructions();
            TransitDetail transitDetail = step.getTransitDetail();
            if (transitDetail != null) {
                Line line = transitDetail.getLine();
                str += "\n" + transitDetail.getHeadway()
                        + " " + line.getName()
                        + " (" + transitDetail.getNumStop() + (transitDetail.getNumStop() > 0 ? "stops" : "stop") + ")";
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView.setText(Html.fromHtml(str, Html.FROM_HTML_MODE_COMPACT));
            } else {
                textView.setText(Html.fromHtml(str));
            }
        }
    }

    @BindingAdapter("travelModeIcon")
    public static void travelModeIcon(TextView textView, Place place) {
        String travelMode = place.getTravelMode();
        if ("driving".equalsIgnoreCase(travelMode)) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_drive_eta_black_24dp, 0, 0, 0);
        } else if ("walking".equalsIgnoreCase(travelMode)) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_directions_walk_black_24dp, 0, 0, 0);
        } else if ("bicycling".equalsIgnoreCase(travelMode)) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_directions_bike_black_24dp, 0, 0, 0);
        } else if ("transit".equalsIgnoreCase(travelMode)) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_directions_transit_black_24dp, 0, 0, 0);
        }
    }
}

