package com.hksmarttripplanner.app.ui.profile;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import io.reactivex.Single;

public class ProfileViewModel extends BaseViewModel {
    public ProfileViewModel(@NonNull Application application) {
        super(application);
    }

    public Single<User> getMyInfo() {
        return getAppRepository().getMyUserInfo();
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ProfileViewModel(application);
        }
    }
}
