package com.hksmarttripplanner.app.ui.traffic_detail;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import org.joda.time.DateTime;

import io.reactivex.Completable;
import io.reactivex.Single;

public class TrafficDetailViewModel extends BaseViewModel {

    public TrafficDetailViewModel(@NonNull Application application) {
        super(application);
    }

    public Completable updateTravelMode(int activityId, String travelMode) {
        return getAppRepository().updateActivityTravelMode(activityId, travelMode);
    }

    public Single<DetailRoute> getRoute(int actIdA, int actIdB, DateTime departureTime) {
        return getAppRepository().getTrafficDetail(actIdA, actIdB, departureTime);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new TrafficDetailViewModel(application);
        }
    }
}
