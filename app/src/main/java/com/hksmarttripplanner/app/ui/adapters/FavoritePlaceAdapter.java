package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.databinding.ItemFavoritePlaceBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class FavoritePlaceAdapter extends RecyclerView.Adapter<FavoritePlaceAdapter.FavouritePlaceViewHolder> {
    private final PublishSubject<Place> onClickItem = PublishSubject.create();
    private final PublishSubject<Place> onAddClickItem = PublishSubject.create();
    private List<Place> itemlist;

    public FavoritePlaceAdapter(List<Place> itemlist) {
        this.itemlist = itemlist;
    }

    @NonNull
    @Override
    public FavoritePlaceAdapter.FavouritePlaceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemFavoritePlaceBinding binding = ItemFavoritePlaceBinding.inflate(layoutInflater, viewGroup, false);
        return new FavoritePlaceAdapter.FavouritePlaceViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoritePlaceAdapter.FavouritePlaceViewHolder favouritePlaceViewHolder, int i) {
        Place place = itemlist.get(i);
        favouritePlaceViewHolder.bind(place);

        favouritePlaceViewHolder.binding.cardView.setOnClickListener(v -> {
            onClickItem.onNext(place);
        });

        favouritePlaceViewHolder.binding.btnAddToItinerary.setOnClickListener(v -> {
            onAddClickItem.onNext(place);
        });
    }

    @Override
    public int getItemCount() {
        return itemlist == null ? 0 : itemlist.size();
    }

    public PublishSubject<Place> getOnAddClickPos() {
        return onAddClickItem;
    }

    public PublishSubject<Place> getOnClickItem() {
        return onClickItem;
    }

    public void setItemlist(List<Place> newItemList) {
        if (newItemList == null) {
            int oldSize = itemlist.size();
            itemlist.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new FavoritePlaceAdapter.FavouritePlaceDiffCallBack(itemlist, newItemList));
            itemlist.clear();
            itemlist.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    public void clear() {
        itemlist.clear();
        notifyDataSetChanged();
    }

    public static class FavouritePlaceViewHolder extends RecyclerView.ViewHolder {
        private final ItemFavoritePlaceBinding binding;

        public FavouritePlaceViewHolder(ItemFavoritePlaceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Place place) {
            binding.setPlace(place);
            binding.executePendingBindings();
        }
    }

    private class FavouritePlaceDiffCallBack extends DiffUtil.Callback {
        private List<Place> oldList;
        private List<Place> newList;

        public FavouritePlaceDiffCallBack(List<Place> oldList, List<Place> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            Place oldItem = oldList.get(oldItemPos);
            Place newItem = newList.get(newItemPos);
            return Objects.equals(oldItem, newItem);
        }
    }
}
