package com.hksmarttripplanner.app.ui.binding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.TextView;

import com.hksmarttripplanner.app.data.source.local.model.Budget;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.utils.MyDateUtils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.DecimalFormat;

public class ItineraryBindings {

    @BindingAdapter("itineraryDescription")
    public static void setDescription(TextView textView, Itinerary itinerary) {
        DecimalFormat df = new DecimalFormat("#.##");
        long dateDiff = Days.daysBetween(itinerary.getStartDate().toLocalDate(), itinerary.getEndDate().toLocalDate()).getDays();
        Budget budget = itinerary.getBudget();

        String des = MyDateUtils.getFormattedDate(itinerary.getStartDate()) + " to " + MyDateUtils.getFormattedDate(itinerary.getEndDate())
                + "\nBudget: $" + df.format(budget.getAmount()) + " " + budget.getCurrency() + " (total " + (dateDiff + 1) + " day" + (dateDiff > 1 ? "s" : "")
                + ")";

        textView.setText(des);
    }

    @BindingAdapter("showDate")
    public static void setDateStr(TextView textView, DateTime dateTime) {
        textView.setText(MyDateUtils.getFormattedDate(dateTime));
    }
}
