package com.hksmarttripplanner.app.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.data.source.remote.api.response.Route;
import com.hksmarttripplanner.app.databinding.ItemItineraryDayPlaceBinding;
import com.hksmarttripplanner.app.databinding.ItemPlaceActivityHeaderBinding;
import com.hksmarttripplanner.app.ui.traffic_detail.TrafficDetailFragment;
import com.hksmarttripplanner.app.utils.DetailRouteUtils;
import com.hksmarttripplanner.app.utils.ItemTouchHelperAdapter;
import com.hksmarttripplanner.app.utils.ItemTouchHelperViewHolder;
import com.hksmarttripplanner.app.utils.MyDateUtils;
import com.hksmarttripplanner.app.utils.OnDragListener;
import com.hksmarttripplanner.app.utils.PlaceUtils;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import io.reactivex.subjects.PublishSubject;

public class PlaceActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemTouchHelperAdapter {

    public interface OnActivityItemClickListener {
        void onActBudgetClick(Place place, int i);
        void onStartTimeClick(ItineraryDay itineraryDay);
        void onDurationClick(Place place, int i);
        void onActivityDelClick(Place place, int i);
        void onActivityDetailClick(Place place, int i);
    }

    private static final String TAG = PlaceActivityAdapter.class.getSimpleName();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_NORMAL = 1;

    private final SwappableItemListener mSwappableItemListener;
    private final OnDragListener mOnDragListener;

    private OnActivityItemClickListener mOnActivityItemClickListener;
    private final PublishSubject<Integer> updateTrafficPos = PublishSubject.create();
    private Context context;
    private boolean editMode = false;
    private ItineraryDay itineraryDay;

    public PlaceActivityAdapter(OnDragListener onDragListener, SwappableItemListener activityListener) {
        mOnDragListener = onDragListener;
        mSwappableItemListener = activityListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_NORMAL;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        if (i == TYPE_HEADER) {
            ItemPlaceActivityHeaderBinding binding = ItemPlaceActivityHeaderBinding.inflate(layoutInflater, viewGroup, false);
            return new PlaceActivityHeaderViewHolder(binding);
        } else {
            ItemItineraryDayPlaceBinding binding = ItemItineraryDayPlaceBinding.inflate(layoutInflater, viewGroup, false);
            return new PlaceActivityViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof PlaceActivityViewHolder) {
            List<Place> itemList = itineraryDay.getPlaces();
            int j = i - 1;
            Place place = itemList.get(j);
            PlaceActivityViewHolder placeActivityViewHolder = (PlaceActivityViewHolder) viewHolder;
            placeActivityViewHolder.bind(place);

            String travelMode = place.getTravelMode();
            if ("driving".equalsIgnoreCase(travelMode)) {
                placeActivityViewHolder.binding.btnTrafficDetail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_drive_eta_black_24dp, 0, 0, 0);
            } else if ("walking".equalsIgnoreCase(travelMode)) {
                placeActivityViewHolder.binding.btnTrafficDetail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_directions_walk_black_24dp, 0, 0, 0);
            } else if ("bicycling".equalsIgnoreCase(travelMode)) {
                placeActivityViewHolder.binding.btnTrafficDetail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_directions_bike_black_24dp, 0, 0, 0);
            } else if ("transit".equalsIgnoreCase(travelMode)) {
                placeActivityViewHolder.binding.btnTrafficDetail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_directions_transit_black_24dp, 0, 0, 0);
            }
            if (DetailRouteUtils.isOnlyWalking(place.getDetailRoute())) {
                placeActivityViewHolder.binding.btnTrafficDetail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_directions_walk_black_24dp, 0, 0, 0);
            }

            ItemItineraryDayPlaceBinding binding = placeActivityViewHolder.binding;
            Log.d(TAG, "onBindViewHolder: called, i = " + j +
                    "\n, " + "place route is " + (place.getDetailRoute() == null ? "null" : "exist") +
                    "\n, " + "itemList size: " + getItemCount());

            DetailRoute detailRoute = place.getDetailRoute();
            if (detailRoute == null && j < itemList.size() - 1) {
                binding.btnTrafficDetail.setText("");
                setLoadingTrafficDetail(placeActivityViewHolder, true);
                placeActivityViewHolder.binding.btnTrafficDetail.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
            } else if (j != itemList.size() - 1 && DetailRouteUtils.isRouteExist(detailRoute)) {
                DecimalFormat df = new DecimalFormat("#.##");
                List<Route> routes = detailRoute.getRoutes();
                Route route = routes.get(0);
                setLoadingTrafficDetail(placeActivityViewHolder, false);
                String trafficDescription = df.format(detailRoute.getFee()) + "HKD " + route.getLegs().get(0).getDistance().getText() + " " +
                        route.getLegs().get(0).getDuration().getText();
                binding.btnTrafficDetail.setText(trafficDescription);
                binding.btnTrafficDetail.setOnClickListener(v -> {
                    FragmentManager supportFragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                    supportFragmentManager.beginTransaction()
                            .add(R.id.fragmentContainer, TrafficDetailFragment.newInstance(place, itemList.get(j + 1)), TAG)
                            .addToBackStack(TAG)
                            .commit();
                });
            } else if (j == itemList.size() - 1) {
                setLoadingTrafficDetail(placeActivityViewHolder, false);
                place.setDetailRoute(null);
            } else if (detailRoute != null) {
                binding.btnTrafficDetail.setText("No Available Route");
                setLoadingTrafficDetail(placeActivityViewHolder, false);
                binding.btnTrafficDetail.setOnClickListener(v -> {
                    FragmentManager supportFragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                    supportFragmentManager.beginTransaction()
                            .add(R.id.fragmentContainer, TrafficDetailFragment.newInstance(place, itemList.get(j + 1)), TAG)
                            .addToBackStack(TAG)
                            .commit();
                });
            }

            Log.d(TAG, "onBindViewHolder: startTime" + place.getStartTime().toString());
            if (PlaceUtils.checkWithinOpeningTime(place, place.getStartTime())) {
                binding.ivLateTime.setVisibility(View.INVISIBLE);
            } else {
                binding.ivLateTime.setVisibility(View.VISIBLE);
            }

            updateTimeLine(placeActivityViewHolder, j);

            binding.ivDrag.setOnTouchListener((v, event) -> {
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    mOnDragListener.onStartDrag(placeActivityViewHolder);
                }
                return false;
            });

            binding.cvItineraryDayPlace.setOnClickListener(v -> {
                mOnActivityItemClickListener.onActivityDetailClick(place, j);
            });

            if (editMode) {
                setEditModeBorder(binding.tvPlaceBudget, true);
                binding.tvPlaceBudget.setOnClickListener(v -> {
                    mOnActivityItemClickListener.onActBudgetClick(place, j);
                });
                setEditModeBorder(binding.tvDuration, true);
                binding.tvDuration.setOnClickListener(v -> {
                    mOnActivityItemClickListener.onDurationClick(place, j);
                });
            } else {
                setEditModeBorder(binding.tvPlaceBudget, false);
                setEditModeBorder(binding.tvDuration, false);
                binding.tvPlaceBudget.setOnClickListener(null);
                binding.tvDuration.setOnClickListener(null);
            }
        } else if (viewHolder instanceof PlaceActivityHeaderViewHolder) {
            PlaceActivityHeaderViewHolder placeActivityHeaderViewHolder = (PlaceActivityHeaderViewHolder) viewHolder;
            placeActivityHeaderViewHolder.bind(itineraryDay);
            ItemPlaceActivityHeaderBinding binding = placeActivityHeaderViewHolder.binding;

            Log.d(TAG, "onBindViewHolder: bind itineraryDay");
            if (editMode) {
                setEditModeBorder(binding.tvDayShortDes, true);
                binding.tvDayShortDes.setOnClickListener(v -> {
                    Log.d(TAG, "onBindViewHolder: itineraryDay Click");
                    mOnActivityItemClickListener.onStartTimeClick(itineraryDay);
                });
            } else {
                setEditModeBorder(binding.tvDayShortDes, false);
                binding.tvDayShortDes.setOnClickListener(null);
            }
        }
    }

    @Override
    public int getItemCount() {
        return itineraryDay == null || itineraryDay.getPlaces() == null ? 1 : itineraryDay.getPlaces().size() + 1;
    }

    @Override
    public void onItemDismiss(int position) {
        Log.d(TAG, "onItemDismiss: position = " + position);
        List<Place> itemList = itineraryDay.getPlaces();
        position--;
        mOnActivityItemClickListener.onActivityDelClick(itemList.get(position), position);
        itemList.remove(position);
        for (int i = position - 1; i < itemList.size() - 1; i++) {
            handleNotifyTrafficUpdate(i, false);
        }
        position++;
        notifyItemRemoved(position);
        notifyItemRangeChanged(position - 1, itemList.size() + 1);
        updateAllStartEndTimeSetWithHeader();
    }

    @Override
    public boolean onItemMove(RecyclerView.ViewHolder from, RecyclerView.ViewHolder to, int fromPosition, int toPosition) {
        Log.d(TAG, "onItemMove: moving " + fromPosition + ", " + toPosition);
        List<Place> itemList = itineraryDay.getPlaces();
        fromPosition--;
        toPosition--;
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(itemList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(itemList, i, i - 1);
            }
        }
        updateTimeLine((PlaceActivityViewHolder) from, toPosition);
        updateTimeLine((PlaceActivityViewHolder) to, fromPosition);

        fromPosition++;
        toPosition++;
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void clearView(RecyclerView.ViewHolder viewHolder, int selectedPos, int endPos) {
        Log.d(TAG, "clearView: " + selectedPos + ", " + endPos);
        if (endPos == -1) return;
        selectedPos--;
        endPos--;
        if (selectedPos != endPos) {
            List<Place> places = itineraryDay.getPlaces();
            if (endPos == 0) {
                mSwappableItemListener.changeOrderNextId(places.get(endPos).getActivityId(), places.get(endPos + 1).getActivityId());
            } else if (endPos == itineraryDay.getPlaces().size() - 1) {
                mSwappableItemListener.changeOrderPrevId(places.get(endPos).getActivityId(), places.get(endPos - 1).getActivityId());
            } else {
                mSwappableItemListener.changeOrder(places.get(endPos).getActivityId(), places.get(endPos - 1).getActivityId(), places.get(endPos + 1).getActivityId());
            }
        }
        if (selectedPos > endPos) {
            for (int i = 0; i < itineraryDay.getPlaces().size(); i++) {
                if (i == endPos - 1 || i == endPos || i == selectedPos) {
                    handleNotifyTrafficUpdate(i, true);
                } else {
                    handleNotifyTrafficUpdate(i, false);
                }
            }
        } else if (selectedPos < endPos) {
            for (int i = 0; i < itineraryDay.getPlaces().size(); i++) {
                if (i == selectedPos - 1 || i == endPos - 1 || i == endPos) {
                    handleNotifyTrafficUpdate(i, true);
                } else {
                    handleNotifyTrafficUpdate(i, false);
                }
            }
        }
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setItineraryDay(ItineraryDay itineraryDay) {
        this.itineraryDay = itineraryDay;
        updateAllStartEndTimeSetWithHeader();
        updateAllTrafficDetail();
    }

    public boolean getEditMode() {
        return editMode;
    }

    public void setEditMode(boolean flag) {
        editMode = flag;
        notifyDataSetChanged();
    }

    public void setOnActivityItemClickListener(OnActivityItemClickListener onActivityItemClickListener) {
        mOnActivityItemClickListener = onActivityItemClickListener;
    }

    public PublishSubject<Integer> getUpdateTrafficPos() {
        return updateTrafficPos;
    }

    private void setEditModeBorder(View view, boolean flag) {
        if (flag) {
            GradientDrawable border = new GradientDrawable();
            border.setStroke(2, Color.DKGRAY);
            view.setBackground(border);
        } else {
            view.setBackground(null);
        }
    }

    private void updateTimeLine(@NonNull PlaceActivityViewHolder placeActivityViewHolder, int i) {
        if (i == 0) {
            placeActivityViewHolder.binding.itemLine.setBackgroundResource(R.drawable.line_bg_top);
            placeActivityViewHolder.binding.btnTrafficDetail.setVisibility(View.VISIBLE);
        } else if (i == itineraryDay.getPlaces().size() - 1) {
            placeActivityViewHolder.binding.itemLine.setBackgroundResource(R.drawable.line_bg_bottom);
            placeActivityViewHolder.binding.btnTrafficDetail.setVisibility(View.GONE);
        } else {
            placeActivityViewHolder.binding.itemLine.setBackgroundResource(R.drawable.line_bg_middle);
            placeActivityViewHolder.binding.btnTrafficDetail.setVisibility(View.VISIBLE);
        }
    }

    private void updateAllTrafficDetail() {
        List<Place> places = itineraryDay.getPlaces();
        for (int i = 0; i < places.size() - 1; i++) {
            Place place = places.get(i);
            if (!DetailRouteUtils.isRouteExist(place.getDetailRoute())) {
                updateTrafficPos.onNext(i);
            } else {
                notifyPlaceItemChanged(i);
            }
        }
    }

    public void clear() {
        List<Place> itemList = itineraryDay.getPlaces();
        itemList.clear();
        notifyDataSetChanged();
    }

    private void handleNotifyTrafficUpdate(int i, boolean flag) {
        List<Place> itemList = itineraryDay.getPlaces();
        if (i < itemList.size() - 1 && i > -1) {
            try {
                if (flag || DetailRouteUtils.isTransitModeExist(itemList.get(i).getDetailRoute())) {
                    Log.d(TAG, "handleNotifyTrafficUpdate: true");
                    itemList.get(i).setDetailRoute(null);
                    notifyPlaceItemChanged(i);
                    updateTrafficPos.onNext(i);
                }
            } catch (NullPointerException e) {
                itemList.get(i).setDetailRoute(null);
                notifyPlaceItemChanged(i);
                updateTrafficPos.onNext(i);
            }
        }
    }
    
    public void updateTrafficDetail(DetailRoute detailRoute, int i) {
        List<Place> itemList = itineraryDay.getPlaces();
        Place place = itemList.get(i);
        place.setDetailRoute(detailRoute);
        notifyPlaceItemChanged(i);
        updateAllStartEndTimeSetWithHeader();
    }

    public void notifyTrafficDetailUpdateAfterTravelModeUpdate(int i, DetailRoute detailRoute, String travelMode) {
        List<Place> places = itineraryDay.getPlaces();
        places.get(i).setTravelMode(travelMode);
        updateTrafficDetail(detailRoute, i);
        for (int j = i; j < places.size() - 1; j++) {
            handleNotifyTrafficUpdate(j, false);
        }
        updateAllStartEndTimeSetWithHeader();
    }

    private void updateAllStartEndTimeSetWithHeader() {
        DateTime initStartTime = itineraryDay.getDate().withTime(itineraryDay.getStartTime());
        DateTime nextStartTime = initStartTime;
        List<Place> itemList = itineraryDay.getPlaces();
        if (itemList.size() > 0) {
            Place firstPlace = itemList.get(0);
            firstPlace.setStartTime(initStartTime);
        }
        for (int i = 0; i < itemList.size() - 1; i++) {
            Place place = itemList.get(i);
            Place nextPlace = itemList.get(i + 1);
            DetailRoute detailRoute = place.getDetailRoute();
            nextStartTime = nextStartTime.plusMinutes(place.getDuration());
            nextStartTime = nextStartTime.plusSeconds((int) DetailRouteUtils.getDetailRouteDuration(detailRoute));
            nextStartTime = MyDateUtils.roundMinute(nextStartTime);
            nextPlace.setStartTime(nextStartTime);
        }
        notifyDataSetChanged();
    }

    private void notifyPlaceItemChanged(int i) {
        notifyItemChanged(i + 1);
    }

    private void notifyPlaceItemRangeChanged(int posStart, int count) {
        notifyItemRangeChanged(posStart + 1, count);
    }

    public Place getPlaceByPos(int pos) {
        return itineraryDay.getPlaces().get(pos);
    }

    public int findPosByActivityId(int activityId) {
        List<Place> places = itineraryDay.getPlaces();
        for (int i = 0; i < places.size(); i++) {
            if (activityId == places.get(i).getActivityId()) {
                return i;
            }
        }
        return -1;
    }

    private void setLoadingTrafficDetail(PlaceActivityViewHolder viewHolder, boolean flag) {
        viewHolder.binding.btnTrafficDetail.setVisibility(flag ? View.GONE : View.VISIBLE);
        viewHolder.binding.pbTrafficDetail.setVisibility(flag ? View.VISIBLE : View.GONE);
    }

    public void updatePlaceDurationByPos(int min, int i) {
        List<Place> places = itineraryDay.getPlaces();
        places.get(i).setDuration(min);
        for (int j = i; j < places.size() - 1; j++) {
            handleNotifyTrafficUpdate(j, false);
        }
        updateAllStartEndTimeSetWithHeader();
    }

    public void updatePlaceBudgetByPos(double budget, int i) {
        List<Place> places = itineraryDay.getPlaces();
        Place place = places.get(i);
        place.getActualBudget().setAmount(budget);
        notifyItemChanged(0);
        notifyPlaceItemChanged(i);
    }

    public void updateItineraryDayStartTime(LocalTime localTime) {
        itineraryDay.setStartTime(localTime);
        for (int j = 0; j < itineraryDay.getPlaces().size() - 1; j++) {
            handleNotifyTrafficUpdate(j, false);
        }
        updateAllStartEndTimeSetWithHeader();
    }

    public void addNewAddedPlace(Place place) {
        List<Place> places = itineraryDay.getPlaces();
        places.add(place);
        handleNotifyTrafficUpdate(places.size() - 2, true);
        notifyItemChanged(0);
        notifyPlaceItemRangeChanged(places.size() - 2, 2);
        updateAllStartEndTimeSetWithHeader();
    }

    public static class PlaceActivityViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        private final ItemItineraryDayPlaceBinding binding;

        PlaceActivityViewHolder(ItemItineraryDayPlaceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Place place) {
            binding.setPlace(place);
            binding.executePendingBindings();
        }

        @Override
        public void onItemSelected() {
            binding.cvItineraryDayPlace.setCardBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            binding.cvItineraryDayPlace.setCardBackgroundColor(Color.WHITE);
        }
    }

    public static class PlaceActivityHeaderViewHolder extends RecyclerView.ViewHolder {
        private final ItemPlaceActivityHeaderBinding binding;

        PlaceActivityHeaderViewHolder(ItemPlaceActivityHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ItineraryDay itineraryDay) {
            binding.setItineraryDay(itineraryDay);
            binding.executePendingBindings();
        }
    }
}
