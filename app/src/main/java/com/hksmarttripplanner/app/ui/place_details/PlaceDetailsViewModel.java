package com.hksmarttripplanner.app.ui.place_details;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.Comment;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class PlaceDetailsViewModel extends BaseViewModel {

    public PlaceDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public Completable addToFavorite( int placeId) {
        return getAppRepository().addToFavorite(getAppRepository().getMyUserId(), placeId);
    }

    public Completable removeFromFavorite(int placeId) {
        return getAppRepository().removeFromFavorite(getAppRepository().getMyUserId(), placeId);
    }

    public Single<Boolean> isFavorite(int placeId) {
        return getAppRepository().isFavorite(getAppRepository().getMyUserId(), placeId);
    }

    public Completable writeComment(int placeId, String comment) {
        return getAppRepository().writeComment(getAppRepository().getMyUserId(), placeId, comment);
    }

    public Single<List<Comment>> getCommentByPlaceId(int placeId) {
        return getAppRepository().getCommentByPlaceId(placeId);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new PlaceDetailsViewModel(application);
        }
    }
}
