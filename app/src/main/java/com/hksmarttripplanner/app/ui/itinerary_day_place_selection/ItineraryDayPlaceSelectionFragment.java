package com.hksmarttripplanner.app.ui.itinerary_day_place_selection;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentItineraryDayPlaceSelectionBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItineraryDayPlaceSelectionFragment extends BaseFragment {
    private static final String TAG = ItineraryDayPlaceSelectionFragment.class.getSimpleName();
    private static final String ARG_ITINERARY_DAY_ID = "ITINERARY_DAY_ID";

    private FragmentItineraryDayPlaceSelectionBinding binding;
    private ItineraryDayPlaceSelectionViewModel viewModel;
//    private SimpleItineraryDayAdapter adapter = new SimpleItineraryDayAdapter(new ArrayList<>());

    public static ItineraryDayPlaceSelectionFragment newInstance(int itineraryDayId) {
        Bundle args = new Bundle();
        ItineraryDayPlaceSelectionFragment fragment = new ItineraryDayPlaceSelectionFragment();
        args.putInt(ARG_ITINERARY_DAY_ID, itineraryDayId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_itinerary_day_place_selection, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ItineraryDayPlaceSelectionViewModel.Factory factory = new ItineraryDayPlaceSelectionViewModel.Factory(getActivity().getApplication(), getArguments().getInt(ARG_ITINERARY_DAY_ID));
        viewModel = ViewModelProviders.of(this, factory).get(ItineraryDayPlaceSelectionViewModel.class);
        binding.setViewModel(viewModel);

        setupSwipeRefreshLayout();
        setupRecyclerView();
        setupToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();

//        getCompositeDisposable().add(adapter.getOnAddClickPos()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(itinerary -> {
//                    getActivity().getSupportFragmentManager()
//                            .beginTransaction()
//                            .add(R.id.fragmentContainer, )
//                            .addToBackStack(TAG)
//                            .commit();
//                }));
    }

    public void setupSwipeRefreshLayout() {
        binding.srlItineraryDayPlaceSelection.setOnRefreshListener(() -> {
//            adapter.clear();
//            getCompositeDisposable().add(viewModel.getItineraryDays()
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe((itineraryDays, throwable) -> {
//                        adapter.setSteps(itineraryDays);
//                        binding.srlItineraryDayPlaceSelection.setRefreshing(false);
//                    }));
        });
    }

    public void setupRecyclerView() {
        binding.rvSimpleItineraryDayPlaces.setLayoutManager(new LinearLayoutManager(getContext()));
//        binding.rvSimpleItineraryDayPlaces.setAdapter(adapter);
    }

    public void setupToolbar() {
        binding.tbItineraryDayPlaceSelection.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }
}
