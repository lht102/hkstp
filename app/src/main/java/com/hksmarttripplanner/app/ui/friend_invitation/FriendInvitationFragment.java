package com.hksmarttripplanner.app.ui.friend_invitation;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentFriendInvitationBinding;
import com.hksmarttripplanner.app.ui.adapters.UserInviteAdapter;
import com.hksmarttripplanner.app.ui.adapters.UserReceiveAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.friend_information.FriendInformationFragment;

import org.checkerframework.checker.units.qual.A;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FriendInvitationFragment extends BaseFragment {
    public static final String TAG = FriendInvitationFragment.class.getSimpleName();
    private FriendInvitationViewModel friendInvitationViewModel;
    private FragmentFriendInvitationBinding fragmentFriendInvitationBinding;
    private UserInviteAdapter userInviteAdapter;
    private UserReceiveAdapter userReceiveAdapter;

    public static FriendInvitationFragment newInstance() {
        Bundle bundle = new Bundle();
        FriendInvitationFragment friendInvitationFragment = new FriendInvitationFragment();
        friendInvitationFragment.setArguments(bundle);
        return friendInvitationFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFriendInvitationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_invitation, container, false);
        userInviteAdapter = new UserInviteAdapter(new ArrayList());
        userReceiveAdapter = new UserReceiveAdapter(new ArrayList());
        return fragmentFriendInvitationBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FriendInvitationViewModel.Factory factory = new FriendInvitationViewModel.Factory(getActivity().getApplication());
        friendInvitationViewModel = ViewModelProviders.of(getActivity(), factory).get(FriendInvitationViewModel.class);
        fragmentFriendInvitationBinding.setFriendInvitationViewModel(friendInvitationViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(friendInvitationViewModel
                .getSentInviteFriendList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((sentUsers) -> {
                    if (!sentUsers.isEmpty()) {
                        getCompositeDisposable().add(friendInvitationViewModel
                                .getFirmInviteFriendList().subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe((firmUsers) -> {
                                    List<Integer> sent = new LinkedList();
                                    for (int i = 0; i < sentUsers.size(); i++) {
                                        sent.add(sentUsers.get(i).getId());
                                    }
                                    List<Integer> firm = new LinkedList();
                                    for (int i = 0; i < firmUsers.size(); i++) {
                                        firm.add(firmUsers.get(i).getId());
                                    }
                                    int countSent = sent.size();
                                    int countFirm = firm.size();
                                    while (true) {
                                        int count = 0;
                                        for (int i = 0; i < countSent; i++) {
                                            for (int j = 0; j < countFirm; j++) {
                                                if (sentUsers.get(i).getId() == firmUsers.get(j).getId()) {
                                                    sentUsers.remove(i);
                                                    firmUsers.remove(j);
                                                    countSent--;
                                                    countFirm--;
                                                    count--;
                                                }
                                            }
                                            count++;
                                        }
                                        if (count == countSent) {
                                            break;
                                        }
                                    }
                                    userInviteAdapter.setItemlist(sentUsers);
                                }));
                    }
                }, Throwable::printStackTrace));

        getCompositeDisposable().add(friendInvitationViewModel
                .getFirmReceiveFriendList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((firmUsers) -> {
                    if (!firmUsers.isEmpty()) {
                        getCompositeDisposable().add(friendInvitationViewModel
                                .getSentReceiveFriendList().subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe((sentUsers) -> {
                                    List<Integer> sent = new LinkedList();
                                    for (int i = 0; i < sentUsers.size(); i++) {
                                        sent.add(sentUsers.get(i).getId());
                                    }
                                    List<Integer> firm = new LinkedList();
                                    for (int i = 0; i < firmUsers.size(); i++) {
                                        firm.add(firmUsers.get(i).getId());
                                    }
                                    int countSent = sent.size();
                                    int countFirm = firm.size();
                                    while (true) {
                                        int count = 0;
                                        for (int i = 0; i < countSent; i++) {
                                            for (int j = 0; j < countFirm; j++) {
                                                if (sentUsers.get(i).getId() == firmUsers.get(j).getId()) {
                                                    sentUsers.remove(i);
                                                    firmUsers.remove(j);
                                                    countSent--;
                                                    countFirm--;
                                                    count--;
                                                }
                                            }
                                            count++;
                                        }
                                        if (count == countSent) {
                                            break;
                                        }
                                    }
                                    userReceiveAdapter.setItemlist(firmUsers);
                                }));
                    }
                }, Throwable::printStackTrace));

        getCompositeDisposable().add(userInviteAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userInvite -> {
                    friendInvitationViewModel.makeUserSelection(userInvite);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, FriendInformationFragment
                                    .newInstance(userInvite), TAG).addToBackStack(TAG).commit();
                }));

        getCompositeDisposable().add(userReceiveAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userReceive -> {
                    friendInvitationViewModel.makeUserSelection(userReceive);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, FriendInformationFragment
                                    .newInstance(userReceive), TAG).addToBackStack(TAG).commit();
                }));

        getCompositeDisposable().add(userReceiveAdapter.getOnClickAccept()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userReceive -> {
                    getCompositeDisposable().add(friendInvitationViewModel.addOrReceiveFriend(
                            friendInvitationViewModel.getAppRepository().getMyUserId(), userReceive.getId())
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> { }));
                }));

        getCompositeDisposable().add(userReceiveAdapter.getOnClickReject()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userReceive -> {
                    getCompositeDisposable().add(friendInvitationViewModel.removeFriend(
                            userReceive.getId(), friendInvitationViewModel.getAppRepository().getMyUserId())
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {
                            }));
                }));

        getCompositeDisposable().add(userInviteAdapter.getOnClickCancel()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userInvite -> {
                    getCompositeDisposable().add(friendInvitationViewModel.removeFriend(
                            friendInvitationViewModel.getAppRepository().getMyUserId(), userInvite.getId())
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {
                            }));
                }));


        setToolbar();
        setRecyclerView();
    }
    public void setToolbar() {
        fragmentFriendInvitationBinding.tbFriendInvitation.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    public void setRecyclerView() {
        fragmentFriendInvitationBinding.rvFriendInviting.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentFriendInvitationBinding.rvFriendInviting.setAdapter(userInviteAdapter);
        fragmentFriendInvitationBinding.rvFriendReceiving.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentFriendInvitationBinding.rvFriendReceiving.setAdapter(userReceiveAdapter);
    }
}
