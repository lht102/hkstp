package com.hksmarttripplanner.app.ui.itinerary_day_place_list;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

public class ItineraryDayPlaceViewModel extends BaseViewModel {
    private static final String TAG = ItineraryDayPlaceViewModel.class.getSimpleName();
    private int  mItineraryId;
    private int mItineraryDayId;
    private BehaviorSubject<ItineraryDay> itineraryDaySubject;
    private BehaviorSubject<Itinerary> itinerarySubject;
    private ItineraryDay optItineraryDay;

    public ItineraryDayPlaceViewModel(@NonNull Application application) {
        super(application);
        itineraryDaySubject = BehaviorSubject.create();
        itinerarySubject = BehaviorSubject.create();
    }

    public void setItineraryId(int itineraryId) {
        mItineraryId = itineraryId;
        itinerarySubject.onNext(new Itinerary());
        setItineraryDayId(0);
    }

    public void setItineraryDayId(int itineraryId) {
        mItineraryDayId = itineraryId;
        itineraryDaySubject.onNext(new ItineraryDay());
        optItineraryDay = null;
    }

    public BehaviorSubject<ItineraryDay> getItineraryDaySubject() {
        return itineraryDaySubject;
    }

    public BehaviorSubject<Itinerary> getItinerarySubject() {
        return itinerarySubject;
    }

    public Single<ItineraryDay> getItineraryDayPlaceList() {
        return getAppRepository().getItineraryDayById(mItineraryDayId);
    }

    public Single<DetailRoute> getRouteInfo(int activityIdA, int activityIdB, DateTime departureTime) {
        return getAppRepository().getTrafficDetail(activityIdA, activityIdB, departureTime);
    }

    public Single<DetailRoute> getRouteInfoTemp(int activityIdA, int activityIdB, DateTime departureTime) {
        return getAppRepository().getTrafficDetailTemp(activityIdA, activityIdB, departureTime);
    }

    public Single<Place> createActivity(int placeId) {
        return getAppRepository().createActivity(mItineraryDayId, placeId);
    }

    public Completable deleteActivity(int activityId) {
        return getAppRepository().deleteActivityById(activityId);
    }

    public Completable updateActivityBudget(int activityId, double budget) {
        return getAppRepository().updateActivityBudget(activityId, budget);
    }

    public Completable updateActivityDuration(int activityId, int min) {
        return getAppRepository().updateActivityDuration(activityId, min);
    }

    public Completable updateItineraryDayStartTime(int itineraryDayId, String startTime) {
        return getAppRepository().updateItineraryDayStartTime(itineraryDayId, startTime);
    }

    public Completable updateActivityOrder(int activityId, int prevId, int nextId) {
        return getAppRepository().updateActivityOrder(activityId, prevId, nextId);
    }

    public Completable updateActivityOrder(int activityId, ArrayList<Integer> activityOrderList) {
        return getAppRepository().updateActivityOrder(activityId, activityOrderList);
    }

    public Completable updateActivityOrderPrevId(int activityId, int prevId) {
        return getAppRepository().updateActivityOrderPrevId(activityId, prevId);
    }

    public Completable updateActivityOrderNextId(int activityId, int nextId) {
        return getAppRepository().updateActivityOrderNextId(activityId, nextId);
    }

    public Single<ItineraryDay> getOptItineraryDay(int id) {
        return getAppRepository().getOptItineraryDayById(id)
                .doOnSuccess(optDay -> {
                    optItineraryDay = optDay;
                });
    }

    public Single<Itinerary> getItineraryCall() {
        return getAppRepository().getItineraryById(mItineraryId);
    }

    public Completable deleteItineraryDayById(int id) {
        return getAppRepository().deleteItineraryDayById(id);
    }

    public Single<ItineraryDay> createItineraryDay() {
        return getAppRepository().createItineraryDayByItineraryId(mItineraryId);
    }

    public Single<List<DetailRoute>> getItineraryDayDetailRoute(int itineraryDayId) {
        return getAppRepository().getItineraryDayDetailRoute(itineraryDayId);
    }

    public Single<Itinerary> optimizeItinerary() {
        return getAppRepository().optimizeItineraryById(mItineraryId);
    }

    public ItineraryDay getItineraryDay() {
        return itineraryDaySubject.getValue();
    }

    public void setItineraryDay(ItineraryDay itineraryDay) {
        this.itineraryDaySubject.onNext(itineraryDay);
    }

    public ItineraryDay getOptItineraryDay() {
        return optItineraryDay;
    }

    public void setOptItineraryDay(ItineraryDay optItineraryDay) {
        this.optItineraryDay = optItineraryDay;
    }

    public Itinerary getItinerary() {
        return itinerarySubject.getValue();
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerarySubject.onNext(itinerary);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ItineraryDayPlaceViewModel(application);
        }
    }
}
