package com.hksmarttripplanner.app.ui.friend_result;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentFriendResultBinding;
import com.hksmarttripplanner.app.ui.adapters.UserAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.friend_information.FriendInformationFragment;
import com.hksmarttripplanner.app.ui.friend_invitation.FriendInvitationFragment;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FriendResultFragment extends BaseFragment {
    private static final String TAG = FriendResultFragment.class.getSimpleName();
    private FriendResultViewModel friendResultViewModel;
    private FragmentFriendResultBinding fragmentFriendResultBinding;
    private UserAdapter userAdapter;

    public static FriendResultFragment newInstance(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        FriendResultFragment friendResultFragment = new FriendResultFragment();
        friendResultFragment.setArguments(bundle);
        return friendResultFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFriendResultBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_result, container, false);
        userAdapter = new UserAdapter(new ArrayList());
        return fragmentFriendResultBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FriendResultViewModel.Factory factory = new FriendResultViewModel.Factory(getActivity().getApplication());
        friendResultViewModel = ViewModelProviders.of(getActivity(), factory).get(FriendResultViewModel.class);
        fragmentFriendResultBinding.setFriendResultViewModel(friendResultViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(friendResultViewModel.getFriendResult(getArguments().getInt("id"))
        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe((users) -> {
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getId() == friendResultViewModel.getAppRepository().getMyUserId()) {
                    users.remove(i);
                }
            }
            userAdapter.setItemlist(users);
        }, throwable -> {
            throwable.printStackTrace();
                }));

        getCompositeDisposable().add(userAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    friendResultViewModel.makeUserSelection(user);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, FriendInformationFragment
                                    .newInstance(user), TAG).addToBackStack(TAG).commit();
                }));
        setRecyclerView();
        setToolbar();
    }

    public void setRecyclerView() {
        fragmentFriendResultBinding.rvFriendResult.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentFriendResultBinding.rvFriendResult.setAdapter(userAdapter);
    }

    public void setToolbar() {
        fragmentFriendResultBinding.tbFriendResult.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }
}
