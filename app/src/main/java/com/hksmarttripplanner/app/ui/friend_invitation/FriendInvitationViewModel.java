package com.hksmarttripplanner.app.ui.friend_invitation;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.data.source.local.model.UserInvite;
import com.hksmarttripplanner.app.data.source.local.model.UserReceive;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

import retrofit2.HttpException;

public class FriendInvitationViewModel extends BaseViewModel {
    private final MutableLiveData<UserInvite> selectInviteUser = new MutableLiveData();
    private final MutableLiveData<UserReceive> selectReceiveUser = new MutableLiveData();

    public FriendInvitationViewModel(@NonNull Application application) {
        super(application);
    }

    public void makeUserSelection(UserInvite userInvite) {
        selectInviteUser.setValue(userInvite);
    }

    public void makeUserSelection(UserReceive userReceive) {
        selectReceiveUser.setValue(userReceive);
    }


    public Single<List<UserInvite>> getSentInviteFriendList() {
        return getAppRepository().getSentFriendInviteListById(getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Single<List<UserInvite>> getFirmInviteFriendList() {
        return getAppRepository().getFirmFriendInviteListById(getAppRepository()
                .getMyUserId()).doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Single<List<UserReceive>> getSentReceiveFriendList() {
        return getAppRepository().getSentFriendReceiveListById(getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Single<List<UserReceive>> getFirmReceiveFriendList() {
        return getAppRepository().getFirmFriendReceiveListById(getAppRepository()
                .getMyUserId()).doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Completable addOrReceiveFriend(int userId, int friendId) {
        return getAppRepository().addOrReceiveFriend(userId, friendId);
    }

    public Completable removeFriend(int userId, int friendId) {
        return getAppRepository().removeFriend(userId, friendId);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new FriendInvitationViewModel(application);
        }
    }
}
