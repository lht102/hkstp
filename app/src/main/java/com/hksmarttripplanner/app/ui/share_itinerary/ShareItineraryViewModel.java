package com.hksmarttripplanner.app.ui.share_itinerary;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import retrofit2.HttpException;

public class ShareItineraryViewModel extends BaseViewModel {
    private final MutableLiveData<User> selectUser = new MutableLiveData();

    public ShareItineraryViewModel(@NonNull Application application) { super(application); }

    public void makeUserSelection(User user) { selectUser.setValue(user); }

    public Single<List<User>> getSentFriendList() {
        return getAppRepository().getSentFriendListById(getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Single<List<User>> getFirmFriendList() {
        return getAppRepository().getFirmFriendListById(getAppRepository()
                .getMyUserId()).doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Single<Itinerary> getItineraryById(int itineraryId) {
        return getAppRepository().getItineraryById(itineraryId)
                .doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Completable shareItinerary(int receiverId, int itineraryId) {
        return getAppRepository().shareItinerary(getAppRepository().getMyUserId(),
                receiverId, itineraryId).doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @android.support.annotation.NonNull
        @Override
        public <T extends ViewModel> T create(@android.support.annotation.NonNull Class<T> modelClass) {
            return (T) new ShareItineraryViewModel(application);
        }
    }
}
