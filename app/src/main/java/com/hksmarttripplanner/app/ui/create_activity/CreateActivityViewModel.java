package com.hksmarttripplanner.app.ui.create_activity;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.ui.base.BaseViewModel;

public class CreateActivityViewModel extends BaseViewModel {

    public CreateActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new CreateActivityViewModel(application);
        }
    }
}
