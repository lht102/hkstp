package com.hksmarttripplanner.app.ui.itinerary_selection;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.util.List;

import io.reactivex.Single;

public class ItinerarySelectionViewModel extends BaseViewModel {

    public ItinerarySelectionViewModel(@NonNull Application application) {
        super(application);
    }

    public Single<List<Itinerary>> getMyItineraries() {
        return getAppRepository().getUserItineraries(getAppRepository().getMyUserId());
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ItinerarySelectionViewModel(application);
        }
    }
}
