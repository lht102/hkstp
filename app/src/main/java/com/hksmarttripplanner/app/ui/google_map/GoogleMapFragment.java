package com.hksmarttripplanner.app.ui.google_map;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.databinding.FragmentGoogleMapBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GoogleMapFragment extends BaseFragment implements OnMapReadyCallback {
    public static final String TAG = GoogleMapFragment.class.getSimpleName();
    private int zoom = 16;
    private GoogleMapViewModel googleMapViewModel;
    private FragmentGoogleMapBinding fragmentGoogleMapBinding;
    private LatLng location;
    private GoogleMap googleMap;

    public static GoogleMapFragment newInstance(String name, double latitude, double longitude) {
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putDouble("latitude", latitude);
        bundle.putDouble("longitude", longitude);
        GoogleMapFragment googleMapFragment = new GoogleMapFragment();
        googleMapFragment.setArguments(bundle);
        return googleMapFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentGoogleMapBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_google_map, container, false);
        return fragmentGoogleMapBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        GoogleMapViewModel.Factory factory = new GoogleMapViewModel.Factory(getActivity().getApplication());
        googleMapViewModel = ViewModelProviders.of(getActivity(), factory).get(GoogleMapViewModel.class);
        fragmentGoogleMapBinding.setGoogleMapViewModel(googleMapViewModel);
        setBackPlaceDetails();
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(googleMapViewModel.getSnackbarText()
        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::showSnackBar, throwable ->
                Log.e(getTag(), "Unable to show the map.")));
        setGoogleMap();
    }

    private void setGoogleMap() {
        SupportMapFragment supportMapFragment = (SupportMapFragment)
                this.getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
    }

    public void onMapReady(GoogleMap mGoogleMap) {
        String name = getArguments().getString("name");
        double latitude = getArguments().getDouble("latitude");
        double longitude = getArguments().getDouble("longitude");

        googleMap = mGoogleMap;
        location = new LatLng(latitude, longitude);
        fragmentGoogleMapBinding.tbGoogleMap.setTitle(name);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        moveMap(location);
    }

    private void moveMap(LatLng place) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(place).zoom(zoom).build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void setBackPlaceDetails() {
        fragmentGoogleMapBinding.tbGoogleMap.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }
}
