package com.hksmarttripplanner.app.ui.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentLoginBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.main.MainActivity;
import com.hksmarttripplanner.app.ui.register.RegisterActivity;

import java.util.Date;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LoginFragment extends BaseFragment {
    private LoginViewModel mLoginViewModel;
    private FragmentLoginBinding mFragmentLoginBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        return mFragmentLoginBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LoginViewModel.Factory factory = new LoginViewModel.Factory(getActivity().getApplication());
        mLoginViewModel = ViewModelProviders.of(getActivity(), factory).get(LoginViewModel.class);
        mFragmentLoginBinding.setViewModel(mLoginViewModel);

        setupLoginBtn();
        setupRegisterBtn();
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(mLoginViewModel.getSnackbarText()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showSnackBar,
                        throwable -> Log.e(getTag(), "Unable to display SnackBar")));
    }

    private void setupLoginBtn() {
        mFragmentLoginBinding.btnLogin.setOnClickListener(view -> {
            String account = mFragmentLoginBinding.etAccount.getText().toString();
            String password = mFragmentLoginBinding.etPassword.getText().toString();

            getCompositeDisposable().add(mLoginViewModel.validate(account, password)
                    .flatMapSingle(aBoolean -> mLoginViewModel.login(account, password))
                    .flatMapSingle(loginResponse -> {
                        Date expireDate = loginResponse.getExpireDate();
                        long expireTime = expireDate.getTime();
                        String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();
                        mLoginViewModel.saveLoginInformaiton(account, password, token, expireTime);
                        return mLoginViewModel.getMyUserInfo();
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(user -> {
                        mLoginViewModel.saveUserId(user.getId());
                        mLoginViewModel.saveUserCurrency(user.getCurrency());

                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }, throwable -> {
                        Log.e(getTag(), throwable.getClass().getName());
                        throwable.printStackTrace();
                    }));
        });
    }

    private void setupRegisterBtn() {
        mFragmentLoginBinding.btnRegister.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), RegisterActivity.class);
            startActivity(intent);
        });
    }
}
