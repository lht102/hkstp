package com.hksmarttripplanner.app.ui.binding;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import com.hksmarttripplanner.app.data.source.local.model.Budget;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.utils.DetailRouteUtils;
import com.hksmarttripplanner.app.utils.MyDateUtils;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.DecimalFormat;
import java.util.List;

public class HeaderBinding {
    @BindingAdapter("shortDayDes")
    public static void showShortDayDes(TextView textView, ItineraryDay itineraryDay) {
        if (itineraryDay != null) {

            DateTime dateTime = itineraryDay.getDate();
            LocalTime startTime = itineraryDay.getStartTime();
            DateTimeFormatter fmt = DateTimeFormat.forPattern("EEEE");
            String str = "Start Time: " + startTime.toString("HH:mm") + " (" + fmt.print(dateTime) + ")";
            textView.setText(str);
        }
    }

    @BindingAdapter("headerDuration")
    public static void showHeaderDuration(TextView textView, ItineraryDay itineraryDay) {
        if (itineraryDay != null) {

            Duration duration = new Duration(0);
            List<Place> itemList = itineraryDay.getPlaces();
            for (int i = 0; i < itemList.size(); i++) {
                Place place = itemList.get(i);
                duration = duration.plus(Duration.standardMinutes(place.getDuration()));
                long sec = DetailRouteUtils.getDetailRouteDuration(place.getDetailRoute());
                if (sec % 60 >= 30) {
                    sec = sec - sec % 60 + 60;
                } else {
                    sec = sec - sec % 60;
                }
                duration = duration.plus(Duration.standardSeconds(sec));

            }
            PeriodFormatter formatter = new PeriodFormatterBuilder()
                    .appendHours()
                    .appendSuffix("hrs")
                    .appendMinutes()
                    .appendSuffix("mins")
                    .toFormatter();
            String str = "Duration: " + formatter.print(duration.toPeriod());
            textView.setText(str);
        }
    }

    @BindingAdapter("headerActBudget")
    public static void showHeaderBudget(TextView textView, ItineraryDay itineraryDay) {
        if (itineraryDay != null) {
            double val = 0;
            String curr = "";
            DecimalFormat df = new DecimalFormat("#.##");
            List<Place> itemList = itineraryDay.getPlaces();
            for (int i = 0; i < itemList.size(); i++) {
                Place place = itemList.get(i);
                Budget budget = place.getActualBudget();
                if (DetailRouteUtils.isRouteExist(place.getDetailRoute()) && place.getDetailRoute().getFee() > 0) {
                    val += place.getDetailRoute().getDisplayfee().getAmount();
                }
                if (place.getActualBudget() != null && place.getActualBudget().getCurrency() != null) {
                    curr = place.getActualBudget().getCurrency();
                }
                if (budget != null) {
                    val += budget.getAmount();
                }
            }
            String str = "Budget: $" + df.format(val) + " " + curr;
            textView.setText(str);
        }
    }

    @BindingAdapter("headerFeeDes")
    public static void showHeaderFee(TextView textView, ItineraryDay itineraryDay) {
        if (itineraryDay != null) {
            double val = 0;
            double hkdval = 0;
            String curr = "";
            DecimalFormat df = new DecimalFormat("#.##");
            List<Place> itemList = itineraryDay.getPlaces();
            for (int i = 0; i < itemList.size(); i++) {
                Place place = itemList.get(i);
                if (DetailRouteUtils.isRouteExist(place.getDetailRoute()) && place.getDetailRoute().getFee() > 0 && place.getDetailRoute().getDisplayfee() != null) {
                    curr = place.getDetailRoute().getDisplayfee().getCurrency();
                    hkdval += place.getDetailRoute().getFee();
                    val += place.getDetailRoute().getDisplayfee().getAmount();
                }
            }
            String str = "Total fee: $" + df.format(hkdval) + " HKD ($" + df.format(val) + " " + curr +  ")";
            textView.setText(str);
        }
    }
}
