package com.hksmarttripplanner.app.ui.itinerary_day_management;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.ui.adapters.ItineraryDayAdapter;
import com.hksmarttripplanner.app.ui.main.MainActivity;

import java.util.LinkedList;
import java.util.List;

public class ItineraryDayManagementActivity extends AppCompatActivity {

    private  List<ItineraryDay> itineraryDayList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itinerary_day_management);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // here for set the itinerary name
        getSupportActionBar().setTitle("Mong Kong Plan");
        toolbar.setNavigationOnClickListener(view -> {
            Intent i = new Intent(view.getContext(), MainActivity.class);
            startActivity(i);
            finish();
        });

        itineraryDayList = new LinkedList();

    }
}
