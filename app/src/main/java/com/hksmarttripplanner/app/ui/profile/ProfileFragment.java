package com.hksmarttripplanner.app.ui.profile;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentProfileBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ProfileFragment extends BaseFragment {
    public static final String TAG = ProfileFragment.class.getSimpleName();

    private FragmentProfileBinding binding;
    private ProfileViewModel viewModel;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ProfileViewModel.Factory factory = new ProfileViewModel.Factory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(this, factory).get(ProfileViewModel.class);
        binding.setViewModel(viewModel);

        setupToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(viewModel.getMyInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((user, throwable) -> {
                   binding.setUser(user);
                }));
    }

    public void setupToolbar() {
        binding.tbUserProfile.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }
}
