package com.hksmarttripplanner.app.ui.friend_information;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import io.reactivex.Completable;
import io.reactivex.Single;

public class FriendInformationViewModel extends BaseViewModel {
    public FriendInformationViewModel(@NonNull Application application) {
        super(application);
    }

    public Single<Boolean> isFriend(int friendId, int userId) {
        return getAppRepository().isFriend(friendId, userId);
    }

    public Completable addOrReceiveFriend(int userId, int friendId) {
        return getAppRepository().addOrReceiveFriend(userId, friendId);
    }

    public Completable removeFriend(int userId, int friendId) {
        return getAppRepository().removeFriend(userId, friendId);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new FriendInformationViewModel(application);
        }
    }
}
