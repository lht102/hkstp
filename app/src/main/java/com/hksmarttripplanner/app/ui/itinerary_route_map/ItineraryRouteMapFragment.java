package com.hksmarttripplanner.app.ui.itinerary_route_map;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.EncodedPolyline;
import com.hksmarttripplanner.app.data.source.remote.api.response.Leg;
import com.hksmarttripplanner.app.data.source.remote.api.response.Route;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;
import com.hksmarttripplanner.app.databinding.FragmentItineraryDayRouteMapBinding;
import com.hksmarttripplanner.app.databinding.FragmentItineraryRouteMapBinding;
import com.hksmarttripplanner.app.ui.adapters.ItineraryRouteMapAdapter;
import com.hksmarttripplanner.app.ui.adapters.PlaceRouteMapAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_list.ItineraryDayViewModel;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceViewModel;
import com.hksmarttripplanner.app.utils.DetailRouteUtils;
import com.hksmarttripplanner.app.utils.MyColorUtils;
import com.hksmarttripplanner.app.utils.MyDateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItineraryRouteMapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnPolylineClickListener {
    public static final String TAG = ItineraryRouteMapFragment.class.getSimpleName();
    private FragmentItineraryRouteMapBinding binding;
    private ItineraryDayPlaceViewModel viewModel;
    private GoogleMap map;
    private List<Polyline> polylines;
    private ItineraryRouteMapAdapter adapter;

    public static ItineraryRouteMapFragment newInstance() {
        Bundle args = new Bundle();
        ItineraryRouteMapFragment fragment = new ItineraryRouteMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_itinerary_route_map, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ItineraryDayPlaceViewModel.Factory factory = new ItineraryDayPlaceViewModel.Factory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(getActivity(), factory).get(ItineraryDayPlaceViewModel.class);
        binding.setViewModel(viewModel);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_route_map);
        mapFragment.getMapAsync(this);

        setupToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        polylines = new ArrayList<>();
        updatePolyline();

        googleMap.setOnPolylineClickListener(this);
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
    }

    public void setupToolbar() {
        binding.tbRouteMap.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }


    public void updatePolyline() {
        for (int i = 0; i < polylines.size(); i++) {
            Polyline polyline = polylines.get(i);
            polyline.remove();
        }
        polylines.clear();
        map.clear();
        Itinerary itinerary = viewModel.getItinerary();
        int placeIndex = 0;
        if (itinerary.getDays() != null) {
            LatLngBounds.Builder latLngBoundsBuilder = LatLngBounds.builder();
            for (int a = 0; a < itinerary.getDays().size(); a++) {
                ItineraryDay itineraryDay = itinerary.getDays().get(a);
                if (itineraryDay != null && itineraryDay.getPlaces() != null) {
                    IconGenerator iconFactory = new IconGenerator(getContext());
                    iconFactory.setColor(MyColorUtils.getMyColorList()[a % MyColorUtils.getMyColorList().length]);
                    iconFactory.setTextAppearance(R.style.amu_ClusterIcon_TextAppearance);
                    List<Place> places = itineraryDay.getPlaces();

                    for (int i = 0; i < places.size(); i++) {
                        placeIndex++;
                        Place place = places.get(i);
                        LatLng loc = new LatLng(place.getLatitude(), place.getLongitude());
                        latLngBoundsBuilder.include(loc);

                        Marker marker = map.addMarker(new MarkerOptions()
                                .position(loc)
                                .title(place.getName())
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(a * 36)));

                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(String.valueOf(placeIndex))));
                    }
                }
            }
            LatLngBounds latLngBounds = latLngBoundsBuilder.build();
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50));
        }
    }
}
