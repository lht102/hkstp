package com.hksmarttripplanner.app.ui.friend_list;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.Single;
import retrofit2.HttpException;

public class FriendListViewModel extends BaseViewModel {

    private final MutableLiveData<User> selectUser = new MutableLiveData();

    public FriendListViewModel(@NonNull Application application) {
        super(application);
    }

    public void makeUserSelection(User user) {
        selectUser.setValue(user);
    }

    public Single<List<User>> getSentFriendList() {
        return getAppRepository().getSentFriendListById(getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Single<List<User>> getFirmFriendList() {
        return getAppRepository().getFirmFriendListById(getAppRepository()
                .getMyUserId()).doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new FriendListViewModel(application);
        }
    }
}
