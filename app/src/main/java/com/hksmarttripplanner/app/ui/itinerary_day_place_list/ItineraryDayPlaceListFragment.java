package com.hksmarttripplanner.app.ui.itinerary_day_place_list;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.databinding.FragmentItineraryDayPlaceListBinding;
import com.hksmarttripplanner.app.ui.adapters.PlaceActivityAdapter;
import com.hksmarttripplanner.app.ui.adapters.SwappableItemListener;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.create_activity.CreateActivityFragment;
import com.hksmarttripplanner.app.ui.dialog.EditBudgetDialogFragment;
import com.hksmarttripplanner.app.ui.dialog.EditDurationDialogFragment;
import com.hksmarttripplanner.app.ui.dialog.EditStartTimeDialogFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_route_map.ItineraryDayRouteMapFragment;
import com.hksmarttripplanner.app.ui.place_details.PlaceDetailsFragment;
import com.hksmarttripplanner.app.utils.NotSwipeableItemTouchHelperCallback;
import com.hksmarttripplanner.app.utils.OnDragListener;
import com.hksmarttripplanner.app.utils.SimpleItemTouchHelperCallback;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItineraryDayPlaceListFragment extends BaseFragment implements OnDragListener, SwappableItemListener, PlaceActivityAdapter.OnActivityItemClickListener {
    public static final String TAG = ItineraryDayPlaceListFragment.class.getSimpleName();
    private static final String ARG_ITINERARY_DAY_ID = "ITINERARY_DAY_ID";
    private static final String ARG_ITINERARY_DAY_TITLE = "ITINERARY_DAY_TITLE";

    private FragmentItineraryDayPlaceListBinding mFragmentItineraryDayPlaceListBinding;
    private ItineraryDayPlaceViewModel mItineraryDayPlaceViewModel;
    private PlaceActivityAdapter placeActivityAdapter = new PlaceActivityAdapter(this, this);
    private PlaceActivityAdapter optPlaceActivityAdapter;
    private ItemTouchHelper optItemTouchHelper;
    private ItemTouchHelper mItemTouchHelper;
    private boolean initFlag = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: called");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public static ItineraryDayPlaceListFragment newInstance(int itineraryDayId, String title) {
        Log.d(TAG, "newInstance: called");
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ITINERARY_DAY_ID, itineraryDayId);
        bundle.putString(ARG_ITINERARY_DAY_TITLE, title);
        ItineraryDayPlaceListFragment fragment = new ItineraryDayPlaceListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: called");
        mFragmentItineraryDayPlaceListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_itinerary_day_place_list, container, false);
        return  mFragmentItineraryDayPlaceListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: called");
        ItineraryDayPlaceViewModel.Factory factory = new ItineraryDayPlaceViewModel.Factory(getActivity().getApplication());
        mItineraryDayPlaceViewModel = ViewModelProviders.of(getActivity(), factory).get(ItineraryDayPlaceViewModel.class);
        mItineraryDayPlaceViewModel.setItineraryDayId(getArguments().getInt(ARG_ITINERARY_DAY_ID));
        mFragmentItineraryDayPlaceListBinding.setViewModel(mItineraryDayPlaceViewModel);

        setupRecyclerView();
        setupOptRecyclerView();
        setupSwipeRefreshLayout();
        setupToolbar();
        setupFloatingActionButton();
        mFragmentItineraryDayPlaceListBinding.fabSaveOptDay.hide();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");

        if (initFlag) {
            getCompositeDisposable().add(mItineraryDayPlaceViewModel.getItineraryDayPlaceList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(itineraryDay -> {
                        placeActivityAdapter.setItineraryDay(itineraryDay);
                        mItineraryDayPlaceViewModel.setItineraryDay(itineraryDay);
                    }, throwable -> {
                        throwable.printStackTrace();
                    }));
            initFlag = false;
        } else {
            placeActivityAdapter.setItineraryDay(mItineraryDayPlaceViewModel.getItineraryDay());
        }

        getCompositeDisposable().add(placeActivityAdapter.getUpdateTrafficPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .concatMapSingle(i -> {
                    Log.d(TAG, "concatMap Start i = " + i);
                    Place currPlace = placeActivityAdapter.getPlaceByPos(i);
                    Place nextPlace = placeActivityAdapter.getPlaceByPos(i + 1);
                    ItineraryDay itineraryDay = mItineraryDayPlaceViewModel.getItineraryDay();
                    DateTime date = itineraryDay.getDate();
                    if (Days.daysBetween(LocalDate.now(), date.toLocalDate()).getDays() > 30) {
                        date = DateTime.now().plusDays(1);
                    }
                    LocalTime startTime = itineraryDay.getStartTime();
                    if (i > 0) {
                        startTime = currPlace.getStartTime().toLocalTime();
                    }
                    Log.d(TAG, "i = " + i + " nextStartTime: " + startTime.toString());
                    return mItineraryDayPlaceViewModel.getRouteInfo(currPlace.getActivityId(), nextPlace.getActivityId(), date.withTime(startTime))
                            .map(routes -> new Pair<DetailRoute, Integer>(routes, i))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .onErrorReturn(throwable -> new Pair<>(new DetailRoute(), i));
                })
                .subscribe(pair -> {
                    if (pair != null) {
                        DetailRoute routes = pair.first;
                        int i = pair.second;
                        placeActivityAdapter.updateTrafficDetail(routes, i);
                        mItineraryDayPlaceViewModel.setItineraryDay(mItineraryDayPlaceViewModel.getItineraryDay());
                        Log.d(TAG, "fragment: update traffic detail finished");

                    }
                }, throwable -> {
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(optPlaceActivityAdapter.getUpdateTrafficPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .concatMapSingle(i -> {
                    Log.d(TAG, "concatMap Start i = " + i);
                    Place currPlace = optPlaceActivityAdapter.getPlaceByPos(i);
                    Place nextPlace = optPlaceActivityAdapter.getPlaceByPos(i + 1);
                    ItineraryDay itineraryDay = mItineraryDayPlaceViewModel.getOptItineraryDay();
                    DateTime date = itineraryDay.getDate();
                    if (Days.daysBetween(LocalDate.now(), date.toLocalDate()).getDays() > 30) {
                        date = DateTime.now().plusDays(1);
                    }
                    LocalTime startTime = itineraryDay.getStartTime();
                    if (i > 0) {
                        startTime = currPlace.getStartTime().toLocalTime();
                    }
                    Log.d(TAG, "i = " + i + " nextStartTime: " + startTime.toString());
                    return mItineraryDayPlaceViewModel.getRouteInfoTemp(currPlace.getActivityId(), nextPlace.getActivityId(), date.withTime(startTime))
                            .map(routes -> new Pair<DetailRoute, Integer>(routes, i))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .onErrorReturn(throwable -> new Pair<>(new DetailRoute(), i));
                })
                .subscribe(pair -> {
                    if (pair != null) {
                        DetailRoute routes = pair.first;
                        int i = pair.second;
                        optPlaceActivityAdapter.updateTrafficDetail(routes, i);
                        Log.d(TAG, "fragment: update traffic detail finished");

                    }
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_itinerary_setting, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setupSwipeRefreshLayout() {
        mFragmentItineraryDayPlaceListBinding.srlItineraryDayPlaces.setOnRefreshListener(() -> {
            getCompositeDisposable().add(mItineraryDayPlaceViewModel.getItineraryDayPlaceList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((itineraryDay, throwable) -> {
                        mItineraryDayPlaceViewModel.setItineraryDay(itineraryDay);
                        placeActivityAdapter.setItineraryDay(itineraryDay);
                        mFragmentItineraryDayPlaceListBinding.srlItineraryDayPlaces.setRefreshing(false);
                    }));
        });
    }

    public void setupRecyclerView() {
        if (mItineraryDayPlaceViewModel.getItineraryDay() != null && !initFlag) {
            Log.d(TAG, "restore previous state");
            placeActivityAdapter.setItineraryDay(mItineraryDayPlaceViewModel.getItineraryDay());
        }
        placeActivityAdapter.setContext(getContext());
        mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlaces.setLayoutManager(new LinearLayoutManager(getContext()));
        mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlaces.setAdapter(placeActivityAdapter);
        placeActivityAdapter.setOnActivityItemClickListener(this);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(placeActivityAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlaces);
        mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlaces.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                FloatingActionButton fab = mFragmentItineraryDayPlaceListBinding.fabAddItineraryDayPlace;
                if (dy > 0 && fab.isShown()) {
                    fab.hide();
                }
                else if (dy < 0 && !fab.isShown()) {
                    fab.show();
                }
            }
        });
    }

    public void setupOptRecyclerView() {
        optPlaceActivityAdapter = new PlaceActivityAdapter(new OnDragListener() {
            @Override
            public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
                optItemTouchHelper.startDrag(viewHolder);
            }

            @Override
            public void onEndDrag(RecyclerView.ViewHolder viewHolder) {

            }
        }, new SwappableItemListener() {
            @Override
            public void changeOrder(int curr, int prevId, int nextId) {

            }

            @Override
            public void changeOrderPrevId(int curr, int prevId) {

            }

            @Override
            public void changeOrderNextId(int curr, int nextId) {

            }
        });
        optPlaceActivityAdapter.setOnActivityItemClickListener(new PlaceActivityAdapter.OnActivityItemClickListener() {
            @Override
            public void onActBudgetClick(Place place, int i) {

            }

            @Override
            public void onStartTimeClick(ItineraryDay itineraryDay) {

            }

            @Override
            public void onDurationClick(Place place, int i) {

            }

            @Override
            public void onActivityDelClick(Place place, int i) {

            }

            @Override
            public void onActivityDetailClick(Place place, int i) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentContainer, PlaceDetailsFragment.newInstance(place, ""), PlaceDetailsFragment.TAG)
                        .addToBackStack(TAG)
                        .commit();
            }
        });
        optPlaceActivityAdapter.setContext(getContext());
        mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlacesOpt.setLayoutManager(new LinearLayoutManager(getContext()));
        mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlacesOpt.setAdapter(optPlaceActivityAdapter);
        ItemTouchHelper.Callback callback = new NotSwipeableItemTouchHelperCallback(optPlaceActivityAdapter);
        optItemTouchHelper = new ItemTouchHelper(callback);
        optItemTouchHelper.attachToRecyclerView(mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlacesOpt);
    }

    public void setupToolbar() {
        mFragmentItineraryDayPlaceListBinding.tbItineraryDayPlaceList.setTitle(getArguments().getString(ARG_ITINERARY_DAY_TITLE));
        mFragmentItineraryDayPlaceListBinding.tbItineraryDayPlaceList.inflateMenu(R.menu.menu_itinerary_setting);
        mFragmentItineraryDayPlaceListBinding.tbItineraryDayPlaceList.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
        mFragmentItineraryDayPlaceListBinding.tbItineraryDayPlaceList.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_settings:
                    Log.d(TAG, "Edit Mode turn on");
                    if (placeActivityAdapter.getEditMode()) {
                        placeActivityAdapter.setEditMode(false);
                        menuItem.setIcon(R.drawable.ic_edit_black_24dp);
                    } else {
                        placeActivityAdapter.setEditMode(true);
                        menuItem.setIcon(R.drawable.ic_check_black_24dp);
                    }
                    return true;
                case R.id.action_optimize_day:
                    BottomSheetBehavior<ConstraintLayout> bottomSheetBehavior = BottomSheetBehavior.from(mFragmentItineraryDayPlaceListBinding.bottomSheetDayOpt);
                    if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN || bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        mFragmentItineraryDayPlaceListBinding.fabSaveOptDay.show();
                        mFragmentItineraryDayPlaceListBinding.fabAddItineraryDayPlace.hide();
                    } else {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        mFragmentItineraryDayPlaceListBinding.fabSaveOptDay.hide();
                    }

                    if (mItineraryDayPlaceViewModel.getOptItineraryDay() == null) {
                        getCompositeDisposable().add(mItineraryDayPlaceViewModel.getOptItineraryDay(getArguments().getInt(ARG_ITINERARY_DAY_ID))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(itineraryDay -> {
                                    optPlaceActivityAdapter.setItineraryDay(itineraryDay);
                                    showSnackBar(R.string.optimized_itinerary_day_created_msg);
                                    setupFabOptItineraryDay();
                                }, throwable -> {

                                }));
                    } else {
                        optPlaceActivityAdapter.setItineraryDay(mItineraryDayPlaceViewModel.getOptItineraryDay());
                        setupFabOptItineraryDay();
                    }
                    return true;
                case R.id.action_route_map:
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentContainer, ItineraryDayRouteMapFragment.newInstance(), ItineraryDayRouteMapFragment.TAG)
                            .addToBackStack(TAG)
                            .commit();
                    return true;
                default:
                    return true;
            }
        });
    }

    public void setupFloatingActionButton() {
        mFragmentItineraryDayPlaceListBinding.fabAddItineraryDayPlace.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainer, CreateActivityFragment.newInstance(getArguments().getInt(ARG_ITINERARY_DAY_ID)), CreateActivityFragment.TAG)
                    .addToBackStack(TAG)
                    .commit();
        });
    }

    public void setupFabOptItineraryDay() {
        BottomSheetBehavior<ConstraintLayout> bottomSheetBehavior = BottomSheetBehavior.from(mFragmentItineraryDayPlaceListBinding.bottomSheetDayOpt);
        mFragmentItineraryDayPlaceListBinding.rvItineraryDayPlacesOpt.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                FloatingActionButton fab = mFragmentItineraryDayPlaceListBinding.fabSaveOptDay;
                if (dy > 0 && fab.isShown() || bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN || bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    fab.hide();
                }
                else if (dy < 0 && !fab.isShown() && bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED || bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HALF_EXPANDED) {
                    fab.show();
                }
            }
        });
        mFragmentItineraryDayPlaceListBinding.fabSaveOptDay.setOnClickListener(v -> {
            ItineraryDay optItineraryDay = mItineraryDayPlaceViewModel.getOptItineraryDay();
            List<Place> places = optItineraryDay.getPlaces();
            ArrayList<Integer> orderList = new ArrayList<>();
            for (int i = 0; i < places.size(); i++) {
                orderList.add(places.get(i).getActivityId());
            }
            getCompositeDisposable().add(mItineraryDayPlaceViewModel.updateActivityOrder(optItineraryDay.getId(), orderList)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        placeActivityAdapter.setItineraryDay(optItineraryDay);
                        mItineraryDayPlaceViewModel.setItineraryDay(optItineraryDay);
                        showSnackBar(R.string.update_opt_itinerary_day_msg);
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        mFragmentItineraryDayPlaceListBinding.fabSaveOptDay.hide();
                        Log.d(TAG, "setupOptItineraryDay: updated success");
                    }));
        });
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onEndDrag(RecyclerView.ViewHolder viewHolder) {

    }

    public void addActivity(int placeId) {
        getCompositeDisposable().add(mItineraryDayPlaceViewModel.createActivity(placeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(place -> {
                    placeActivityAdapter.addNewAddedPlace(place);
                    showSnackBar(R.string.activity_created_msg);
                }, throwable -> {
                    showSnackBar(R.string.simple_error_msg);
                }));
    }

    public void updateTrafficDetailAfterTravelModeUpdateByActivityId(int activityId, DetailRoute detailRoute, String travelMode) {
        int i = placeActivityAdapter.findPosByActivityId(activityId);
        placeActivityAdapter.notifyTrafficDetailUpdateAfterTravelModeUpdate(i, detailRoute, travelMode);
    }

    @Override
    public void changeOrder(int curr, int prevId, int nextId) {
        Log.d(TAG, "changeOrder: " + curr + ", " + prevId + ", " + nextId);
        getCompositeDisposable().add(mItineraryDayPlaceViewModel.updateActivityOrder(curr, prevId, nextId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {

                }));
    }

    @Override
    public void changeOrderPrevId(int curr, int prevId) {
        Log.d(TAG, "changeOrder prev: " + curr + ", " + prevId);
        getCompositeDisposable().add(mItineraryDayPlaceViewModel.updateActivityOrderPrevId(curr, prevId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {

                }));
    }

    @Override
    public void changeOrderNextId(int curr, int nextId) {
        Log.d(TAG, "changeOrder next: " + curr + ", " + nextId);
        getCompositeDisposable().add(mItineraryDayPlaceViewModel.updateActivityOrderNextId(curr, nextId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {

                }));
    }

    @Override
    public void onActBudgetClick(Place place, int i) {
        EditBudgetDialogFragment dialog = new EditBudgetDialogFragment();
        dialog.setEditBudgetDialogListener(new EditBudgetDialogFragment.EditBudgetDialogListener() {
            @Override
            public void onDialogPositiveClick(DialogFragment dialogFragment, double budget) {
                placeActivityAdapter.updatePlaceBudgetByPos(budget, i);
                getCompositeDisposable().add(mItineraryDayPlaceViewModel.updateActivityBudget(place.getActivityId(), budget)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            Log.d(TAG, "updateBudget: true");
                        }));
            }

            @Override
            public void onDialogNegativeClick(DialogFragment dialogFragment) {
            }
        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    @Override
    public void onStartTimeClick(ItineraryDay itineraryDay) {
        EditStartTimeDialogFragment dialog = new EditStartTimeDialogFragment();
        dialog.setEditStartTimeDialogListener(new EditStartTimeDialogFragment.EditStartTimeDialogListener() {
            @Override
            public void onDialogPositiveClick(DialogFragment dialogFragment, LocalTime localTime) {
                placeActivityAdapter.updateItineraryDayStartTime(localTime);
                getCompositeDisposable().add(mItineraryDayPlaceViewModel.updateItineraryDayStartTime(itineraryDay.getId(), localTime.toString("HH:mm"))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            Log.d(TAG, "update startTime: true");
                        }));
            }

            @Override
            public void onDialogNegativeClick(DialogFragment dialogFragment) {

            }
        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    @Override
    public void onDurationClick(Place place, int i) {
        EditDurationDialogFragment dialog = new EditDurationDialogFragment();
        dialog.setEditDurationDialogListener(new EditDurationDialogFragment.EditDurationDialogListener() {
            @Override
            public void onDialogPositiveClick(DialogFragment dialogFragment, int duration) {
                placeActivityAdapter.updatePlaceDurationByPos(duration, i);
                getCompositeDisposable().add(mItineraryDayPlaceViewModel.updateActivityDuration(place.getActivityId(), duration)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            Log.d(TAG, "update duration: true");
                        }));
            }

            @Override
            public void onDialogNegativeClick(DialogFragment dialogFragment) {
            }
        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    @Override
    public void onActivityDelClick(Place place, int i) {
        getCompositeDisposable().add(mItineraryDayPlaceViewModel.deleteActivity(place.getActivityId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    showSnackBar(R.string.activity_delete_msg);
                })
        );
    }

    @Override
    public void onActivityDetailClick(Place place, int i) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, PlaceDetailsFragment.newInstance(place, ""), PlaceDetailsFragment.TAG)
                .addToBackStack(TAG)
                .commit();
    }
}
