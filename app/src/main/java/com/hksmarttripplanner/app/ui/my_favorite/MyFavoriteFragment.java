package com.hksmarttripplanner.app.ui.my_favorite;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ScrollingTabContainerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentMyFavoriteListBinding;
import com.hksmarttripplanner.app.ui.adapters.FavoritePlaceAdapter;
import com.hksmarttripplanner.app.ui.adapters.ItinerarySelectionAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceListFragment;
import com.hksmarttripplanner.app.ui.main.SearchPlaceFragment;
import com.hksmarttripplanner.app.ui.place_details.PlaceDetailsFragment;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MyFavoriteFragment extends BaseFragment {
    public static final String TAG = MyFavoriteFragment.class.getSimpleName();
    private static final String ARG_ADD_TO_ITINERARY_ID = "ADD_TO_ITINERARY_ID";
    private static final String ARG_ADD_TO_ITINERARY_DAY_ID = "ADD_TO_ITINERARY_DAY_ID";

    private FragmentMyFavoriteListBinding mFragmentMyFavoriteListBinding;
    private MyFavoriteViewModel mMyFavoriteViewModel;
    private FavoritePlaceAdapter favoritePlaceAdapter = new FavoritePlaceAdapter(new ArrayList<>());
    private ItinerarySelectionAdapter itinerarySelectionAdapter = new ItinerarySelectionAdapter();

    private BottomSheetBehavior bottomSheetBehavior;
    private int selectedPlaceId;

    public static MyFavoriteFragment newInstance() {
        MyFavoriteFragment fragment = new MyFavoriteFragment();
        return fragment;
    }

    public static MyFavoriteFragment newInstance(int itineraryId) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ADD_TO_ITINERARY_ID, itineraryId);
        MyFavoriteFragment fragment = new MyFavoriteFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static MyFavoriteFragment newInstance(int itineraryId, int itineraryDayId) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ADD_TO_ITINERARY_ID, itineraryId);
        bundle.putInt(ARG_ADD_TO_ITINERARY_DAY_ID, itineraryDayId);
        MyFavoriteFragment fragment = new MyFavoriteFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentMyFavoriteListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_favorite_list, container, false);
        return mFragmentMyFavoriteListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MyFavoriteViewModel.Factory factory = new MyFavoriteViewModel.Factory(getActivity().getApplication());
        mMyFavoriteViewModel = ViewModelProviders.of(this, factory).get(MyFavoriteViewModel.class);
        mFragmentMyFavoriteListBinding.setViewModel(mMyFavoriteViewModel);

        ConstraintLayout bottomSheet = mFragmentMyFavoriteListBinding.bottomSheetItinerarySelection;
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        setupSwipeRefreshLayout();
        setupRecyclerView();
        setupFloatingActionButton();
        setupItinerarySelectionRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(favoritePlaceAdapter.getOnAddClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(place -> {
                    selectedPlaceId = place.getId();
                    int itineraryId = 0;
                    int itineraryDayId = 0;

                    try {
                        itineraryId = getArguments().getInt(ARG_ADD_TO_ITINERARY_ID);
                        itineraryDayId = getArguments().getInt(ARG_ADD_TO_ITINERARY_DAY_ID);

                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                    }

                    if (itineraryDayId > 0) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        ItineraryDayPlaceListFragment fragment = (ItineraryDayPlaceListFragment) fm.findFragmentByTag(ItineraryDayPlaceListFragment.TAG);
                        fragment.addActivity(place.getId());
                        fm.popBackStack(ItineraryDayPlaceListFragment.TAG, 1);
                    } else  {

                        mMyFavoriteViewModel.getAppRepository().getUserItineraries(mMyFavoriteViewModel.getAppRepository().getMyUserId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(disposable -> {
                                    itinerarySelectionAdapter.setItemList(null);
                                    mFragmentMyFavoriteListBinding.pgSelectionLoading.setVisibility(View.VISIBLE);
                                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
                                })
                                .subscribe((itineraries, throwable) -> {
                                    mFragmentMyFavoriteListBinding.pgSelectionLoading.setVisibility(View.INVISIBLE);
                                    itinerarySelectionAdapter.setItemList(itineraries);
                                });
                    }
                }));

        getCompositeDisposable().add(itinerarySelectionAdapter.getOnClickItineraryDay()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMapSingle(itineraryDay ->  mMyFavoriteViewModel.getAppRepository().createActivity(itineraryDay.getId(), selectedPlaceId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()))
                .subscribe(place -> {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    showSnackBar(R.string.activity_created_msg);
                }));

        getCompositeDisposable().add(favoritePlaceAdapter.getOnClickItem()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(place -> {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentContainer, PlaceDetailsFragment.newInstance(place, ""), PlaceDetailsFragment.TAG)
                            .addToBackStack(TAG)
                            .commit();
                }));

        getCompositeDisposable().add(mMyFavoriteViewModel.getMyFavorites()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(places -> {
                    favoritePlaceAdapter.setItemlist(places);
                }));
    }

    public void setupSwipeRefreshLayout() {
        mFragmentMyFavoriteListBinding.srlMyFavourite.setOnRefreshListener(() -> {
            favoritePlaceAdapter.clear();
            getCompositeDisposable().add(mMyFavoriteViewModel.getMyFavorites()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((places, throwable) -> {
                        favoritePlaceAdapter.setItemlist(places);
                        mFragmentMyFavoriteListBinding.srlMyFavourite.setRefreshing(false);
                    }));
        });
    }

    public void setupRecyclerView() {
        mFragmentMyFavoriteListBinding.rvMyFavourite.setLayoutManager(new LinearLayoutManager(getContext()));
        mFragmentMyFavoriteListBinding.rvMyFavourite.setAdapter(favoritePlaceAdapter);
        mFragmentMyFavoriteListBinding.rvMyFavourite.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                FloatingActionButton fab = mFragmentMyFavoriteListBinding.fabAddNewMyFavourite;
                if (dy > 0 && fab.isShown()) {
                    fab.hide();
                }
                else if (dy < 0 && !fab.isShown()) {
                    fab.show();
                }
            }
        });
    }

    public void setupItinerarySelectionRecyclerView() {
        mFragmentMyFavoriteListBinding.rvItinerarySelection.setLayoutManager(new LinearLayoutManager(getContext()));
        mFragmentMyFavoriteListBinding.rvItinerarySelection.setAdapter(itinerarySelectionAdapter);

        mFragmentMyFavoriteListBinding.ivSelectionClose.setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        });
    }

    public void setupFloatingActionButton() {
        mFragmentMyFavoriteListBinding.fabAddNewMyFavourite.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainer, new SearchPlaceFragment(), SearchPlaceFragment.TAG)
                    .addToBackStack(TAG)
                    .commit();
        });
    }
}
