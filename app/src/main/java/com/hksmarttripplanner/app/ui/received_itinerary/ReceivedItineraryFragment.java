package com.hksmarttripplanner.app.ui.received_itinerary;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentReceivedItineraryBinding;
import com.hksmarttripplanner.app.ui.adapters.ReceivedItineraryAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ReceivedItineraryFragment extends BaseFragment {
    public static final String TAG = ReceivedItineraryFragment.class.getSimpleName();
    private ReceivedItineraryViewModel receivedItineraryViewModel;
    private FragmentReceivedItineraryBinding fragmentReceivedItineraryBinding;
    private ReceivedItineraryAdapter receivedItineraryAdapter;

    public static ReceivedItineraryFragment newInstance() {
        Bundle bundle = new Bundle();
        ReceivedItineraryFragment receivedItineraryFragment = new ReceivedItineraryFragment();
        receivedItineraryFragment.setArguments(bundle);
        return receivedItineraryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReceivedItineraryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_received_itinerary, container, false);
        receivedItineraryAdapter = new ReceivedItineraryAdapter(new ArrayList());
        return fragmentReceivedItineraryBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ReceivedItineraryViewModel.Factory factory = new ReceivedItineraryViewModel.Factory(getActivity().getApplication());
        receivedItineraryViewModel = ViewModelProviders.of(getActivity(), factory).get(ReceivedItineraryViewModel.class);
        fragmentReceivedItineraryBinding.setReceivedItineraryViewModel(receivedItineraryViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(receivedItineraryViewModel.listReceivedItinerary()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((itineraries) -> {
                    receivedItineraryAdapter.setItemList(itineraries);
                }, throwable -> {
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(receivedItineraryAdapter.getOnClickReceiveItem()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itineraries -> {
                    receivedItineraryViewModel.receiveItinerary(itineraries.getId(), itineraries.getItinerary())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {

                            }, throwable -> {
                            });
                }, throwable -> {
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(receivedItineraryAdapter.getOnClickDeclineItem()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itineraries -> {
                    receivedItineraryViewModel.declineItinerary(itineraries.getId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {

                            }, throwable -> {
                            });
                }, throwable -> {
                    throwable.printStackTrace();
                }));
        setToolbar();
        setRecyclerView();
    }

    public void setToolbar() {
        fragmentReceivedItineraryBinding.tbReceivedItinerary.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    public void setRecyclerView() {
        fragmentReceivedItineraryBinding.rvReceivedItinerary.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentReceivedItineraryBinding.rvReceivedItinerary.setAdapter(receivedItineraryAdapter);
    }
}
