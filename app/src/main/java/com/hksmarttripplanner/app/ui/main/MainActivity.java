package com.hksmarttripplanner.app.ui.main;

import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.ui.create_itinerary.CreateItineraryFragment;
import com.hksmarttripplanner.app.ui.friend_list.FriendListFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_list.ItineraryDayListFragment;

public class MainActivity extends AppCompatActivity implements ItineraryDayListFragment.Listener, CreateItineraryFragment.Listener {
    public static final String TAG = MainActivity.class.getSimpleName();
    private static final String STATE_ITINERARY_ID = "STATE_ITINERARY_ID";
    private static final String STATE_ITINERARY_DAY_ID = "STATE_ITINERARY_DAY_ID";
    private static final String STATE_PLACE_ID = "STATE_PLACE_ID";

    public int stateItineraryId;
    public int stateItineraryDayId;
    public int statePlaceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        if (savedInstanceState == null) {
            ItineraryListFragment fragment = new ItineraryListFragment();
//            ItineraryDayPlaceListFragment fragment = ItineraryDayPlaceListFragment.newInstance(1, MyDateUtils.getFormattedDate(new Date(2019, 5, 9)));
//            TrafficDetailFragment fragment = TrafficDetailFragment.newInstance(null);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, fragment, ItineraryListFragment.TAG).commit();
        }
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            menuItem -> {
                Fragment fragment = null;
                String tag = "";
                Intent intent = null;
                switch (menuItem.getItemId()) {
                    case R.id.navItinerary:
                        resetState();
                        fragment = new ItineraryListFragment();
                        tag = ItineraryListFragment.TAG;
                        break;
                    case R.id.navFriend:
                        resetState();
                        fragment = new FriendListFragment();
                        tag = FriendListFragment.TAG;
                        break;
                    case R.id.navSearch:
                        resetState();
                        fragment = new SearchPlaceFragment();
                        tag = SearchPlaceFragment.TAG;
                        break;
                    case R.id.navMore:
                        resetState();
                        fragment = new SettingFragment();
                        tag = SettingFragment.TAG;
                        break;
                }

                getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragment, tag).addToBackStack(TAG).commit();
                return true;
            };

    @Override
    public void onBackPressed(){
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getFragments().size(); i++) {
            Log.d(TAG, "onBack Pressed FragmentManager:" + fm.getFragments().get(i).getTag());
        }
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    public void resetState() {
        stateItineraryId = 0;
        stateItineraryDayId = 0;
        statePlaceId = 0;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_ITINERARY_ID, stateItineraryId);
        outState.putInt(STATE_ITINERARY_DAY_ID, stateItineraryDayId);
        outState.putInt(STATE_PLACE_ID, statePlaceId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        stateItineraryId = savedInstanceState.getInt(STATE_ITINERARY_ID);
        stateItineraryDayId = savedInstanceState.getInt(STATE_ITINERARY_DAY_ID);
        statePlaceId = savedInstanceState.getInt(STATE_PLACE_ID);
    }

    @Override
    public void addOptimizedItinerary(Itinerary itinerary) {
        ItineraryListFragment fragment = (ItineraryListFragment) getSupportFragmentManager().findFragmentByTag(ItineraryListFragment.TAG);
        if (fragment != null) {
            getSupportFragmentManager().popBackStack(ItineraryListFragment.TAG, 1);
            fragment.addItinerary(itinerary);
        }
    }

    @Override
    public void addItinerary(Itinerary itinerary) {
        ItineraryListFragment fragment = (ItineraryListFragment) getSupportFragmentManager().findFragmentByTag(ItineraryListFragment.TAG);
        if (fragment != null) {
            getSupportFragmentManager().popBackStack(ItineraryListFragment.TAG, 1);
            fragment.addItinerary(itinerary);
        }
    }
}
