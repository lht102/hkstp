package com.hksmarttripplanner.app.ui.main;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.ui.adapters.ItineraryAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.databinding.FragmentItineraryListBinding;
import com.hksmarttripplanner.app.ui.create_itinerary.CreateItineraryFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_list.ItineraryDayListFragment;
import com.hksmarttripplanner.app.ui.received_itinerary.ReceivedItineraryFragment;
import com.hksmarttripplanner.app.ui.share_itinerary.ShareItineraryFragment;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItineraryListFragment extends BaseFragment {
    public static final String TAG = ItineraryListFragment.class.getSimpleName();
    private ItineraryListViewModel mItineraryListViewModel;
    private FragmentItineraryListBinding mFragmentItineraryListBinding;
    private ItineraryAdapter itineraryAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentItineraryListBinding = FragmentItineraryListBinding.inflate(inflater, container, false);

        itineraryAdapter = new ItineraryAdapter(new ArrayList<>());

        return mFragmentItineraryListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ItineraryListViewModel.Factory factory = new ItineraryListViewModel.Factory(getActivity().getApplication());
        mItineraryListViewModel = ViewModelProviders.of(this, factory).get(ItineraryListViewModel.class);
        mFragmentItineraryListBinding.setViewModel(mItineraryListViewModel);

        setupRecyclerView();
        setupSwipeRefreshLayout();
        setupFloatingActionButton();
        setupBtnToReceivedFragment();
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(mItineraryListViewModel.getSnackbarText()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showSnackBar,
                        throwable -> Log.e(getTag(), "Unable to display SnackBar")));

        getCompositeDisposable().add(itineraryAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itinerary -> {
                    Log.d(TAG, "id: " + itinerary.getId());
                    mItineraryListViewModel.makeItinerarySelection(itinerary);
                    ((MainActivity) (getActivity())).stateItineraryId = itinerary.getId();
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentContainer, ItineraryDayListFragment.
                                    newInstance(itinerary.getId(), itinerary.getTitle()), ItineraryDayListFragment.TAG)
                            .addToBackStack(TAG)
                            .commit();
                }, throwable -> {
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(itineraryAdapter.getOnClickShareItem()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itinerary -> {
                    mItineraryListViewModel.makeItinerarySelection(itinerary);
                    ((MainActivity) (getActivity())).stateItineraryId = itinerary.getId();
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentContainer, ShareItineraryFragment.
                                    newInstance(itinerary.getId()))
                            .addToBackStack(TAG)
                            .commit();
                }, throwable -> {
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(itineraryAdapter.getOnClickDeleteItem()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itinerary -> {
                    mItineraryListViewModel.deleteItienraryById(itinerary.getId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {
                                mItineraryListViewModel.getSnackbarText().onNext(R.string.itinerary_delete_msg);
                            }, throwable -> {

                            });
                }, throwable -> {
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(mItineraryListViewModel.getMyItineraries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((itineraries) -> {
                    itineraryAdapter.setItemList(itineraries);
                }, throwable -> {
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(mItineraryListViewModel.checkIsExistSharedItinerary()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((booleans) -> {
                    if (booleans) {
                        mFragmentItineraryListBinding.btnToReceivedItineraryFragment.setVisibility(View.VISIBLE);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                }));
    }

    public void setupSwipeRefreshLayout() {
        mFragmentItineraryListBinding.srlItineraries.setOnRefreshListener(() -> {
            itineraryAdapter.clear();
            getCompositeDisposable().add(mItineraryListViewModel.getMyItineraries()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((itineraries) -> {
                        itineraryAdapter.setItemList(itineraries);
                        mFragmentItineraryListBinding.srlItineraries.setRefreshing(false);
                    }, throwable -> {
                        throwable.printStackTrace();
                    }));
        });
    }

    public void setupRecyclerView() {
        mFragmentItineraryListBinding.rvItineraries.setLayoutManager(new LinearLayoutManager(getContext()));
        mFragmentItineraryListBinding.rvItineraries.setAdapter(itineraryAdapter);
        mFragmentItineraryListBinding.rvItineraries.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                FloatingActionButton fab = mFragmentItineraryListBinding.fabAddItinerary;
                if (dy > 0 && fab.isShown()) {
                    fab.hide();
                }
                else if (dy < 0 && !fab.isShown()) {
                    fab.show();
                }
            }
        });
    }

    public void setupFloatingActionButton() {
        mFragmentItineraryListBinding.fabAddItinerary.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainer, CreateItineraryFragment.newInstance(), CreateItineraryFragment.TAG)
                    .addToBackStack(TAG)
                    .commit();
        });
    }

    public void setupBtnToReceivedFragment() {
        mFragmentItineraryListBinding.btnToReceivedItineraryFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction().add(R.id.fragmentContainer, ReceivedItineraryFragment.newInstance())
                .addToBackStack(TAG).commit();
            }
        });
    }

    public void addItinerary(Itinerary itinerary) {
        itineraryAdapter.addItinerary(itinerary);
        showSnackBar(R.string.itinerary_created_msg);
    }
}
