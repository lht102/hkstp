package com.hksmarttripplanner.app.ui.itinerary_selection;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentItinerarySelectionBinding;
import com.hksmarttripplanner.app.ui.adapters.SimpleItineraryAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_selection.ItineraryDaySelectionFragment;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItinerarySelectionFragment extends BaseFragment {
    public static final String TAG = ItinerarySelectionFragment.class.getSimpleName();

    private FragmentItinerarySelectionBinding binding;
    private ItinerarySelectionViewModel viewModel;
    private SimpleItineraryAdapter adapter = new SimpleItineraryAdapter(new ArrayList<>());

    public static ItinerarySelectionFragment newInstance() {
        Bundle args = new Bundle();
        ItinerarySelectionFragment fragment = new ItinerarySelectionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_itinerary_selection, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ItinerarySelectionViewModel.Factory factory = new ItinerarySelectionViewModel.Factory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(this, factory).get(ItinerarySelectionViewModel.class);
        binding.setViewModel(viewModel);

        setupSwipeRefreshLayout();
        setupRecyclerView();
        setupToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(viewModel.getMyItineraries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((itineraries, throwable) -> {
                    adapter.setItemList(itineraries);
                }));

        getCompositeDisposable().add(adapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itinerary -> {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragmentContainer, ItineraryDaySelectionFragment.newInstance(itinerary.getId()), TAG)
                            .addToBackStack(TAG)
                            .commit();
                }));
    }

    public void setupSwipeRefreshLayout() {
        binding.srlItinerarySelection.setOnRefreshListener(() -> {
            adapter.clear();
            getCompositeDisposable().add(viewModel.getMyItineraries()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((itineraries, throwable) -> {
                        adapter.setItemList(itineraries);
                        binding.srlItinerarySelection.setRefreshing(false);
                    }));
        });
    }

    public void setupRecyclerView() {
        binding.rvSimpleItineraries.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvSimpleItineraries.setAdapter(adapter);
    }

    public void setupToolbar() {
        binding.tbItinerarySelection.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }
}
