package com.hksmarttripplanner.app.ui.adapters;

public interface SwappableItemListener {
    void changeOrder(int curr, int prevId, int nextId);
    void changeOrderPrevId(int curr, int prevId);
    void changeOrderNextId(int curr, int nextId);
}
