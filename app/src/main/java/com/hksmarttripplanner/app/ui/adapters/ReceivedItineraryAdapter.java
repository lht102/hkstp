package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.ReceivedItinerary;
import com.hksmarttripplanner.app.databinding.ItemReceivedItineraryBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class ReceivedItineraryAdapter extends RecyclerView.Adapter<ReceivedItineraryAdapter.ReceivedItineraryViewHolder> {
    private final PublishSubject<ReceivedItinerary> onClickReceiveItem = PublishSubject.create();
    private final PublishSubject<ReceivedItinerary> onClickDeclineItem = PublishSubject.create();
    private List<ReceivedItinerary> itemList;

    public ReceivedItineraryAdapter(List<ReceivedItinerary> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ReceivedItineraryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemReceivedItineraryBinding binding = ItemReceivedItineraryBinding.inflate(layoutInflater, viewGroup, false);
        return new ReceivedItineraryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceivedItineraryViewHolder receivedItineraryViewHolder, int i) {
        ReceivedItinerary receivedItinerary = itemList.get(i);
        receivedItineraryViewHolder.bind(receivedItinerary);

        receivedItineraryViewHolder.binding.btnReceive.setOnClickListener(v -> {
            onClickReceiveItem.onNext(receivedItinerary);
            int index = itemList.indexOf(receivedItinerary);
            itemList.remove(index);
            notifyItemRemoved(index);
        });

        receivedItineraryViewHolder.binding.btnDecline.setOnClickListener(v -> {
            onClickDeclineItem.onNext(receivedItinerary);
            int index = itemList.indexOf(receivedItinerary);
            itemList.remove(index);
            notifyItemRemoved(index);
        });
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setItemList(List<ReceivedItinerary> newItemList) {
        if (newItemList == null) {
            int oldSize = itemList.size();
            itemList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new ReceivedItineraryDiffCallBack(itemList, newItemList));
            itemList.clear();
            itemList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    public void clear() {
        itemList.clear();
        notifyDataSetChanged();
    }

    public PublishSubject<ReceivedItinerary> getOnClickReceiveItem() { return onClickReceiveItem; }

    public PublishSubject<ReceivedItinerary> getOnClickDeclineItem() { return onClickDeclineItem; }

    public static class ReceivedItineraryViewHolder extends RecyclerView.ViewHolder {
        private final ItemReceivedItineraryBinding binding;

        public ReceivedItineraryViewHolder(ItemReceivedItineraryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ReceivedItinerary receivedItinerary) {
            binding.setReceivedItinerary(receivedItinerary);
            binding.executePendingBindings();
        }
    }

    private class ReceivedItineraryDiffCallBack extends DiffUtil.Callback {
        private List<ReceivedItinerary> oldList;
        private List<ReceivedItinerary> newList;

        public ReceivedItineraryDiffCallBack(List<ReceivedItinerary> oldList, List<ReceivedItinerary> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            ReceivedItinerary oldItinerary = oldList.get(oldItemPos);
            ReceivedItinerary newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
