package com.hksmarttripplanner.app.ui.main;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;

import io.reactivex.Single;
import retrofit2.HttpException;

public class SearchPlaceViewModel extends BaseViewModel {

    public SearchPlaceViewModel(Application application) {
        super(application);
    }

    public Single<User> getCurrency() {
        return getAppRepository().getMyUserInfo()
                .doOnSubscribe(disposable -> {})
                .doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new SearchPlaceViewModel(application);
        }
    }
}
