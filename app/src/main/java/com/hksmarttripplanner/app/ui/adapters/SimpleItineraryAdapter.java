package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.databinding.ItemSimpleItineraryBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class SimpleItineraryAdapter extends RecyclerView.Adapter<SimpleItineraryAdapter.ItineraryViewHolder> {
    private final PublishSubject<Itinerary> onClickItem = PublishSubject.create();
    private List<Itinerary> itemList;

    public SimpleItineraryAdapter(List<Itinerary> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ItineraryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemSimpleItineraryBinding binding = ItemSimpleItineraryBinding.inflate(layoutInflater, viewGroup, false);
        return new ItineraryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItineraryViewHolder itineraryViewHolder, int i) {
        Itinerary itinerary = itemList.get(i);
        itineraryViewHolder.bind(itinerary);

        itineraryViewHolder.binding.cvSimpleItinerary.setOnClickListener(v -> {
            onClickItem.onNext(itinerary);
        });
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public PublishSubject<Itinerary> getOnClickPos() {
        return onClickItem;
    }

    public void setItemList(List<Itinerary> newItemList) {
        if (newItemList == null) {
            int oldSize = itemList.size();
            itemList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new ItineraryDiffCallBack(itemList, newItemList));
            itemList.clear();
            itemList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    public void clear() {
        itemList.clear();
        notifyDataSetChanged();
    }

    public static class ItineraryViewHolder extends RecyclerView.ViewHolder {
        private final ItemSimpleItineraryBinding binding;

        public ItineraryViewHolder(ItemSimpleItineraryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Itinerary itinerary) {
            binding.setItinerary(itinerary);
            binding.executePendingBindings();
        }
    }

    private class ItineraryDiffCallBack extends DiffUtil.Callback {
        private List<Itinerary> oldList;
        private List<Itinerary> newList;

        public ItineraryDiffCallBack(List<Itinerary> oldList, List<Itinerary> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            Itinerary oldItinerary = oldList.get(oldItemPos);
            Itinerary newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
