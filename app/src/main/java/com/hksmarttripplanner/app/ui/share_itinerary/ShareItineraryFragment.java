package com.hksmarttripplanner.app.ui.share_itinerary;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.databinding.FragmentShareItineraryBinding;
import com.hksmarttripplanner.app.ui.adapters.UserShareAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.friend_information.FriendInformationFragment;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ShareItineraryFragment extends BaseFragment {
    private static final String TAG = ShareItineraryFragment.class.getSimpleName();
    private ShareItineraryViewModel shareItineraryViewModel;
    private FragmentShareItineraryBinding fragmentShareItineraryBinding;
    private UserShareAdapter userShareAdapter;
    private List<User> user;
    private int itinerarayId;

    public static ShareItineraryFragment newInstance(int itineraryId) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", itineraryId);
        ShareItineraryFragment shareItineraryFragment = new ShareItineraryFragment();
        shareItineraryFragment.setArguments(bundle);
        return shareItineraryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentShareItineraryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_share_itinerary, container, false);
        userShareAdapter = new UserShareAdapter(new ArrayList());
        return fragmentShareItineraryBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ShareItineraryViewModel.Factory factory = new ShareItineraryViewModel.Factory(getActivity().getApplication());
        shareItineraryViewModel = ViewModelProviders.of(getActivity(), factory).get(ShareItineraryViewModel.class);
        fragmentShareItineraryBinding.setShareItineraryViewModel(shareItineraryViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        itinerarayId = getArguments().getInt("id");
        getCompositeDisposable().add(shareItineraryViewModel
                .getSentFriendList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((sentUsers) -> {
                    getCompositeDisposable().add(shareItineraryViewModel
                            .getFirmFriendList().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((firmUsers) -> {
                                List<Integer> sent = new LinkedList();
                                for (int i = 0; i < sentUsers.size(); i++) {
                                    sent.add(sentUsers.get(i).getId());
                                }
                                List<Integer> firm = new LinkedList();
                                for (int i = 0; i < firmUsers.size(); i++) {
                                    firm.add(firmUsers.get(i).getId());
                                }
                                user = new LinkedList();
                                int countSent = sent.size();
                                int countFirm = firm.size();
                                for (int i = 0; i < countSent; i++) {
                                    for (int j = 0; j < countFirm; j++) {
                                        if (sentUsers.get(i).getId() == firmUsers.get(j).getId()) {
                                            user.add(firmUsers.get(j));
                                        }
                                    }
                                }
                                userShareAdapter.setItemlist(user);
                            }, Throwable::printStackTrace));
                }, Throwable::printStackTrace));

        getCompositeDisposable().add(userShareAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    shareItineraryViewModel.makeUserSelection(user);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, FriendInformationFragment
                                    .newInstance(user), TAG).addToBackStack(TAG).commit();
                }));

        getCompositeDisposable().add(userShareAdapter.getOnClickShare()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    getCompositeDisposable().add(shareItineraryViewModel.getItineraryById(itinerarayId)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(itinerary -> {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Make sure share [ " + itinerary.getTitle() + " ] to " + user.getEmail() + " ?")
                                .setCancelable(false).setNegativeButton("No", (dialog, which) -> dialog.cancel())
                                .setPositiveButton("Yes", (dialog, which) -> {
                                    getCompositeDisposable().add(shareItineraryViewModel
                                            .shareItinerary(user.getId(), itinerary.getId())
                                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                    .subscribe());
                                    AlertDialog.Builder success = new AlertDialog.Builder(getActivity());
                                    success.setTitle("Success");
                                    success.setMessage("You have shared [ " + itinerary.getTitle() + " ] to " + user.getEmail() + ".");
                                    success.show();
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }));
                }));

        setRecyclerView();
        setToolbar();
    }

    public void setRecyclerView() {
        fragmentShareItineraryBinding.rvShareItinerary.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentShareItineraryBinding.rvShareItinerary.setAdapter(userShareAdapter);
    }

    private void setToolbar() {
        fragmentShareItineraryBinding.tbShareItinerary.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }
}
