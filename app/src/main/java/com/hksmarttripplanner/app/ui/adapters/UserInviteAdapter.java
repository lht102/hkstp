package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.UserInvite;
import com.hksmarttripplanner.app.databinding.ItemUserInviteBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class UserInviteAdapter extends RecyclerView.Adapter<UserInviteAdapter.UserInviteViewHolder> {
    private final PublishSubject<UserInvite> onClickItem = PublishSubject.create();
    private final PublishSubject<UserInvite> onClickCancel = PublishSubject.create();
    private List<UserInvite> userInviteList;

    public UserInviteAdapter(List<UserInvite> userInviteList) {
        this.userInviteList = userInviteList;
    }

    @NonNull
    @Override
    public UserInviteAdapter.UserInviteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemUserInviteBinding itemUserInviteBinding = ItemUserInviteBinding.inflate(layoutInflater, viewGroup, false);
        return new UserInviteViewHolder(itemUserInviteBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserInviteViewHolder userInviteViewHolder, int i) {
        UserInvite userInvite = userInviteList.get(i);
        userInviteViewHolder.bind(userInvite);

        userInviteViewHolder.itemUserInviteBinding.crUserInvite.setOnClickListener(v -> {
            onClickItem.onNext(userInvite);
        });

        userInviteViewHolder.itemUserInviteBinding.btnCancelInvite.setOnClickListener(v -> {
            onClickCancel.onNext(userInvite);
            int index = userInviteList.indexOf(userInvite);
            userInviteList.remove(index);
            notifyItemRemoved(index);
        });
    }

    @Override
    public int getItemCount()  {
        return userInviteList == null ? 0 : userInviteList.size();
    }

    public PublishSubject<UserInvite> getOnClickPos() {
        return onClickItem;
    }

    public PublishSubject<UserInvite> getOnClickCancel() {
        return onClickCancel;
    }

    public static class UserInviteViewHolder extends RecyclerView.ViewHolder {
        private final ItemUserInviteBinding itemUserInviteBinding;

        public UserInviteViewHolder(ItemUserInviteBinding itemUserInviteBinding) {
            super(itemUserInviteBinding.getRoot());
            this.itemUserInviteBinding = itemUserInviteBinding;
        }

        public void bind(UserInvite userInvite) {
            itemUserInviteBinding.setUserInvite(userInvite);
            itemUserInviteBinding.executePendingBindings();
        }
    }

    public void setItemlist(List<UserInvite> newItemList) {
        if (newItemList == null) {
            int oldSize = userInviteList.size();
            userInviteList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new UserDiffCallBack(userInviteList, newItemList));
            userInviteList.clear();
            userInviteList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    private class UserDiffCallBack extends DiffUtil.Callback {
        private List<UserInvite> oldList;
        private List<UserInvite> newList;

        public UserDiffCallBack(List<UserInvite> oldList, List<UserInvite> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            UserInvite oldItinerary = oldList.get(oldItemPos);
            UserInvite newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
