package com.hksmarttripplanner.app.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentSettingBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.login.LoginActivity;
import com.hksmarttripplanner.app.ui.my_favorite.MyFavoriteFragment;
import com.hksmarttripplanner.app.ui.profile.ProfileFragment;
import com.hksmarttripplanner.app.ui.weather.FragmentWeather;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SettingFragment extends BaseFragment {
    public static final String TAG = SettingFragment.class.getSimpleName();
    private SettingViewModel settingViewModel;
    private FragmentSettingBinding fragmentSettingBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSettingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        return fragmentSettingBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SettingViewModel.Factory factory = new SettingViewModel.Factory(getActivity().getApplication());
        settingViewModel = ViewModelProviders.of(getActivity(), factory).get(SettingViewModel.class);
        fragmentSettingBinding.setSetting(settingViewModel);

        setupLogoutBtn();
        setFavoriteBtn();
        setupProfileBtn();
        setupWeatherBtn();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setFavoriteBtn() {
        fragmentSettingBinding.cvFavoriteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentContainer, MyFavoriteFragment.newInstance(), MyFavoriteFragment.TAG)
                        .addToBackStack(TAG)
                        .commit();
            }
        });
    }

    public void setupLogoutBtn() {
        fragmentSettingBinding.cvLogout.setOnClickListener(v -> {
            getCompositeDisposable().add(settingViewModel.logout()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }));
        });
    }

    public void setupProfileBtn() {
        fragmentSettingBinding.cvMyProfile.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, ProfileFragment.newInstance(), ProfileFragment.TAG)
                    .addToBackStack(TAG)
                    .commit();
        });
    }

    public void setupWeatherBtn() {
        fragmentSettingBinding.cvWeather.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, FragmentWeather.newInstance(), FragmentWeather.TAG)
                    .addToBackStack(TAG)
                    .commit();
        });
    }
}