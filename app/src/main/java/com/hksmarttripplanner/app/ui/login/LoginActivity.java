package com.hksmarttripplanner.app.ui.login;

import android.content.Intent;
import android.os.Bundle;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.AppPreferencesManager;
import com.hksmarttripplanner.app.ui.base.BaseActivity;
import com.hksmarttripplanner.app.ui.main.MainActivity;

public class LoginActivity extends BaseActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (savedInstanceState == null) {
            AppPreferencesManager appPreferencesManager = AppPreferencesManager.getInstance(getApplicationContext());
            if (appPreferencesManager.getMyUserId() < 1) {
                getSupportFragmentManager().beginTransaction().add(R.id.contentFrame, new LoginFragment(), TAG)
                        .commitNow();
            } else {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                this.finish();
            }
        }
    }
}
