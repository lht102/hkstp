package com.hksmarttripplanner.app.ui.create_itinerary;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.databinding.FragmentCreateItineraryBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.create_activity.CreateActivityFragment;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CreateItineraryFragment extends BaseFragment {
    public static final String TAG = CreateActivityFragment.class.getSimpleName();
    private FragmentCreateItineraryBinding mFragmentCreateItineraryBinding;
    private CreateItineraryViewModel mCreateItineraryViewModel;

    private DatePickerDialog datePickerDialog;
    private Listener listener;

    public interface Listener {
        public void addItinerary(Itinerary itinerary);
    }

    public static CreateItineraryFragment newInstance() {
        CreateItineraryFragment fragment = new CreateItineraryFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +  " must implement Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentCreateItineraryBinding = FragmentCreateItineraryBinding.inflate(inflater, container, false);
        return mFragmentCreateItineraryBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CreateItineraryViewModel.Factory factory = new CreateItineraryViewModel.Factory(getActivity().getApplication());
        mCreateItineraryViewModel = ViewModelProviders.of(getActivity(), factory).get(CreateItineraryViewModel.class);
        mFragmentCreateItineraryBinding.setViewModel(mCreateItineraryViewModel);

        setupTextView();
        setupConfirmBtn();
        setupToolbar();
    }

    public void setupTextView() {
        mFragmentCreateItineraryBinding.etStartDate.setOnClickListener(v -> {
            datePickerDialog = createDatePickerDiaglog((TextView) v);
            datePickerDialog.show(getActivity().getFragmentManager(), getTag());
        });

        mFragmentCreateItineraryBinding.etEndDate.setOnClickListener(v -> {
            datePickerDialog = createDatePickerDiaglog((TextView) v);
            datePickerDialog.show(getActivity().getFragmentManager(), getTag());
        });
    }

    public void setupConfirmBtn() {
        mFragmentCreateItineraryBinding.btnCreateItinerary.setOnClickListener(v -> {
            try {
                String title = mFragmentCreateItineraryBinding.etTitle.getText().toString();
                double budget = Double.parseDouble(mFragmentCreateItineraryBinding.etBudget.getText().toString());
                String startDateStr = mFragmentCreateItineraryBinding.etStartDate.getText().toString();
                String endDateStr = mFragmentCreateItineraryBinding.etEndDate.getText().toString();

                getCompositeDisposable().add(mCreateItineraryViewModel
                        .createItinerary(title, budget, startDateStr, endDateStr)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(itinerary -> {
                            listener.addItinerary(itinerary);
                        }, throwable -> {

                        }));
            } catch (NumberFormatException e) {
                showSnackBar(R.string.simple_error_msg);
            }
        });
    }

    public void setupToolbar() {
        mFragmentCreateItineraryBinding.tbCreateItinerary.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    public DatePickerDialog createDatePickerDiaglog(TextView textView) {
        Calendar now = Calendar.getInstance();
        if (datePickerDialog == null) {
            datePickerDialog = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                            String dateStr = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            textView.setText(dateStr);
                        }
                    }, now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
        } else {
            datePickerDialog.initialize(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                            String dateStr = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            textView.setText(dateStr);
                        }
                    }, now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
        }
        return datePickerDialog;
    }
}
