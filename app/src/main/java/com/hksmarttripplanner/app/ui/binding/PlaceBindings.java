package com.hksmarttripplanner.app.ui.binding;

import android.arch.persistence.room.util.StringUtil;
import android.databinding.BindingAdapter;
import android.widget.TextView;

import com.hksmarttripplanner.app.data.source.local.model.Budget;
import com.hksmarttripplanner.app.data.source.local.model.Hour;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.local.model.Time;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.util.List;

public class PlaceBindings {
    @BindingAdapter("openTimeDescription")
    public static void setOpenTimeDescription(TextView textView, Place place) {
        String str = "";
        List<Time> times = place.getTimes();
        for (int i = 0; i < times.size(); i++) {
            Time time = times.get(i);
            String weekday = time.getWeekday();
            str += weekday;
            List<Hour> hours = time.getHours();
            for (int j = 0; j < hours.size(); j++) {
                Hour hour = hours.get(j);
                String open = hour.getOpen();
                String close = hour.getClose();

                str += "\t\t" + open + "\t\t" + close + "\n";
            }
        }
        textView.setText(str);
    }

    @BindingAdapter("PlaceEndTime")
    public static void setPlaceEndTime(TextView textView, Place place) {
        DateTime startTimeDate = place.getStartTime();
        if (startTimeDate != null) {
            LocalTime startTime = startTimeDate.toLocalTime();
            textView.setText(startTime.plusMinutes(place.getDuration()).toString("HH:mm"));
        }
    }
}
