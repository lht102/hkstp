package com.hksmarttripplanner.app.ui.login;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.data.source.remote.api.response.LoginResponse;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;
import retrofit2.HttpException;

public class LoginViewModel extends BaseViewModel {

    public LoginViewModel(Application application) {
        super(application);
    }

    public Single<LoginResponse> login(String account, String password) {
        return getAppRepository()
                .login(account, password)
                .doOnSubscribe(disposable -> setIsLoading(true))
                .doOnError(throwable -> {
                    setIsLoading(false);
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                        getSnackbarText().onNext(R.string.login_fail_login);
                    }
                });
    }

    public boolean isAccountAndPasswordValid(String account, String password) {
        return !(TextUtils.isEmpty(account) || TextUtils.isEmpty(password));
    }

    public Observable<Boolean> validate(String account, String password) {
        ObservableOnSubscribe<Boolean> handler = emitter -> {
            if (isAccountAndPasswordValid(account, password)) {
                emitter.onNext(true);
                emitter.onComplete();
            } else {
                emitter.onError(new IllegalArgumentException());
            }
        };

        return PublishSubject.create(handler).doOnError(throwable -> getSnackbarText()
                .onNext(R.string.login_miss_account_password))
                .doOnDispose(() -> Log.d("isDisposed", "it is diposed"));
    }

    public boolean saveLoginInformaiton(String account, String password, String token, long expireTime) {
        return getAppRepository().saveLoginInformation(account, password, token, expireTime);
    }

    public boolean saveUserId(int id) {
        return getAppRepository().saveMyUserId(id);
    }

    public boolean saveUserCurrency(String cur) {
        return getAppRepository().saveMyUserCurrency(cur);
    }

    public Single<User> getMyUserInfo() {
        return getAppRepository().getMyUserInfo();
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new LoginViewModel(application);
        }
    }
}