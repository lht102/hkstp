package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.UserReceive;
import com.hksmarttripplanner.app.databinding.ItemUserReceiveBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class UserReceiveAdapter extends RecyclerView.Adapter<UserReceiveAdapter.UserReceiveViewHolder> {
    private final PublishSubject<UserReceive> onClickItem = PublishSubject.create();
    private final PublishSubject<UserReceive> onClickAccept = PublishSubject.create();
    private final PublishSubject<UserReceive> onClickReject = PublishSubject.create();
    private List<UserReceive> userReceiveList;

    public UserReceiveAdapter(List<UserReceive> userReceiveList) {
        this.userReceiveList = userReceiveList;
    }

    @NonNull
    @Override
    public UserReceiveViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemUserReceiveBinding itemUserReceiveBinding = ItemUserReceiveBinding.inflate(layoutInflater, viewGroup, false);
        return new UserReceiveViewHolder(itemUserReceiveBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserReceiveViewHolder userReceiveViewHolder, int i) {
        UserReceive userReceive = userReceiveList.get(i);
        userReceiveViewHolder.bind(userReceive);

        userReceiveViewHolder.itemUserReceiveBinding.crUserReceive.setOnClickListener(v -> {
            onClickItem.onNext(userReceive);
        });

        userReceiveViewHolder.itemUserReceiveBinding.btnAcceptInvite.setOnClickListener(v -> {
            onClickAccept.onNext(userReceive);
            int index = userReceiveList.indexOf(userReceive);
            userReceiveList.remove(index);
            notifyItemRemoved(index);
        });

        userReceiveViewHolder.itemUserReceiveBinding.btnRejectInvite.setOnClickListener(v -> {
            onClickReject.onNext(userReceive);
            int index = userReceiveList.indexOf(userReceive);
            userReceiveList.remove(index);
            notifyItemRemoved(index);
        });
    }

    @Override
    public int getItemCount()  {
        return userReceiveList == null ? 0 : userReceiveList.size();
    }

    public PublishSubject<UserReceive> getOnClickPos() {
        return onClickItem;
    }

    public PublishSubject<UserReceive> getOnClickAccept() {
        return onClickAccept;
    }

    public PublishSubject<UserReceive> getOnClickReject() {
        return onClickReject;
    }

    public static class UserReceiveViewHolder extends RecyclerView.ViewHolder {
        private final ItemUserReceiveBinding itemUserReceiveBinding;

        public UserReceiveViewHolder(ItemUserReceiveBinding itemUserReceiveBinding) {
            super(itemUserReceiveBinding.getRoot());
            this.itemUserReceiveBinding = itemUserReceiveBinding;
        }

        public void bind(UserReceive userReceive) {
            itemUserReceiveBinding.setUserReceive(userReceive);
            itemUserReceiveBinding.executePendingBindings();
        }
    }

    public void setItemlist(List<UserReceive> newItemList) {
        if (newItemList == null) {
            int oldSize = userReceiveList.size();
            userReceiveList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new UserDiffCallBack(userReceiveList, newItemList));
            userReceiveList.clear();
            userReceiveList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    private class UserDiffCallBack extends DiffUtil.Callback {
        private List<UserReceive> oldList;
        private List<UserReceive> newList;

        public UserDiffCallBack(List<UserReceive> oldList, List<UserReceive> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            UserReceive oldItinerary = oldList.get(oldItemPos);
            UserReceive newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
