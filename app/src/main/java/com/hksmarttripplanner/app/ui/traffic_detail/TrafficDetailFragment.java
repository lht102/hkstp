package com.hksmarttripplanner.app.ui.traffic_detail;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.data.source.remote.api.response.EncodedPolyline;
import com.hksmarttripplanner.app.data.source.remote.api.response.Leg;
import com.hksmarttripplanner.app.data.source.remote.api.response.Route;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;
import com.hksmarttripplanner.app.databinding.FragmentTrafficDetailBinding;
import com.hksmarttripplanner.app.ui.adapters.StepListAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceListFragment;
import com.hksmarttripplanner.app.utils.DetailRouteUtils;
import com.hksmarttripplanner.app.utils.MyStringUtils;

import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TrafficDetailFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnPolylineClickListener {
    private static final String TAG = TrafficDetailFragment.class.getSimpleName();
    private static final String SELECTION_DRIVING = "driving";
    private static final String SELECTION_WALK = "walking";
    private static final String SELECTION_TRANSIT = "transit";
    private static final String ARG_START_PLACE = "ARG_START_PLACE";
    private static final String ARG_END_PLACE = "ARG_END_PLACE";

    private FragmentTrafficDetailBinding binding;
    private TrafficDetailViewModel viewModel;
    private StepListAdapter adapter;
    private GoogleMap map;
    private List<Polyline> polylines;
    private String travelMode;
    private DetailRoute detailRoute;

    public static TrafficDetailFragment newInstance(Place startPlace, Place endPlace) {
        Bundle args = new Bundle();
        TrafficDetailFragment fragment = new TrafficDetailFragment();
        args.putParcelable(ARG_START_PLACE, startPlace);
        args.putParcelable(ARG_END_PLACE, endPlace);
        fragment.setArguments(args);
        return fragment;
    }

    public Place getStartPlace() throws NullPointerException {
        return getArguments().getParcelable(ARG_START_PLACE);
    }

    public Place getEndPlace() throws NullPointerException {
        return getArguments().getParcelable(ARG_END_PLACE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_traffic_detail, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TrafficDetailViewModel.Factory factory = new TrafficDetailViewModel.Factory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(this, factory).get(TrafficDetailViewModel.class);
        binding.setViewModel(viewModel);

        travelMode = getStartPlace().getTravelMode();
        detailRoute = getStartPlace().getDetailRoute();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_traffic_detail);
        mapFragment.getMapAsync(this);

        LinearLayout bottomSheet = binding.bottomSheetTrafficDetail;
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        binding.tvStartPlace.setText(getStartPlace().getName());
        binding.tvEndPlace.setText(getEndPlace().getName());
        if (DetailRouteUtils.isRouteExist(getStartPlace().getDetailRoute())) {
            PeriodFormatter formatter = new PeriodFormatterBuilder()
                    .appendHours()
                    .appendSuffix("hrs")
                    .appendMinutes()
                    .appendSuffix("mins")
                    .toFormatter();
            DetailRoute detailRoute = getStartPlace().getDetailRoute();
            Duration duration = Duration.standardMinutes(detailRoute.getTime());

            binding.tvDesTimeStr.setText(formatter.print(duration.toPeriod())
                    + "(" + detailRoute.getRoutes().get(0).getLegs().get(0).getDistance().getText() + ")");

        }
        setupRecyclerView();
        setupToolbar();
        setupTransportationSelection();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setupRecyclerView() {
        binding.rvTrafficDetail.setLayoutManager(new LinearLayoutManager(getContext()));
        List<Step> steps = getSteps();
        adapter = new StepListAdapter(getContext(), steps, binding.rvTrafficDetail);
        binding.rvTrafficDetail.setAdapter(adapter);
    }

    private List<Step> getSteps() {
        List<Step> steps = new ArrayList<>();
        try {
            List<Route> routes = detailRoute.getRoutes();
            List<Leg> legs = routes.get(0).getLegs();
            steps = legs.get(0).getSteps();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return steps;
    }

    public void setupToolbar() {
        binding.tbTrafficDetail.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        polylines = new ArrayList<>();
        updatePolyline();

        googleMap.setOnPolylineClickListener(this);
    }

    public void updatePolyline() {
        map.clear();
        List<Route> routes = detailRoute.getRoutes();
        List<LatLng> path = new ArrayList<>();

        LatLngBounds.Builder latLngBoundsBuilder = LatLngBounds.builder();

        int seq = 1;
        if (routes != null) {
            for (int i = 0; i < routes.size(); i++) {
                Route route = routes.get(i);
                List<Leg> legs = route.getLegs();
                if (legs != null) {
                    for (int j = 0; j < legs.size(); j++) {
                        Leg leg = legs.get(j);
                        List<LatLng> decode = PolyUtil.decode(route.getOverviewPolyline().getPoints());
                        IconGenerator iconFactory = new IconGenerator(getContext());
                        if (decode != null && !decode.isEmpty()) {
                            LatLng from = decode.get(0);
                            LatLng to = decode.get(decode.size() - 1);

                            Marker markerFrom = map.addMarker(new MarkerOptions()
                                    .position(from)
                                    .title(getStartPlace().getName())
                                    .icon(BitmapDescriptorFactory
                                            .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                            Marker markerTo = map.addMarker(new MarkerOptions()
                                    .position(to)
                                    .title(getEndPlace().getName())
                                    .icon(BitmapDescriptorFactory
                                            .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                            markerFrom.setZIndex(Float.MAX_VALUE);
                            markerTo.setZIndex(Float.MAX_VALUE);
                            markerFrom.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("Start")));
                            markerTo.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("End")));
                        }

                        List<Step> steps = leg.getSteps();
                        if (steps != null) {
                            for (int k = 0; k < steps.size(); k++, seq++) {
                                Step step = steps.get(k);
                                if (step.getSteps() != null && step.getSteps().size() > 0) {
                                    int subCount = 1;
                                    for (int l = 0; l < step.getSteps().size(); l++, subCount++) {
                                        Step innerStep = step.getSteps().get(l);
                                        EncodedPolyline polyline = innerStep.getPolyline();

                                        if (polyline != null) {
                                            List<LatLng> latLngs = PolyUtil.decode(polyline.getPoints());
                                            if (k != 0 && l != 0 || k != steps.size() - 1 && l != step.getSteps().size() - 1) {
                                                Marker marker = map.addMarker(new MarkerOptions()
                                                        .position(latLngs.get(0))
                                                        .title(MyStringUtils.stripHtml(innerStep.getHtmlInstructions())));
                                            }
                                            for (int m = 0; m < latLngs.size(); m++) {
                                                LatLng latLng = latLngs.get(m);
                                                latLngBoundsBuilder.include(latLng);
                                                path.add(latLng);

                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline polyline = step.getPolyline();

                                    if (polyline != null) {
                                        List<LatLng> latLngs = PolyUtil.decode(polyline.getPoints());
                                        if (k != 0 && k != steps.size() - 1) {
                                            Marker marker = map.addMarker(new MarkerOptions()
                                                    .position(latLngs.get(0))
                                                    .title(MyStringUtils.stripHtml(step.getHtmlInstructions())));
                                        }
                                        for (int m = 0; m < latLngs.size(); m++) {
                                            LatLng latLng = latLngs.get(m);
                                            latLngBoundsBuilder.include(latLng);
                                            path.add(latLng);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < polylines.size(); i++) {
                Polyline polyline = polylines.get(i);
                polyline.remove();
            }
            polylines.clear();

            if (path.size() > 0) {
                PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
                Polyline polyline = map.addPolyline(opts);
                polylines.add(polyline);

            }

            LatLngBounds latLngBounds = latLngBoundsBuilder.build();
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 400));
        }
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        Log.d(TAG, "onPolylineClick: clicked");
    }

    public void setupTransportationSelection() {
        if (travelMode.equalsIgnoreCase(SELECTION_DRIVING)) {
            binding.ivDirectionCar.setImageResource(R.drawable.ic_directions_car_black_24dp);
            binding.ivDirectionWalking.setImageResource(R.drawable.ic_directions_walk_gray_24dp);
            binding.ivDirectionTransit.setImageResource(R.drawable.ic_directions_transit_gray_24dp);
        } else if (travelMode.equalsIgnoreCase(SELECTION_WALK)) {
            binding.ivDirectionCar.setImageResource(R.drawable.ic_directions_car_gray_24dp);
            binding.ivDirectionWalking.setImageResource(R.drawable.ic_directions_walk_black_24dp);
            binding.ivDirectionTransit.setImageResource(R.drawable.ic_directions_transit_gray_24dp);
        } else if (travelMode.equalsIgnoreCase(SELECTION_TRANSIT)) {
            binding.ivDirectionCar.setImageResource(R.drawable.ic_directions_car_gray_24dp);
            binding.ivDirectionWalking.setImageResource(R.drawable.ic_directions_walk_gray_24dp);
            binding.ivDirectionTransit.setImageResource(R.drawable.ic_directions_transit_black_24dp);
        }

        binding.ivDirectionCar.setOnClickListener(v -> {
            if (!travelMode.equalsIgnoreCase(SELECTION_DRIVING)) {
                travelMode = SELECTION_DRIVING;
                binding.ivDirectionCar.setImageResource(R.drawable.ic_directions_car_black_24dp);
                binding.ivDirectionWalking.setImageResource(R.drawable.ic_directions_walk_gray_24dp);
                binding.ivDirectionTransit.setImageResource(R.drawable.ic_directions_transit_gray_24dp);
                updateTravelMode();
            }
        });

        binding.ivDirectionWalking.setOnClickListener(v -> {
            if (!travelMode.equalsIgnoreCase(SELECTION_WALK)) {
                travelMode = SELECTION_WALK;
                binding.ivDirectionCar.setImageResource(R.drawable.ic_directions_car_gray_24dp);
                binding.ivDirectionWalking.setImageResource(R.drawable.ic_directions_walk_black_24dp);
                binding.ivDirectionTransit.setImageResource(R.drawable.ic_directions_transit_gray_24dp);
                updateTravelMode();
            }
        });

        binding.ivDirectionTransit.setOnClickListener(v -> {
            if (!travelMode.equalsIgnoreCase(SELECTION_TRANSIT)) {
                travelMode = SELECTION_TRANSIT;
                binding.ivDirectionCar.setImageResource(R.drawable.ic_directions_car_gray_24dp);
                binding.ivDirectionWalking.setImageResource(R.drawable.ic_directions_walk_gray_24dp);
                binding.ivDirectionTransit.setImageResource(R.drawable.ic_directions_transit_black_24dp);
                updateTravelMode();
            }
        });
    }

    private void updateTravelMode() {
        getCompositeDisposable().add(viewModel.updateTravelMode(getStartPlace().getActivityId(), travelMode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    viewModel.setIsLoading(true);
                })
                .subscribe(() -> {
                    viewModel.getRoute(getStartPlace().getActivityId(), getEndPlace().getActivityId(), getStartPlace().getStartTime())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(detailRou -> {
                                viewModel.setIsLoading(false);
                                detailRoute = detailRou;
                                updatePolyline();
                                updateRecyclerViewDetail();
                                if (DetailRouteUtils.isRouteExist(detailRou)) {
                                    PeriodFormatter formatter = new PeriodFormatterBuilder()
                                            .appendHours()
                                            .appendSuffix("hrs")
                                            .appendMinutes()
                                            .appendSuffix("mins")
                                            .toFormatter();
                                    DetailRoute detailRoute = detailRou;
                                    Duration duration = Duration.standardMinutes(detailRoute.getTime());

                                    binding.tvDesTimeStr.setText(formatter.print(duration.toPeriod())
                                            + "(" + detailRoute.getRoutes().get(0).getLegs().get(0).getDistance().getText() + ")");

                                }
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                ItineraryDayPlaceListFragment fragment = (ItineraryDayPlaceListFragment) fm.findFragmentByTag(ItineraryDayPlaceListFragment.TAG);
                                if (fragment != null) {
                                    fragment.updateTrafficDetailAfterTravelModeUpdateByActivityId(getStartPlace().getActivityId(), detailRou, travelMode);
                                }
                            }, throwable -> {
                                throwable.printStackTrace();
                                viewModel.setIsLoading(false);
                                detailRoute = new DetailRoute();
                                updatePolyline();
                                updateRecyclerViewDetail();
                                binding.tvDesTimeStr.setText("");
                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                ItineraryDayPlaceListFragment fragment = (ItineraryDayPlaceListFragment) fm.findFragmentByTag(ItineraryDayPlaceListFragment.TAG);
                                if (fragment != null) {
                                    fragment.updateTrafficDetailAfterTravelModeUpdateByActivityId(getStartPlace().getActivityId(), detailRoute, travelMode);
                                }
                            });
                }));
    }

    private void updateRecyclerViewDetail() {
        List<Step> steps = getSteps();
        adapter.setSteps(steps);
    }
}