package com.hksmarttripplanner.app.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.hksmarttripplanner.app.R;

import org.joda.time.LocalTime;

public class EditStartTimeDialogFragment extends DialogFragment {

    public interface EditStartTimeDialogListener {
        void onDialogPositiveClick(DialogFragment dialogFragment, LocalTime localTime);
        void onDialogNegativeClick(DialogFragment dialogFragment);
    }

    private EditStartTimeDialogListener editStartTimeDialogListener;

    public void setEditStartTimeDialogListener(EditStartTimeDialogListener editStartTimeDialogistener) {
        this.editStartTimeDialogListener = editStartTimeDialogistener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_edit_start_time, null);
        EditText etHour = (EditText) view.findViewById(R.id.etEditStartTimeHrs);
        EditText etMin = (EditText) view.findViewById(R.id.etEditStartTimeMins);
        builder.setView(view)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LocalTime localTime = new LocalTime(Integer.parseInt(etHour.getText().toString()), Integer.parseInt(etMin.getText().toString()));
                        editStartTimeDialogListener.onDialogPositiveClick(EditStartTimeDialogFragment.this, localTime);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editStartTimeDialogListener.onDialogNegativeClick(EditStartTimeDialogFragment.this);
                    }
                });
        return builder.create();
    }
}
