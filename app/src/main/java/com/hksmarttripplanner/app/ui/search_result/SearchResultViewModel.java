package com.hksmarttripplanner.app.ui.search_result;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.Single;
import retrofit2.HttpException;

public class SearchResultViewModel extends BaseViewModel {
    private final MutableLiveData<Place> selectPlace = new MutableLiveData();

    public SearchResultViewModel(@NonNull Application application) {
        super(application);
    }

    public void makePlaceSelection(Place place) {
        selectPlace.setValue(place);
    }

    public Single<List<Place>> getSearchResult(String keyword, String district, String type, String category, String budget) {
        return getAppRepository().getSearchPlacesCall(keyword, district, type, category, budget)
                .doOnSubscribe(disposable -> setIsLoading(true)).
                        doOnError(throwable -> {
                            throwable.printStackTrace();
                            setIsLoading(false);
                            if (throwable instanceof SocketTimeoutException) {
                                getSnackbarText().onNext(R.string.network_timeout);
                            } else if (throwable instanceof HttpException) {
                                getSnackbarText().onNext(R.string.search_no_result);
                            }
                        });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new SearchResultViewModel(application);
        }
    }
}
