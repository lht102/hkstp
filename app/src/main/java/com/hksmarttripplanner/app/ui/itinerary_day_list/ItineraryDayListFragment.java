package com.hksmarttripplanner.app.ui.itinerary_day_list;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.databinding.FragmentItineraryDayListBinding;
import com.hksmarttripplanner.app.ui.adapters.ItineraryDayAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceListFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceViewModel;
import com.hksmarttripplanner.app.ui.itinerary_route_map.ItineraryRouteMapFragment;
import com.hksmarttripplanner.app.ui.main.MainActivity;
import com.hksmarttripplanner.app.utils.MyDateUtils;

import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItineraryDayListFragment extends BaseFragment {
    public static final String TAG = ItineraryDayListFragment.class.getSimpleName();
    private static final String ARG_ITINERARY_ID = "ITINERARY_ID";
    private static final String ARG_ITINERARY_TITLE = "ITINERARY_TITLE";

    private FragmentItineraryDayListBinding mFragmentItineraryDayListBinding;
    private ItineraryDayPlaceViewModel viewModel;
    private ItineraryDayAdapter itineraryDayAdapter = new ItineraryDayAdapter(new ArrayList<>());

    Listener listener;
    public interface Listener {
        public void addOptimizedItinerary(Itinerary itinerary);
    }

    public static ItineraryDayListFragment newInstance(int itineraryId, String title) {
        Log.d(TAG, "newInstance: called");
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ITINERARY_ID, itineraryId);
        bundle.putString(ARG_ITINERARY_TITLE, title);
        ItineraryDayListFragment fragment = new ItineraryDayListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: called");
        mFragmentItineraryDayListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_itinerary_day_list, container, false);
        return mFragmentItineraryDayListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: called");
        ItineraryDayPlaceViewModel.Factory factory = new ItineraryDayPlaceViewModel.Factory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(getActivity(), factory).get(ItineraryDayPlaceViewModel.class);
        mFragmentItineraryDayListBinding.setViewModel(viewModel);
        viewModel.setItineraryId(getArguments().getInt(ARG_ITINERARY_ID));

        setupRecyclerView();
        setupSwipeRefreshLayout();
        setupToolbar();
        setupFloatingActionButton();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +  " must implement Listener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");

        getCompositeDisposable().add(itineraryDayAdapter.getBudgetPublishSubject()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aDouble -> {
                    DecimalFormat df = new DecimalFormat("#.##");
                    mFragmentItineraryDayListBinding.tvDayTotalBudget.setText("Total Budget: $" + df.format(aDouble) + " " + viewModel.getAppRepository().getMyCurrency());
                }));

        getCompositeDisposable().add(itineraryDayAdapter.getDurationPublishSubject()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(duration -> {
                    PeriodFormatter formatter = new PeriodFormatterBuilder()
                            .appendHours()
                            .appendSuffix("hrs")
                            .appendMinutes()
                            .appendSuffix("mins")
                            .toFormatter();
                    mFragmentItineraryDayListBinding.tvTotalDuration.setText("Total Duration:" + formatter.print(duration.toPeriod()));
                }));

        getCompositeDisposable().add(viewModel.getSnackbarText()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showSnackBar,
                        throwable -> Log.e(getTag(), "Unable to display SnackBar")));

        getCompositeDisposable().add(viewModel.getItineraryCall()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itinerary -> {
                    List<ItineraryDay> itineraryDays = itinerary.getDays();
                    Collections.sort(itineraryDays);
                    for (int i = 0; i < itineraryDays.size(); i++) {
                        ItineraryDay itineraryDay = itineraryDays.get(i);
                        itineraryDay.setDay(i + 1);
                    }
                    viewModel.setItinerary(itinerary);
                    itineraryDayAdapter.setItemlist(itineraryDays);
                    for (int i = 0; i < itineraryDays.size(); i++) {
                        ItineraryDay itineraryDay = itineraryDays.get(i);

                        int j = i;
                        viewModel.getItineraryDayDetailRoute(itineraryDay.getId())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .onErrorReturn(throwable -> {
                                    List<DetailRoute> detailRoutes = new ArrayList<>();
                                    return detailRoutes;
                                })
                                .subscribe(detailRoutes -> {
                                    itineraryDayAdapter.setDetailRoute(detailRoutes, itineraryDay.getId(), j);
                                });
                    }
                }, throwable -> {
                    Log.e(getTag(), throwable.getClass().getName());
                    throwable.printStackTrace();
                }));

        getCompositeDisposable().add(itineraryDayAdapter.getOnClickDeletePos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itineraryDay -> {
                    itineraryDayAdapter.updateDayOrder();
                    viewModel.deleteItineraryDayById(itineraryDay.getId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {
                                viewModel.getSnackbarText().onNext(R.string.itinerary_day_delete_msg);
                            });
                }, throwable -> {

                }));

        getCompositeDisposable().add(itineraryDayAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itineraryDay -> {
                    Log.d(TAG, "Itinerary day id: " + itineraryDay.getId());
                    ((MainActivity) getActivity()).stateItineraryDayId = itineraryDay.getId();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, ItineraryDayPlaceListFragment.newInstance(itineraryDay.getId(), MyDateUtils.getFormattedDate(itineraryDay.getDate())), ItineraryDayPlaceListFragment.TAG)
                            .addToBackStack(TAG)
                            .commit();
                }));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_itinerary_opt, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setupSwipeRefreshLayout() {
        mFragmentItineraryDayListBinding.srlItineraryDays.setOnRefreshListener(() -> {
            itineraryDayAdapter.clear();
            getCompositeDisposable().add(viewModel.getItineraryCall()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((itinerary, throwable) -> {
                        itineraryDayAdapter.clear();
                        viewModel.setItinerary(new Itinerary());
                        List<ItineraryDay> itineraryDays = itinerary.getDays();
                        Collections.sort(itineraryDays);
                        for (int i = 0; i < itineraryDays.size(); i++) {
                            ItineraryDay itineraryDay = itineraryDays.get(i);
                            itineraryDay.setDay(i + 1);
                        }
                        viewModel.setItinerary(itinerary);
                        itineraryDayAdapter.setItemlist(itineraryDays);
                        for (int i = 0; i < itineraryDays.size(); i++) {
                            ItineraryDay itineraryDay = itineraryDays.get(i);

                            int j = i;
                            viewModel.getItineraryDayDetailRoute(itineraryDay.getId())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onErrorReturn(throwable1 -> {
                                        List<DetailRoute> detailRoutes = new ArrayList<>();
                                        return detailRoutes;
                                    })
                                    .subscribe(detailRoutes -> {
                                        itineraryDayAdapter.setDetailRoute(detailRoutes, itineraryDay.getId(), j);
                                    });
                        }
                        mFragmentItineraryDayListBinding.srlItineraryDays.setRefreshing(false);
                    }));
        });
    }

    public void setupRecyclerView() {
        mFragmentItineraryDayListBinding.rvItineraryDay.setLayoutManager(new LinearLayoutManager(getContext()));
        mFragmentItineraryDayListBinding.rvItineraryDay.setAdapter(itineraryDayAdapter);
        mFragmentItineraryDayListBinding.rvItineraryDay.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                FloatingActionButton fab = mFragmentItineraryDayListBinding.fabAddItineraryDay;
                if (dy > 0 && fab.isShown()) {
                    fab.hide();
                }
                else if (dy < 0 && !fab.isShown()) {
                    fab.show();
                }
            }
        });
    }

    public void setupToolbar() {
        mFragmentItineraryDayListBinding.tbItineraryList.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        mFragmentItineraryDayListBinding.tbItineraryList.setTitle(getArguments().getString(ARG_ITINERARY_TITLE));
        mFragmentItineraryDayListBinding.tbItineraryList.inflateMenu(R.menu.menu_itinerary_opt);
        mFragmentItineraryDayListBinding.tbItineraryList.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_optimize:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Optimize this Itinerary");
                    builder.setPositiveButton(R.string.mdtp_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getCompositeDisposable().add(viewModel.optimizeItinerary()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(itinerary -> {
                                        showSnackBar(R.string.optimized_itinerary_created_msg);
                                        listener.addOptimizedItinerary(itinerary);
                                    }));
                        }
                    });
                    builder.setNegativeButton(R.string.mdtp_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return true;
                case R.id.action_route_map:
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, ItineraryRouteMapFragment.newInstance(), ItineraryRouteMapFragment.TAG)
                            .addToBackStack(TAG)
                            .commit();
                    return true;
                default:
                    return true;
            }
        });
    }

    public void setupFloatingActionButton() {
        mFragmentItineraryDayListBinding.fabAddItineraryDay.setOnClickListener(v -> {
            getCompositeDisposable().add(viewModel.createItineraryDay()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((itineraryDay, throwable) -> {
                        itineraryDayAdapter.addItineraryDay(itineraryDay);
                        showSnackBar(R.string.itinerary_day_created_msg);
                    }));
        });
    }
}
