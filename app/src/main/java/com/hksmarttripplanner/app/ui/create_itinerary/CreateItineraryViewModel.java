package com.hksmarttripplanner.app.ui.create_itinerary;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.util.Date;

import io.reactivex.Completable;
import io.reactivex.Single;

public class CreateItineraryViewModel extends BaseViewModel {
    public CreateItineraryViewModel(@NonNull Application application) {
        super(application);
    }

    public Single<Itinerary> createItinerary(String title, double budget, String startDate, String endDate) {
        return getAppRepository().createUserItineraryById(getAppRepository().getMyUserId(), title, budget, startDate, endDate);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new CreateItineraryViewModel(application);
        }
    }
}
