package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.Comment;
import com.hksmarttripplanner.app.databinding.ItemCommentBinding;

import java.util.List;
import java.util.Objects;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
    private List<Comment> commentList;

    public CommentAdapter(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @NonNull
    @Override
    public CommentAdapter.CommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemCommentBinding itemCommentBinding = ItemCommentBinding.inflate(layoutInflater, viewGroup, false);
        return new CommentViewHolder(itemCommentBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder commentViewHolder, int i) {
        Comment comment = commentList.get(i);
        commentViewHolder.bind(comment);

        commentViewHolder.itemCommentBinding.tvCommentUserEmail.setText(comment.getEmail() + ":");
        commentViewHolder.itemCommentBinding.tvCommentDate.setText(comment.getCreated_at());
    }

    @Override
    public int getItemCount() {
        return commentList == null ? 0 : commentList.size();
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder {
        private final ItemCommentBinding itemCommentBinding;

        public CommentViewHolder(ItemCommentBinding itemCommentBinding) {
            super(itemCommentBinding.getRoot());
            this.itemCommentBinding = itemCommentBinding;
        }

        public void bind(Comment comment) {
            itemCommentBinding.setComment(comment);
            itemCommentBinding.executePendingBindings();
        }
    }

    public void setItemlist(List<Comment> newItemList) {
        if (newItemList == null) {
            int oldSize = commentList.size();
            commentList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new CommentDiffCallBack(commentList, newItemList));
            commentList.clear();
            commentList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    public void clear() {
        commentList.clear();
        notifyDataSetChanged();
    }

    private class CommentDiffCallBack extends DiffUtil.Callback {
        private List<Comment> oldList;
        private List<Comment> newList;

        public CommentDiffCallBack(List<Comment> oldList, List<Comment> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            Comment oldItinerary = oldList.get(oldItemPos);
            Comment newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
