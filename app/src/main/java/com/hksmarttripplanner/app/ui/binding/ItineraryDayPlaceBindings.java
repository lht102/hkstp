package com.hksmarttripplanner.app.ui.binding;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.Leg;
import com.hksmarttripplanner.app.data.source.remote.api.response.Route;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;

import java.util.List;

public class ItineraryDayPlaceBindings {

    @BindingAdapter("route")
    public static void setRouteDescription(TextView textView, List<Route> routes) {
        if (routes != null) {
            String str = "";
            for (Route route : routes) {
                for (Leg leg : route.getLegs()) {
                    if (leg.getDepartureTime() != null && leg.getArrivalTime() != null) {
                        str += "Traffic Time: " + leg.getDepartureTime().getText()
                                + " to " + leg.getArrivalTime().getText();
                    }
                    str += "\nDistance: " + leg.getDistance().getText()
                            + ", Duration: " + leg.getDuration().getText() + "\n";

                    for (Step step : leg.getSteps()) {
                        str += "\t" + step.getHtmlInstructions() + "(" + step.getDistance().getText() + ", " + step.getDuration().getText()  + ")\n";
                    }
                }
            }
            textView.setText(str);
        }
    }

    @BindingAdapter("placeDescription")
    public static void setPlaceDescription(TextView textView, Place place) {
        String str = "";
        str += place.getAddress() + "\n\n";
        str += "Start Time: " + place.getStartTime() + " Stay for: " + place.getDuration() + "mins\n"
                + "Budget: $" + place.getBudget();

        textView.setText(str);
    }
}
