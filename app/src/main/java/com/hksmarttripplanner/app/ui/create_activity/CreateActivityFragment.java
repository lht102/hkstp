package com.hksmarttripplanner.app.ui.create_activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentCreateActivityBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.main.SearchPlaceFragment;
import com.hksmarttripplanner.app.ui.my_favorite.MyFavoriteFragment;

public class CreateActivityFragment extends BaseFragment {
    public static final String TAG = CreateActivityFragment.class.getSimpleName();
    private static final String ARG_ITINERARY_DAY_ID = "ITINERARY_DAY_ID";
    private static final int REQUEST_PLACE_PICKER = 1;

    private FragmentCreateActivityBinding mFragmentCreateActivityBinding;
    private CreateActivityViewModel mCreateActivityViewModel;

    public static CreateActivityFragment newInstance(int itineraryDayId) {
        Bundle args = new Bundle();
        args.putInt(ARG_ITINERARY_DAY_ID, itineraryDayId);
        CreateActivityFragment fragment = new CreateActivityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentCreateActivityBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_activity, container, false);
        return mFragmentCreateActivityBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CreateActivityViewModel.Factory factory = new CreateActivityViewModel.Factory(getActivity().getApplication());
        mCreateActivityViewModel = ViewModelProviders.of(this, factory).get(CreateActivityViewModel.class);
        mFragmentCreateActivityBinding.setViewModel(mCreateActivityViewModel);

        setupMyFavoriteCard();
        setupSearchPlaceCard();
    }

    public void setupMyFavoriteCard() {
        mFragmentCreateActivityBinding.cvMyfavorite.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainer, MyFavoriteFragment.newInstance(0, getArguments().getInt(ARG_ITINERARY_DAY_ID)), MyFavoriteFragment.TAG)
                    .addToBackStack(TAG)
                    .commit();
        });
    }

    public void setupSearchPlaceCard() {
        mFragmentCreateActivityBinding.cvSerchPlace.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainer, new SearchPlaceFragment(), SearchPlaceFragment.TAG)
                    .addToBackStack(TAG)
                    .commit();
        });
    }
}
