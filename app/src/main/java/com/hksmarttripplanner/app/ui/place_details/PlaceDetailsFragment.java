package com.hksmarttripplanner.app.ui.place_details;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.local.model.Time;
import com.hksmarttripplanner.app.databinding.FragmentPlaceDetailsAltBinding;
import com.hksmarttripplanner.app.databinding.FragmentPlaceDetailsBinding;
import com.hksmarttripplanner.app.ui.adapters.CommentAdapter;
import com.hksmarttripplanner.app.ui.adapters.ItinerarySelectionAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.google_map.GoogleMapFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceListFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_selection.ItineraryDaySelectionFragment;
import com.hksmarttripplanner.app.ui.itinerary_selection.ItinerarySelectionFragment;
import com.hksmarttripplanner.app.ui.main.MainActivity;


import java.text.DecimalFormat;
import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PlaceDetailsFragment extends BaseFragment {
    public static final String TAG = PlaceDetailsFragment.class.getSimpleName();
    private String Status;
    private static Place place;
    private Boolean booleans;
    private PlaceDetailsViewModel placeDetailsViewModel;
    private FragmentPlaceDetailsAltBinding fragmentPlaceDetailsBinding;
    private CommentAdapter commentAdapter;

    private ItinerarySelectionAdapter itinerarySelectionAdapter = new ItinerarySelectionAdapter();
    private BottomSheetBehavior bottomSheetBehavior;


    public static PlaceDetailsFragment newInstance(Place place, String status) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("Place", place);
        bundle.putString("Status", status);
        PlaceDetailsFragment placeDetailsFragment = new PlaceDetailsFragment();
        placeDetailsFragment.setArguments(bundle);
        return placeDetailsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentPlaceDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_place_details_alt, container, false);
        commentAdapter = new CommentAdapter(new ArrayList<>());
        return fragmentPlaceDetailsBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PlaceDetailsViewModel.Factory factory = new PlaceDetailsViewModel.Factory(getActivity().getApplication());
        placeDetailsViewModel = ViewModelProviders.of(getActivity(), factory).get(PlaceDetailsViewModel.class);
        fragmentPlaceDetailsBinding.setPlaceDetailsViewModel(placeDetailsViewModel);

        ConstraintLayout bottomSheet = fragmentPlaceDetailsBinding.bottomSheetItinerarySelection;
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        setupItinerarySelectionRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(placeDetailsViewModel.getSnackbarText()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showSnackBar,
                        throwable -> Log.e(getTag(), "Unable to display place details.")));

        setFragmentView();
        setBtnGoogleMapActivity();
        setBackSearchResultActivity();
        setBtnAddOrRemoveFavourite();
        setTableLayout();
        setAddPlaceToItinerary();
        getBoolean();
        setBtnWriteComment();

        Status = getArguments().getString("Status");
        if (Status.equals("Search")) {
            fragmentPlaceDetailsBinding.etWriteComment.setVisibility(View.GONE);
            fragmentPlaceDetailsBinding.btnWriteComment.setVisibility(View.GONE);
        }

        getCompositeDisposable().add(itinerarySelectionAdapter.getOnClickItineraryDay()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMapSingle(itineraryDay ->  placeDetailsViewModel.getAppRepository().createActivity(itineraryDay.getId(), place.getId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()))
                .subscribe(place -> {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    showSnackBar(R.string.activity_created_msg);
                }));

        getCompositeDisposable().add(placeDetailsViewModel.getCommentByPlaceId(place.getId())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(comments -> {
                    if (!comments.isEmpty()) {
                        commentAdapter.setItemlist(comments);
                    } else {
                        fragmentPlaceDetailsBinding.tvPlaceNoComment.setVisibility(View.VISIBLE);
                    }
                }));

        setRecyclerView();

        if (place.getTimes().isEmpty()) {
            fragmentPlaceDetailsBinding.tvBusinessHours.setVisibility(View.GONE);
            fragmentPlaceDetailsBinding.tlPlaceDetails.setVisibility(View.GONE);
            fragmentPlaceDetailsBinding.tvBusinessHours.setVisibility(View.GONE);
        }
    }

    public void setFragmentView() {
        place = getArguments().getParcelable("Place");
        String image = place.getImage();

        if (place.getType().equalsIgnoreCase("attraction")) {
            image = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + place.getImage() + "&key=AIzaSyCnsjBC-bI0dZLzFNyFJUTv7v5XjUvSo8Y";
        }

        Context context = fragmentPlaceDetailsBinding.ivPlaceImage.getContext();
        Glide.with(context).load(image).into(fragmentPlaceDetailsBinding.ivPlaceImage);
        fragmentPlaceDetailsBinding.tbPlaceDetails.setTitle(place.getName());
        fragmentPlaceDetailsBinding.tvPlaceDetailName.setText(place.getName());
        fragmentPlaceDetailsBinding.tvPlaceDetailAddress.setText(place.getAddress());
        if (place.getPhone() != null) {
            fragmentPlaceDetailsBinding.tvPlaceDetailPhone.setText(place.getPhone());
        } else {
            fragmentPlaceDetailsBinding.tvPlaceDetailPhone.setText("No Record");
        }
        if (place.getBudget() != null && String.valueOf(place.getBudget().getAmount()).equals("0.0")) {
            fragmentPlaceDetailsBinding.tvPlaceDetailBudget.setText("No Record");
        } else if (place.getBudget() != null) {
            DecimalFormat df = new DecimalFormat(".#");
            fragmentPlaceDetailsBinding.tvPlaceDetailBudget.setText("Average " + String.valueOf(df.format(place.getBudget().getAmount())) + " " + place.getBudget().getCurrency());
        }
//        else {
//            fragmentPlaceDetailsBinding.tvPlaceDetailBudget.setText("Adult: " + String.valueOf(place.getBudget().getAmount()) +
//                    "\nChild: " + String.valueOf(place.getSpecialBudget().getAmount()));
//        }
        if (String.valueOf(place.getRating()).equals("0.0")) {
            fragmentPlaceDetailsBinding.tvPlaceDetailRating.setText("No Record");
        } else {
            fragmentPlaceDetailsBinding.tvPlaceDetailRating.setText(String.valueOf(place.getRating()) + " / 5.0");
        }
        if (place.getDescription() != null) {
            fragmentPlaceDetailsBinding.tvPlaceDetailDescription.setText(place.getDescription());
        } else {
            fragmentPlaceDetailsBinding.tvPlaceDetailDescription.setText("No Description");
        }
    }

    public void setTableLayout() {
        if (!place.getTimes().isEmpty()) {
            resort();
            for (int i = 0; i < place.getTimes().size(); i++) {
                TableRow tableRow = new TableRow(getActivity());
                TextView textViewDay = new TextView(getActivity());
                setTextStyleAndText(textViewDay, place.getTimes().get(i).getWeekday());
                TextView textViewOpen = new TextView(getActivity());
                TextView textViewClose = new TextView(getActivity());
                String open = "";
                String close = "";
                for (int j = 0; j < place.getTimes().get(i).getHours().size(); j++) {
                    if (j != 0) {
                        open += "\n";
                        close += "\n";
                    }
                    open += place.getTimes().get(i).getHours().get(j).getOpen().substring(0, 8);
                    close += place.getTimes().get(i).getHours().get(j).getClose().substring(0, 8);
                }
                setTextStyleAndText(textViewOpen, open);
                setTextStyleAndText(textViewClose, close);
                tableRow.addView(textViewDay);
                tableRow.addView(textViewOpen);
                tableRow.addView(textViewClose);
                fragmentPlaceDetailsBinding.tlPlaceDetails.addView(tableRow);
            }
        }
    }

    public void setTextStyleAndText(TextView textView, String string) {
        textView.setText(string);
        textView.setTextSize(19);
        textView.setTextColor(Color.rgb(0, 0, 0));
        textView.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);
    }

    public void setBackSearchResultActivity() {
        fragmentPlaceDetailsBinding.tbPlaceDetails.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    public void setupItinerarySelectionRecyclerView() {
        fragmentPlaceDetailsBinding.rvItinerarySelection.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentPlaceDetailsBinding.rvItinerarySelection.setAdapter(itinerarySelectionAdapter);

        fragmentPlaceDetailsBinding.ivSelectionClose.setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        });
    }

    public void setBtnGoogleMapActivity() {
        fragmentPlaceDetailsBinding.btnGoogleMapActivity.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, GoogleMapFragment
                            .newInstance(place.getName(), place.getLatitude(), place.getLongitude()), GoogleMapFragment.TAG)
                    .addToBackStack(TAG).commit();
        });
    }

    public void setBtnAddOrRemoveFavourite() {
        fragmentPlaceDetailsBinding.btnAddOrRemove.setOnClickListener(v -> {
            if (booleans != null && !booleans) {
                getCompositeDisposable().add(placeDetailsViewModel.addToFavorite(place.getId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            fragmentPlaceDetailsBinding.btnAddOrRemove.setHint("Remove Favorite");
                            booleans = true;
                        }, throwable -> {
                            throwable.printStackTrace();
                        }));
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Make sure remove this place from your favorite list?").setCancelable(false)
                        .setNegativeButton("No", (dialog, which) -> dialog.cancel())
                        .setPositiveButton("Yes", (dialog, which) -> {
                            getCompositeDisposable().add(placeDetailsViewModel.removeFromFavorite(place.getId())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> {
                                        fragmentPlaceDetailsBinding.btnAddOrRemove.setHint("Add Favorite");
                                        booleans = false;
                                    }, throwable -> {
                                        throwable.printStackTrace();
                                    }));
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    public void setBtnWriteComment() {
        fragmentPlaceDetailsBinding.btnWriteComment.setOnClickListener(v -> {
            if (!fragmentPlaceDetailsBinding.etWriteComment.getText().toString().equals("")) {
                getCompositeDisposable().add(placeDetailsViewModel.writeComment(place.getId(),
                        fragmentPlaceDetailsBinding.etWriteComment.getText().toString())
                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            fragmentPlaceDetailsBinding.tvPlaceNoComment.setVisibility(View.INVISIBLE);
                            fragmentPlaceDetailsBinding.etWriteComment.setText("");
                            commentAdapter.clear();
                            getCompositeDisposable().add(placeDetailsViewModel.getCommentByPlaceId(place.getId())
                                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(comments -> {
                                        commentAdapter.setItemlist(comments);
                                    }));
                        }));
            } else {
                getCompositeDisposable().add(placeDetailsViewModel.getSnackbarText()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::showSnackBar,
                                throwable -> Log.e(getTag(), "Please input the comment first.")));
            }
        });
    }

    public void setAddPlaceToItinerary() {
        fragmentPlaceDetailsBinding.btnPlaceAddToItinerary.setOnClickListener(v -> {
            int itineraryId = ((MainActivity) getActivity()).stateItineraryId;
            int itineraryDayId = ((MainActivity) getActivity()).stateItineraryDayId;

            Log.d(TAG, "itineraryId: " + itineraryId);
            Log.d(TAG, "itineraryDayId: " + itineraryDayId);


            if (itineraryDayId > 0) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                ItineraryDayPlaceListFragment fragment = (ItineraryDayPlaceListFragment) fm.findFragmentByTag(ItineraryDayPlaceListFragment.TAG);
                fragment.addActivity(place.getId());
                fm.popBackStack(ItineraryDayPlaceListFragment.TAG, 1);
            } else {
                placeDetailsViewModel.getAppRepository().getUserItineraries(placeDetailsViewModel.getAppRepository().getMyUserId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> {
                            itinerarySelectionAdapter.setItemList(null);
                            fragmentPlaceDetailsBinding.pgSelectionLoading.setVisibility(View.VISIBLE);
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
                        })
                        .subscribe((itineraries, throwable) -> {
                            fragmentPlaceDetailsBinding.pgSelectionLoading.setVisibility(View.INVISIBLE);
                            itinerarySelectionAdapter.setItemList(itineraries);
                        });
            }
        });
    }

    public void getBoolean() {
        getCompositeDisposable().add(placeDetailsViewModel.
                isFavorite(place.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((booleans) -> {
                    this.booleans = booleans;
                    if (booleans) {
                        fragmentPlaceDetailsBinding.btnAddOrRemove.setHint("Remove Favorite");
                    }
                }, throwable ->
                        throwable.printStackTrace()));
    }

    public void resort() {
        for (int i = 0; i < place.getTimes().size(); i++) {
            String week [] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Pub"};

            Time time;
            if (place.getTimes().get(i).getWeekday().equals(week[0]) && i != 0) {
                time = place.getTimes().remove(i);
                place.getTimes().add(0, time);
                i--;
            } else if (place.getTimes().get(i).getWeekday().equals(week[1]) && i != 1) {
                time = place.getTimes().remove(i);
                place.getTimes().add(1, time);
                i--;
            } else if (place.getTimes().get(i).getWeekday().equals(week[2]) && i != 2) {
                time = place.getTimes().remove(i);
                place.getTimes().add(2, time);
                i--;
            } else if (place.getTimes().get(i).getWeekday().equals(week[3]) && i != 3) {
                time = place.getTimes().remove(i);
                place.getTimes().add(3, time);
                i--;
            } else if (place.getTimes().get(i).getWeekday().equals(week[4]) && i != 4) {
                time = place.getTimes().remove(i);
                place.getTimes().add(4, time);
                i--;
            } else if (place.getTimes().get(i).getWeekday().equals(week[5]) && i != 5) {
                time = place.getTimes().remove(i);
                place.getTimes().add(5, time);
                i--;
            } else if (place.getTimes().get(i).getWeekday().equals(week[6]) && i != 6) {
                time = place.getTimes().remove(i);
                place.getTimes().add(6, time);
                i--;
            } else if (place.getTimes().get(i).getWeekday().equals(week[7]) && i != 7) {
                time = place.getTimes().remove(i);
                place.getTimes().add(7, time);
                i--;
            }

            for (int j = 0; j < place.getTimes().size(); j++) {
                if (j == 0) { place.getTimes().get(j).setWeekday("Monday"); }
                if (j == 1) { place.getTimes().get(j).setWeekday("Tuesday"); }
                if (j == 2) { place.getTimes().get(j).setWeekday("Wednesday"); }
                if (j == 3) { place.getTimes().get(j).setWeekday("Thursday"); }
                if (j == 4) { place.getTimes().get(j).setWeekday("Friday"); }
                if (j == 5) { place.getTimes().get(j).setWeekday("Saturday"); }
                if (j == 6) { place.getTimes().get(j).setWeekday("Sunday"); }
                if (j == 7) { place.getTimes().get(j).setWeekday("Holiday"); }
            }
        }
    }

    public void setRecyclerView() {
        fragmentPlaceDetailsBinding.rcUserComment.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentPlaceDetailsBinding.rcUserComment.setAdapter(commentAdapter);
    }
}
