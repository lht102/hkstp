package com.hksmarttripplanner.app.ui.itinerary_day_route_map;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.ui.IconGenerator;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.EncodedPolyline;
import com.hksmarttripplanner.app.data.source.remote.api.response.Leg;
import com.hksmarttripplanner.app.data.source.remote.api.response.Route;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;
import com.hksmarttripplanner.app.databinding.FragmentItineraryDayRouteMapBinding;
import com.hksmarttripplanner.app.ui.adapters.PlaceRouteMapAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceViewModel;
import com.hksmarttripplanner.app.utils.DetailRouteUtils;
import com.hksmarttripplanner.app.utils.MyColorUtils;
import com.hksmarttripplanner.app.utils.MyDateUtils;
import com.hksmarttripplanner.app.utils.MyStringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItineraryDayRouteMapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnPolylineClickListener {
    public static final String TAG = ItineraryDayRouteMapFragment.class.getSimpleName();
    private FragmentItineraryDayRouteMapBinding binding;
    private ItineraryDayPlaceViewModel viewModel;
    private GoogleMap map;
    private List<Polyline> polylines;
    private PlaceRouteMapAdapter adapter;

    public static ItineraryDayRouteMapFragment newInstance() {
        Bundle args = new Bundle();
        ItineraryDayRouteMapFragment fragment = new ItineraryDayRouteMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_itinerary_day_route_map, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ItineraryDayPlaceViewModel.Factory factory = new ItineraryDayPlaceViewModel.Factory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(getActivity(), factory).get(ItineraryDayPlaceViewModel.class);
        binding.setViewModel(viewModel);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_route_map);
        mapFragment.getMapAsync(this);

        LinearLayout bottomSheet = binding.bottomSheetRouteMap;
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        setupRecyclerView();
        setupToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        polylines = new ArrayList<>();
        updatePolyline();

        googleMap.setOnPolylineClickListener(this);
        getCompositeDisposable().add(viewModel.getItineraryDaySubject()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itineraryDay -> {
                    updatePolyline();
                }));

        getCompositeDisposable().add(adapter.getOnClickPos()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(i -> {
            if (polylines != null && polylines.size() > i) {
                moveToBounds(polylines.get(i));
            }
        }));
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        moveToBounds(polyline);
    }

    public void setupToolbar() {
        binding.tbRouteMap.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
        binding.tbRouteMap.setTitle(MyDateUtils.getFormattedDate(viewModel.getItineraryDay().getDate()));
    }

    public void setupRecyclerView() {
        adapter = new PlaceRouteMapAdapter(viewModel.getItineraryDay());
        binding.rvTrafficDetail.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvTrafficDetail.setAdapter(adapter);
    }

    public void updatePolyline() {
        for (int i = 0; i < polylines.size(); i++) {
            Polyline polyline = polylines.get(i);
            polyline.remove();
        }
        polylines.clear();
        map.clear();
        ItineraryDay itineraryDay = viewModel.getItineraryDay();
        if (itineraryDay != null && itineraryDay.getPlaces() != null) {
            LatLngBounds.Builder latLngBoundsBuilder = LatLngBounds.builder();
            IconGenerator iconFactory = new IconGenerator(getContext());

            List<Place> places = itineraryDay.getPlaces();

            for (int i = 0; i < places.size(); i++) {
                Place place = places.get(i);
                LatLng loc = new LatLng(place.getLatitude(), place.getLongitude());

                Marker marker = map.addMarker(new MarkerOptions()
                .position(loc)
                .title(place.getName())
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                marker.setZIndex(Float.MAX_VALUE);
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(String.valueOf(i + 1))));
            }



            for (int i = 0; i < places.size() - 1; i++) {
                Place place = places.get(i);
                List<LatLng> path = new ArrayList<>();
                if (DetailRouteUtils.isRouteExist(place.getDetailRoute())) {
                    List<Route> routes = place.getDetailRoute().getRoutes() ;
                    int seq = 1;
                    for (int z = 0; z < routes.size(); z++) {
                        Route route = routes.get(z);
                        List<Leg> legs = route.getLegs();
                        if (legs != null) {
                            for (int j = 0; j < legs.size(); j++) {
                                Leg leg = legs.get(j);

                                List<Step> steps = leg.getSteps();
                                if (steps != null) {
                                    for (int k = 0; k < steps.size(); k++, seq++) {
                                        Step step = steps.get(k);
                                        if (step.getSteps() != null && step.getSteps().size() > 0) {
                                            int subCount = 1;
                                            for (int l = 0; l < step.getSteps().size(); l++, subCount++) {
                                                Step innerStep = step.getSteps().get(l);
                                                EncodedPolyline polyline = innerStep.getPolyline();

                                                if (polyline != null) {
                                                    List<LatLng> latLngs = PolyUtil.decode(polyline.getPoints());
                                                    for (int m = 0; m < latLngs.size(); m++) {
                                                        LatLng latLng = latLngs.get(m);
                                                        latLngBoundsBuilder.include(latLng);
                                                        path.add(latLng);

                                                    }
                                                }
                                            }
                                        } else {
                                            EncodedPolyline polyline = step.getPolyline();

                                            if (polyline != null) {
                                                List<LatLng> latLngs = PolyUtil.decode(polyline.getPoints());
                                                for (int m = 0; m < latLngs.size(); m++) {
                                                    LatLng latLng = latLngs.get(m);
                                                    latLngBoundsBuilder.include(latLng);
                                                    path.add(latLng);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (path.size() > 0) {
                        List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(30), new Gap(20));
                        PolylineOptions opts = new PolylineOptions().addAll(path).color(MyColorUtils.getMyColorList()[i % MyColorUtils.getMyColorList().length]).width(10);
                        Polyline polyline = map.addPolyline(opts.pattern(pattern).geodesic(false));
                        polylines.add(polyline);

                    }
                }
            }

            LatLngBounds latLngBounds = latLngBoundsBuilder.build();
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 400));
        }
    }

    private void moveToBounds(Polyline p) {
        for (int i = 0; i < polylines.size(); i++) {
            polylines.get(i).setZIndex(1.0f);
        }
        p.setZIndex(10.0f);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        List<LatLng> arr = p.getPoints();
        for(int i = 0; i < arr.size();i++){
            builder.include(arr.get(i));
        }
        LatLngBounds bounds = builder.build();
        map.setPadding(100, 40, 40, 350);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);
        map.animateCamera(cu);
    }
}
