package com.hksmarttripplanner.app.ui.itinerary_day_list;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.util.Collections;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class ItineraryDayViewModel extends BaseViewModel {
    private final int mItineraryId;


    public ItineraryDayViewModel(@NonNull Application application, int itineraryId) {
        super(application);
        mItineraryId = itineraryId;
    }

    public Single<List<ItineraryDay>> getItineraryDays() {
        if (mItineraryId < 1) {
            return Single.error(new Exception("Itinerary id is invalid"));
        }
        return getAppRepository().getItineraryDaysByItineraryId(mItineraryId);
    }

    public Completable deleteItineraryDayById(int id) {
        return getAppRepository().deleteItineraryDayById(id);
    }

    public Single<ItineraryDay> createItineraryDay() {
        return getAppRepository().createItineraryDayByItineraryId(mItineraryId);
    }

    public Single<List<DetailRoute>> getItineraryDayDetailRoute(int itineraryDayId) {
        return getAppRepository().getItineraryDayDetailRoute(itineraryDayId);
    }

    public Single<Itinerary> optimizeItinerary() {
        return getAppRepository().optimizeItineraryById(mItineraryId);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;
        private final int mItineraryId;

        public Factory(Application application, int itineraryId) {
            this.application = application;
            mItineraryId = itineraryId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ItineraryDayViewModel(application, mItineraryId);
        }
    }
}
