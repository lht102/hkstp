package com.hksmarttripplanner.app.ui.itinerary_day_place_selection;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import io.reactivex.Single;

public class ItineraryDayPlaceSelectionViewModel extends BaseViewModel {
    private final int mItineraryDayId;

    public ItineraryDayPlaceSelectionViewModel(@NonNull Application application, int itineraryDayId) {
        super(application);
        mItineraryDayId = itineraryDayId;
    }

    public Single<ItineraryDay> getItineraryDay() {
        return getAppRepository().getItineraryDayById(mItineraryDayId);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;
        private final int mItineraryId;

        public Factory(Application application, int itineraryId) {
            this.application = application;
            mItineraryId = itineraryId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ItineraryDayPlaceSelectionViewModel(application, mItineraryId);
        }
    }
}
