package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.databinding.ItemUserBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private final PublishSubject<User> onClickItem = PublishSubject.create();
    private List<User> userList;

    public UserAdapter(List<User> userList) {
        this.userList = userList;
    }

    @NonNull
    @Override
    public UserAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemUserBinding itemUserBinding = ItemUserBinding.inflate(layoutInflater, viewGroup, false);
        return new UserViewHolder(itemUserBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.UserViewHolder userViewHolder, int i) {
        User user = userList.get(i);
        userViewHolder.bind(user);

        userViewHolder.itemUserBinding.crUserInformation.setOnClickListener(v -> {
            onClickItem.onNext(user);
        });
    }

    @Override
    public int getItemCount() {
        return userList == null ? 0 : userList.size();
    }

    public PublishSubject<User> getOnClickPos() {
        return onClickItem;
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        private final ItemUserBinding itemUserBinding;

        public UserViewHolder(ItemUserBinding itemUserBinding) {
            super(itemUserBinding.getRoot());
            this.itemUserBinding = itemUserBinding;
        }

        public void bind(User user) {
            itemUserBinding.setUser(user);
            itemUserBinding.executePendingBindings();
        }
    }

    public void setItemlist(List<User> newItemList) {
        if (newItemList == null) {
            int oldSize = userList.size();
            userList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new UserDiffCallBack(userList, newItemList));
            userList.clear();
            userList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    private class UserDiffCallBack extends DiffUtil.Callback {
        private List<User> oldList;
        private List<User> newList;

        public UserDiffCallBack(List<User> oldList, List<User> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            User oldItinerary = oldList.get(oldItemPos);
            User newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
