package com.hksmarttripplanner.app.ui.register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.hksmarttripplanner.app.R;

import java.util.LinkedList;

public class RegisterActivity extends AppCompatActivity {

    private Spinner spCurrency, spPrePlace1, spPrePlace2, spPrePlace3, spPrePlace4, spPrePlace5, spPreRest1, spPreRest2, spPreRest3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        LinkedList<String> currency = new LinkedList();
        currency.add(0, "Please select your currency.");
        currency.add("HKD");

        spCurrency = findViewById(R.id.spCurrency);

        ArrayAdapter<String> currencyAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, currency);
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCurrency.setAdapter(currencyAdapter);
        spCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
