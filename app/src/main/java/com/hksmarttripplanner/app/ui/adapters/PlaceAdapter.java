package com.hksmarttripplanner.app.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.databinding.ItemPlaceBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder> {
    private final PublishSubject<Place> onClickItem = PublishSubject.create();
    private List<Place> placeList;

    public PlaceAdapter(List<Place> placeList) {
        this.placeList = placeList;
    }

    @NonNull
    @Override
    public PlaceAdapter.PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemPlaceBinding itemPlaceBinding = ItemPlaceBinding.inflate(layoutInflater, viewGroup, false);
        return new PlaceViewHolder(itemPlaceBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder placeViewHolder, int i) {
        Place place = placeList.get(i);
        placeViewHolder.bind(place);

        placeViewHolder.itemPlaceBinding.crSearchResult.setOnClickListener(v -> {
            onClickItem.onNext(place);
        });

        String image = place.getImage();

        if (place.getType().equalsIgnoreCase("attraction")) {
            image = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + place.getImage() + "&key=AIzaSyCnsjBC-bI0dZLzFNyFJUTv7v5XjUvSo8Y";
        }

        Context context = placeViewHolder.itemPlaceBinding.ivSearchPlaceImage.getContext();
        Glide.with(context).load(image).into(placeViewHolder.itemPlaceBinding.ivSearchPlaceImage);
    }

    public PublishSubject<Place> getOnClickPos() {
        return onClickItem;
    }

    @Override
    public int getItemCount() {
        return placeList == null ? 0 : placeList.size();
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder {
        private final ItemPlaceBinding itemPlaceBinding;

        public PlaceViewHolder(ItemPlaceBinding itemPlaceBinding) {
            super(itemPlaceBinding.getRoot());
            this.itemPlaceBinding = itemPlaceBinding;
        }

        public void bind(Place place) {
            itemPlaceBinding.setPlace(place);
            itemPlaceBinding.executePendingBindings();
        }
    }

    public void setItemlist(List<Place> newItemList) {
        if (newItemList == null) {
            int oldSize = placeList.size();
            placeList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new PlaceDiffCallBack(placeList, newItemList));
            placeList.clear();
            placeList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    private class PlaceDiffCallBack extends DiffUtil.Callback {
        private List<Place> oldList;
        private List<Place> newList;

        public PlaceDiffCallBack(List<Place> oldList, List<Place> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            Place oldItinerary = oldList.get(oldItemPos);
            Place newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}