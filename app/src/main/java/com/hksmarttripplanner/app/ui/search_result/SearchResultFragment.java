package com.hksmarttripplanner.app.ui.search_result;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.databinding.FragmentSearchResultBinding;
import com.hksmarttripplanner.app.ui.adapters.PlaceAdapter;
import com.hksmarttripplanner.app.ui.adapters.UserAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.main.MainActivity;
import com.hksmarttripplanner.app.ui.place_details.PlaceDetailsFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SearchResultFragment extends BaseFragment {
    public static final String TAG = SearchResultFragment.class.getSimpleName();
    private ArrayList<String> arrayList = new ArrayList();
    private SearchResultViewModel searchResultViewModel;
    private FragmentSearchResultBinding fragmentSearchResultBinding;
    private PlaceAdapter placeAdapter;

    public static SearchResultFragment newInstance(String keyword, String district, String type, String category, String budget) {
        Bundle bundle = new Bundle();
        bundle.putString("keyword", keyword);
        bundle.putString("district", district);
        bundle.putString("type", type);
        bundle.putString("category", category);
        bundle.putString("budget", budget);
        SearchResultFragment searchResultFragment = new SearchResultFragment();
        searchResultFragment.setArguments(bundle);
        return searchResultFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSearchResultBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_result, container, false);
        placeAdapter = new PlaceAdapter(new ArrayList());
        return fragmentSearchResultBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SearchResultViewModel.Factory factory = new SearchResultViewModel.Factory(getActivity().getApplication());
        searchResultViewModel = ViewModelProviders.of(getActivity(), factory).get(SearchResultViewModel.class);
        fragmentSearchResultBinding.setSearchResultViewModel(searchResultViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(placeAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(place -> {
                    ((MainActivity) getActivity()).statePlaceId = place.getId();
                    searchResultViewModel.makePlaceSelection(place);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, PlaceDetailsFragment
                                    .newInstance(place, "Search"), PlaceDetailsFragment.TAG).addToBackStack(TAG).commit();
                }));

        setRecycleView();
        setBackSearchPlaceActivity();
    }

    public ArrayList<String> getSearchValue() {
        arrayList.add(getArguments().getString("keyword"));
        arrayList.add(getArguments().getString("district"));
        arrayList.add(getArguments().getString("type"));
        arrayList.add(getArguments().getString("category"));
        arrayList.add(getArguments().getString("budget"));
        return arrayList;
    }

    public void getSearchResult() {
        getCompositeDisposable().add(searchResultViewModel
                .getSearchResult(getSearchValue().get(0), getSearchValue().get(1),
                        getSearchValue().get(2), getSearchValue().get(3)
                , getSearchValue().get(4)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((places) -> {
                    if (!places.isEmpty()) {
                      fragmentSearchResultBinding.tbSearchResult.setTitle(places.size() + " Result(s) Matched");
                    } else {
                        fragmentSearchResultBinding.tbSearchResult.setTitle("No Match Result");
                    }
                    placeAdapter.setItemlist(places);
                }, throwable -> throwable.printStackTrace()));
    }

    public void setRecycleView() {
        fragmentSearchResultBinding.rvPlaceResult.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentSearchResultBinding.rvPlaceResult.setAdapter(placeAdapter);
        getSearchResult();
    }

    private void setBackSearchPlaceActivity() {
        fragmentSearchResultBinding.tbSearchResult.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }
}
