package com.hksmarttripplanner.app.ui.adapters;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.databinding.ItemPlaceRouteMapBinding;
import com.hksmarttripplanner.app.utils.DetailRouteUtils;
import com.hksmarttripplanner.app.utils.MyColorUtils;

import java.util.List;

import io.reactivex.subjects.PublishSubject;

public class PlaceRouteMapAdapter extends RecyclerView.Adapter<PlaceRouteMapAdapter.PlaceRouteMapViewHolder> {
    private ItineraryDay itineraryDay;
    private PublishSubject<Integer> onClickPos = PublishSubject.create();

    public PublishSubject<Integer> getOnClickPos() {
        return onClickPos;
    }

    public PlaceRouteMapAdapter(ItineraryDay itineraryDay) {
        this.itineraryDay = itineraryDay;
    }

    @NonNull
    @Override
    public PlaceRouteMapViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemPlaceRouteMapBinding binding = ItemPlaceRouteMapBinding.inflate(layoutInflater, viewGroup, false);
        return new PlaceRouteMapViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceRouteMapViewHolder viewHolder, int i) {
        List<Place> places = itineraryDay.getPlaces();
        if (places != null) {
            Place place = places.get(i);
            viewHolder.binding.cardView.setOnClickListener(v -> {
                onClickPos.onNext(i);
            });

            viewHolder.bind(place);
            viewHolder.binding.tvStartPlaceNumber.setText(String.valueOf(i + 1));
            viewHolder.binding.tvEndPlaceNumber.setText(String.valueOf(i + 1 + 1));
            GradientDrawable bgStartShape = (GradientDrawable)viewHolder.binding.tvStartPlaceNumber.getBackground();
            bgStartShape.setColor(MyColorUtils.getMyColorList()[i % MyColorUtils.getMyColorList().length]);

            GradientDrawable bgEndShape = (GradientDrawable)viewHolder.binding.tvEndPlaceNumber.getBackground();
            bgEndShape.setColor(MyColorUtils.getMyColorList()[i % MyColorUtils.getMyColorList().length]);

            if (DetailRouteUtils.isRouteExist(place.getDetailRoute())) {
                String travelMode = place.getTravelMode();
                if ("driving".equalsIgnoreCase(travelMode)) {
                    viewHolder.binding.ivDirectionIcon.setImageResource(R.drawable.ic_drive_eta_black_24dp);
                } else if ("walking".equalsIgnoreCase(travelMode)) {
                    viewHolder.binding.ivDirectionIcon.setImageResource(R.drawable.ic_directions_walk_black_24dp);
                } else if ("bicycling".equalsIgnoreCase(travelMode)) {
                    viewHolder.binding.ivDirectionIcon.setImageResource(R.drawable.ic_directions_bike_black_24dp);
                } else if ("transit".equalsIgnoreCase(travelMode)) {
                    viewHolder.binding.ivDirectionIcon.setImageResource(R.drawable.ic_directions_transit_black_24dp);
                }
                if (DetailRouteUtils.isOnlyWalking(place.getDetailRoute())) {
                    viewHolder.binding.ivDirectionIcon.setImageResource(R.drawable.ic_directions_walk_black_24dp);
                }
                viewHolder.binding.tvTrafficDes.setText(place.getDetailRoute().getRoutes().get(0).getLegs().get(0).getDuration().getText());
            }
        }

    }

    @Override
    public int getItemCount() {
        return itineraryDay == null || itineraryDay.getPlaces() == null ? 0 : itineraryDay.getPlaces().size() - 1;
    }

    public static class PlaceRouteMapViewHolder extends RecyclerView.ViewHolder {
        private final ItemPlaceRouteMapBinding binding;

        public PlaceRouteMapViewHolder(ItemPlaceRouteMapBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Place place) {
            binding.setPlace(place);
            binding.executePendingBindings();
        }
    }
}
