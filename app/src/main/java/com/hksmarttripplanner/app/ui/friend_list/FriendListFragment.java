package com.hksmarttripplanner.app.ui.friend_list;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.databinding.FragmentFriendListBinding;
import com.hksmarttripplanner.app.ui.adapters.UserAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.friend_information.FriendInformationFragment;
import com.hksmarttripplanner.app.ui.friend_invitation.FriendInvitationFragment;
import com.hksmarttripplanner.app.ui.friend_result.FriendResultFragment;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FriendListFragment extends BaseFragment {
    public static final String TAG = FriendListFragment.class.getSimpleName();
    private FriendListViewModel friendListViewModel;
    private FragmentFriendListBinding fragmentFriendListBinding;
    private UserAdapter userAdapter;
    private List<User> friend;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFriendListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_list, container, false);
        userAdapter = new UserAdapter(new ArrayList());
        return fragmentFriendListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FriendListViewModel.Factory factory = new FriendListViewModel.Factory(getActivity().getApplication());
        friendListViewModel = ViewModelProviders.of(getActivity(), factory).get(FriendListViewModel.class);
        fragmentFriendListBinding.setFriendListViewModel(friendListViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(friendListViewModel
                .getSentFriendList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((sentUsers) -> {
                    getCompositeDisposable().add(friendListViewModel
                            .getFirmFriendList().subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((firmUsers) -> {
                                List<Integer> sent = new LinkedList();
                                for (int i = 0; i < sentUsers.size(); i++) {
                                    sent.add(sentUsers.get(i).getId());
                                }
                                List<Integer> firm = new LinkedList();
                                for (int i = 0; i < firmUsers.size(); i++) {
                                    firm.add(firmUsers.get(i).getId());
                                }
                                friend = new LinkedList();
                                int countSent = sent.size();
                                int countFirm = firm.size();
                                for (int i = 0; i < countSent; i++) {
                                    for (int j = 0; j < countFirm; j++) {
                                        if (sentUsers.get(i).getId() == firmUsers.get(j).getId()) {
                                            friend.add(firmUsers.get(j));
                                        }
                                    }
                                }
                                userAdapter.setItemlist(friend);
                            }, Throwable::printStackTrace));
                }, Throwable::printStackTrace));

        getCompositeDisposable().add(userAdapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    friendListViewModel.makeUserSelection(user);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, FriendInformationFragment
                                    .newInstance(user), TAG).addToBackStack(TAG).commit();
                }));
        setRecyclerView();
        setupToolbar();
        setSearchFriendButton();
    }

    public void setupToolbar() {
        fragmentFriendListBinding.tbFriendList.inflateMenu(R.menu.menu_friend_setting);
        fragmentFriendListBinding.tbFriendList.setOnMenuItemClickListener(menuItem -> {
            if (menuItem.getItemId() == R.id.friend_settings) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentContainer, FriendInvitationFragment
                                .newInstance(), TAG).addToBackStack(TAG).commit();
            }
            return true;
        });
    }

    public void setRecyclerView() {
        fragmentFriendListBinding.rvFriendList.setLayoutManager(new LinearLayoutManager(getContext()));
        fragmentFriendListBinding.rvFriendList.setAdapter(userAdapter);
    }

    public void setSearchFriendButton() {
        fragmentFriendListBinding.btnSearchFriend.setOnClickListener(v -> {
            if (!fragmentFriendListBinding.etSearchFriend.getText().equals("")) {
                int id = 0;
                try {
                    id = Integer.parseInt(String.valueOf(fragmentFriendListBinding.etSearchFriend.getText()));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                if (id != 0) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentContainer, FriendResultFragment
                                    .newInstance(id), TAG).addToBackStack(TAG).commit();
                }
            }
        });
    }
}
