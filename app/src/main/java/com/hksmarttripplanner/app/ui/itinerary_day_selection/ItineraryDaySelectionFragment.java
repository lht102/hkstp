package com.hksmarttripplanner.app.ui.itinerary_day_selection;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.databinding.FragmentItineraryDaySelectionBinding;
import com.hksmarttripplanner.app.ui.adapters.SimpleItineraryDayAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceListFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_selection.ItineraryDayPlaceSelectionFragment;
import com.hksmarttripplanner.app.ui.main.MainActivity;
import com.hksmarttripplanner.app.ui.main.SearchPlaceFragment;
import com.hksmarttripplanner.app.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ItineraryDaySelectionFragment extends BaseFragment {
    public static final String TAG = ItineraryDaySelectionFragment.class.getSimpleName();
    private static final String ARG_ITINERARY_ID = "ITINERARY_ID";

    private FragmentItineraryDaySelectionBinding binding;
    private ItineraryDaySelectionViewModel viewModel;
    private SimpleItineraryDayAdapter adapter = new SimpleItineraryDayAdapter(new ArrayList<>());

    public static ItineraryDaySelectionFragment newInstance(int itineraryId) {
        Bundle args = new Bundle();
        ItineraryDaySelectionFragment fragment = new ItineraryDaySelectionFragment();
        args.putInt(ARG_ITINERARY_ID, itineraryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_itinerary_day_selection, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ItineraryDaySelectionViewModel.Factory factory = new ItineraryDaySelectionViewModel.Factory(getActivity().getApplication(), getArguments().getInt(ARG_ITINERARY_ID));
        viewModel = ViewModelProviders.of(this, factory).get(ItineraryDaySelectionViewModel.class);
        binding.setViewModel(viewModel);

        setupSwipeRefreshLayout();
        setupRecyclerView();
        setupToolbar();
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(adapter.getOnClickPos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itineraryDay -> {
                    int placeId = ((MainActivity) getActivity()).statePlaceId;
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    ItineraryDayPlaceListFragment fragment = (ItineraryDayPlaceListFragment) fm.findFragmentByTag(ItineraryDayPlaceListFragment.TAG);
                    if (fragment != null) {
                        fragment.addActivity(placeId);
                        FragmentUtils.popBackStackUntilTagExclusive(fm, ItineraryDayPlaceListFragment.TAG);
                    } else {

                    }
                }));

        getCompositeDisposable().add(viewModel.getItinerary()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itinerary -> {
                    List<ItineraryDay> itineraryDays = itinerary.getDays();
                    Collections.sort(itineraryDays);
                    for (int i = 0; i < itineraryDays.size(); i++) {
                        ItineraryDay itineraryDay = itineraryDays.get(i);
                        itineraryDay.setDay(i + 1);
                    }
                    binding.tbItineraryDaySelection.setTitle(itinerary.getTitle());
                    adapter.setItemList(itineraryDays);
                }));
    }

    public void setupSwipeRefreshLayout() {
        binding.srlItineraryDaySelection.setOnRefreshListener(() -> {
            adapter.clear();
            getCompositeDisposable().add(viewModel.getItinerary()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((itinerary, throwable) -> {
                        List<ItineraryDay> itineraryDays = itinerary.getDays();
                        Collections.sort(itineraryDays);
                        for (int i = 0; i < itineraryDays.size(); i++) {
                            ItineraryDay itineraryDay = itineraryDays.get(i);
                            itineraryDay.setDay(i + 1);
                        }
                        adapter.setItemList(itineraryDays);
                        binding.tbItineraryDaySelection.setTitle(itinerary.getTitle());
                        binding.srlItineraryDaySelection.setRefreshing(false);
                    }));
        });
    }

    public void setupRecyclerView() {
        binding.rvSimpleItineraryDays.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvSimpleItineraryDays.setAdapter(adapter);
    }

    public void setupToolbar() {
        binding.tbItineraryDaySelection.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }
}
