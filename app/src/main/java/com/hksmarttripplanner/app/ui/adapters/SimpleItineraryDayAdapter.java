package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.databinding.ItemSimpleItineraryDayBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class SimpleItineraryDayAdapter extends RecyclerView.Adapter<SimpleItineraryDayAdapter.ItineraryDayViewHolder> {
    private final PublishSubject<ItineraryDay> onClickItem = PublishSubject.create();
    private List<ItineraryDay> itemList;

    public SimpleItineraryDayAdapter(List<ItineraryDay> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ItineraryDayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemSimpleItineraryDayBinding binding = ItemSimpleItineraryDayBinding.inflate(layoutInflater, viewGroup, false);
        return new ItineraryDayViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItineraryDayViewHolder itineraryViewHolder, int i) {
        ItineraryDay itinerary = itemList.get(i);
        itineraryViewHolder.bind(itinerary);

        itineraryViewHolder.binding.cvSimpleItineraryDay.setOnClickListener(v -> {
            onClickItem.onNext(itinerary);
        });
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public PublishSubject<ItineraryDay> getOnClickPos() {
        return onClickItem;
    }

    public void setItemList(List<ItineraryDay> newItemList) {
        if (newItemList == null) {
            int oldSize = itemList.size();
            itemList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new ItineraryDayDiffCallBack(itemList, newItemList));
            itemList.clear();
            itemList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    public void clear() {
        itemList.clear();
        notifyDataSetChanged();
    }

    public static class ItineraryDayViewHolder extends RecyclerView.ViewHolder {
        private final ItemSimpleItineraryDayBinding binding;

        public ItineraryDayViewHolder(ItemSimpleItineraryDayBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ItineraryDay itinerary) {
            binding.setItineraryDay(itinerary);
            binding.executePendingBindings();
        }
    }

    private class ItineraryDayDiffCallBack extends DiffUtil.Callback {
        private List<ItineraryDay> oldList;
        private List<ItineraryDay> newList;

        public ItineraryDayDiffCallBack(List<ItineraryDay> oldList, List<ItineraryDay> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            ItineraryDay oldItineraryDay = oldList.get(oldItemPos);
            ItineraryDay newItineraryDay = newList.get(newItemPos);
            return Objects.equals(oldItineraryDay, newItineraryDay);
        }
    }
}
