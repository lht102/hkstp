package com.hksmarttripplanner.app.ui.received_itinerary;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.ReceivedItinerary;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.HttpException;

public class ReceivedItineraryViewModel extends BaseViewModel {
    private final MutableLiveData<ReceivedItinerary> selectedReceivedItinerary = new MutableLiveData();

    public ReceivedItineraryViewModel(@NonNull Application application) {
        super(application);
    }

    public void makeReceivedItinerarySelection(ReceivedItinerary receivedItinerary) {
        selectedReceivedItinerary.setValue(receivedItinerary);
    }

    public Single<List<ReceivedItinerary>> listReceivedItinerary() {
        return getAppRepository().listReceivedItinerary(getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {
                }).doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Completable receiveItinerary(int sharedItineraryId, int itineraryId) {
        return getAppRepository().receiveItinerary(sharedItineraryId, itineraryId, getAppRepository().getMyUserId())
                .doOnSubscribe(disposable -> {
                }).doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public Completable declineItinerary(int sharedItineraryId) {
        return getAppRepository().declineItinerary(sharedItineraryId)
                .doOnSubscribe(disposable -> {
                }).doOnError(throwable -> {
                    if (throwable instanceof SocketTimeoutException) {
                        getSnackbarText().onNext(R.string.network_timeout);
                    } else if (throwable instanceof HttpException) {
                    }
                });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }


        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ReceivedItineraryViewModel(application);
        }
    }
}
