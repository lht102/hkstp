package com.hksmarttripplanner.app.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.hksmarttripplanner.app.R;

public class EditBudgetDialogFragment extends DialogFragment {

    public interface EditBudgetDialogListener {
        void onDialogPositiveClick(DialogFragment dialogFragment, double budget);
        void onDialogNegativeClick(DialogFragment dialogFragment);
    }

    private EditBudgetDialogListener editBudgetDialogListener;

    public void setEditBudgetDialogListener(EditBudgetDialogListener editBudgetDialogListener) {
        this.editBudgetDialogListener = editBudgetDialogListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_edit_budget, null);
        EditText editText = (EditText) view.findViewById(R.id.etEditBudget);
        builder.setView(view)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        double budget = Double.parseDouble(editText.getText().toString());
                        editBudgetDialogListener.onDialogPositiveClick(EditBudgetDialogFragment.this, budget);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editBudgetDialogListener.onDialogNegativeClick(EditBudgetDialogFragment.this);
                    }
                });
        return builder.create();
    }
}
