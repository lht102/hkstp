package com.hksmarttripplanner.app.ui.adapters;

import android.arch.persistence.room.util.StringUtil;
import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;
import com.hksmarttripplanner.app.databinding.ItemTrafficStepListBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class StepListAdapter extends RecyclerView.Adapter<StepListAdapter.StepListViewHolder> {
    private final String TAG = StepListAdapter.class.getSimpleName();
    private final PublishSubject<Step> onClickItem = PublishSubject.create();
    private int mExpandedPosition = -1;
    private Context context;
    private List<Step> itemList;
    private RecyclerView recyclerView;

    public StepListAdapter(Context context, List<Step> itemList, RecyclerView recyclerView) {
        this.context = context;
        this.itemList = itemList;
        this.recyclerView = recyclerView;
    }

    @NonNull
    @Override
    public StepListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemTrafficStepListBinding binding = ItemTrafficStepListBinding.inflate(layoutInflater, viewGroup, false);
        return new StepListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull StepListViewHolder viewHolder, int i) {
        Step item = itemList.get(i);

        viewHolder.binding.vBlackTag.setVisibility(View.GONE);
        List<Step> steps = item.getSteps();
        StepAdapter stepAdapter = null;
        if (steps != null && !steps.isEmpty() && steps.size() != 1 && steps.get(0).getHtmlInstructions() != null) {
            stepAdapter = new StepAdapter(steps);
            viewHolder.binding.rvTrafficStepList.setLayoutManager(new LinearLayoutManager(context));
            viewHolder.binding.rvTrafficStepList.setAdapter(stepAdapter);
            viewHolder.binding.vBlackTag.setVisibility(View.VISIBLE);

            boolean isExpanded = i == mExpandedPosition;
            viewHolder.binding.rvTrafficStepList.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            viewHolder.binding.cvTrafficStepList.setActivated(isExpanded);
            viewHolder.binding.cvTrafficStepList.setOnClickListener(v -> {
                mExpandedPosition = isExpanded ? -1 : i;
                Log.d(TAG, "clicked");

                TransitionManager.beginDelayedTransition(recyclerView);
                notifyDataSetChanged();
            });
        }

        viewHolder.bind(item);
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public PublishSubject<Step> getOnClickPos() {
        return onClickItem;
    }

    public void setSteps(List<Step> steps) {
        itemList.clear();
        itemList.addAll(steps);
        notifyDataSetChanged();
    }

    public void clear() {
        itemList.clear();
        notifyDataSetChanged();
    }

    public static class StepListViewHolder extends RecyclerView.ViewHolder {
        private final ItemTrafficStepListBinding binding;

        public StepListViewHolder(ItemTrafficStepListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Step item) {
            binding.setStep(item);
            binding.executePendingBindings();
        }
    }

    private class StepDiffCallBack extends DiffUtil.Callback {
        private List<Step> oldList;
        private List<Step> newList;

        public StepDiffCallBack(List<Step> oldList, List<Step> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            Step oldStep = oldList.get(oldItemPos);
            Step newStep = newList.get(newItemPos);
            return Objects.equals(oldStep, newStep);
        }
    }
}
