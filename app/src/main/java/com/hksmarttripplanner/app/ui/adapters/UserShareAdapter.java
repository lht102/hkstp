package com.hksmarttripplanner.app.ui.adapters;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.databinding.ItemUserShareBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.PublishSubject;

public class UserShareAdapter extends RecyclerView.Adapter<UserShareAdapter.UserShareViewHolder> {
    private final PublishSubject<User> onClickItem = PublishSubject.create();
    private final PublishSubject<User> onClickShareItem = PublishSubject.create();
    private List<User> userShareList;

    public UserShareAdapter(List<User> userShareList) {
        this.userShareList = userShareList;
    }

    @NonNull
    @Override
    public UserShareAdapter.UserShareViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemUserShareBinding itemUserShareBinding = ItemUserShareBinding.inflate(layoutInflater, viewGroup, false);
        return new UserShareViewHolder(itemUserShareBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserShareViewHolder userShareViewHolder, int i) {
        User user = userShareList.get(i);
        userShareViewHolder.bind(user);

        userShareViewHolder.itemUserShareBinding.crUserInformation.setOnClickListener(v -> onClickItem.onNext(user));

        userShareViewHolder.itemUserShareBinding.btnUserShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickShareItem.onNext(user);
            }
        });
    }

    @Override
    public int getItemCount()  {
        return userShareList == null ? 0 : userShareList.size();
    }

    public PublishSubject<User> getOnClickPos() {
        return onClickItem;
    }

    public PublishSubject<User> getOnClickShare() {
        return onClickShareItem;
    }

    public static class UserShareViewHolder extends RecyclerView.ViewHolder {
        private final ItemUserShareBinding itemUserShareBinding;

        public UserShareViewHolder(ItemUserShareBinding itemUserShareBinding) {
            super(itemUserShareBinding.getRoot());
            this.itemUserShareBinding = itemUserShareBinding;
        }

        public void bind(User user) {
            itemUserShareBinding.setUser(user);
            itemUserShareBinding.executePendingBindings();
        }
    }

    public void setItemlist(List<User> newItemList) {
        if (newItemList == null) {
            int oldSize = userShareList.size();
            userShareList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new UserDiffCallBack(userShareList, newItemList));
            userShareList.clear();
            userShareList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    private class UserDiffCallBack extends DiffUtil.Callback {
        private List<User> oldList;
        private List<User> newList;

        public UserDiffCallBack(List<User> oldList, List<User> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            User oldItinerary = oldList.get(oldItemPos);
            User newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
