package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.databinding.ItemItinerarySelectionBinding;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

import io.reactivex.subjects.PublishSubject;

public class ItinerarySelectionAdapter extends RecyclerView.Adapter<ItinerarySelectionAdapter.ItinerarySelectionViewHolder> {
    private final PublishSubject<ItineraryDay> onClickItineraryDay = PublishSubject.create();
    private int selectedItineraryPos = -1;
    private List<Itinerary> itemList;

    public ItinerarySelectionAdapter() {
    }

    public void setItemList(List<Itinerary> itemList) {
        this.itemList = itemList;
        selectedItineraryPos = -1;
        notifyDataSetChanged();
    }

    public PublishSubject<ItineraryDay> getOnClickItineraryDay() {
        return onClickItineraryDay;
    }

    @NonNull
    @Override
    public ItinerarySelectionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemItinerarySelectionBinding binding = ItemItinerarySelectionBinding.inflate(layoutInflater, viewGroup, false);
        return new ItinerarySelectionAdapter.ItinerarySelectionViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItinerarySelectionViewHolder viewHolder, int i) {
        if (selectedItineraryPos != -1) {
            Itinerary itinerary = itemList.get(selectedItineraryPos);
            ItineraryDay itineraryDay = itinerary.getDays().get(i);
            DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
            viewHolder.binding.tvItineraryTitle.setText(dtf.print(itineraryDay.getDate()));
        } else {
            Itinerary itinerary = itemList.get(i);
            viewHolder.binding.tvItineraryTitle.setText(itinerary.getTitle());
        }

        viewHolder.binding.tvItineraryTitle.setOnClickListener(v -> {
            if (selectedItineraryPos != -1) {
                onClickItineraryDay.onNext(itemList.get(selectedItineraryPos).getDays().get(i));
            } else {
                selectedItineraryPos = i;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (selectedItineraryPos != -1) {
            return itemList.get(selectedItineraryPos).getDays().size();
        }
        return itemList != null ? itemList.size() : 0;
    }

    public static class ItinerarySelectionViewHolder extends RecyclerView.ViewHolder {
        private final ItemItinerarySelectionBinding binding;

        public ItinerarySelectionViewHolder(ItemItinerarySelectionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Itinerary itinerary) {
            binding.setItinerary(itinerary);
            binding.executePendingBindings();
        }
    }
}
