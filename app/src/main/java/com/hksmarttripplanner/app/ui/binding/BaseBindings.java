package com.hksmarttripplanner.app.ui.binding;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.os.Build;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.Budget;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;

public class BaseBindings {
    @BindingAdapter("imageUrl")
    public static void setImage(ImageView imageView, String url) {
        Context context = imageView.getContext();
        // google photo
        if (url != null && !url.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")) {
            url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1280&photoreference=" + url + "&key=" + "AIzaSyCnsjBC-bI0dZLzFNyFJUTv7v5XjUvSo8Y";
        }
        Glide.with(context)
                .load(url)
                .into(imageView);
    }

    @BindingAdapter("textFromHtml")
    public static void textFromHtml(TextView textView, String html) {
        if (html == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
        } else {
            textView.setText(Html.fromHtml(html));
        }
    }

    @BindingAdapter("budgetDes")
    public static void setBudgetDes(TextView textView, Budget budget) {
        DecimalFormat df = new DecimalFormat("#.##");
        String str = "";
        if (budget != null) {
            str = "Budget $" + df.format(budget.getAmount()) + " " + budget.getCurrency();
        } else {
            str = "Budget $0";
        }
        textView.setText(str);
    }

    @BindingAdapter("userIcon")
    public static void setUserIcon(ImageView imageView, String gender) {
        if ("M".equalsIgnoreCase(gender)) {
            imageView.setImageResource(R.drawable.male);
        } else if ("F".equalsIgnoreCase(gender)) {
            imageView.setImageResource(R.drawable.female);
        }
    }

    @BindingAdapter("userGender")
    public static void setUserGender(TextView textView, String gender) {
        if ("M".equalsIgnoreCase(gender)) {
            textView.setText("Male");
        } else if ("F".equalsIgnoreCase(gender)) {
            textView.setText("Female");
        }
    }

    @BindingAdapter("weatherIcon")
    public static void setWeatherIcon(ImageView imageView, String description) {
        if (description != null) {
            description = description.toLowerCase();
            if (description.contains("clear sky")) {
                imageView.setImageResource(R.mipmap.ic_clear_sky);
            } else if (description.contains("few clouds")) {
                imageView.setImageResource(R.mipmap.ic_few_clouds);
            } else if (description.contains("scattered clouds")) {
                imageView.setImageResource(R.mipmap.ic_scattered_clouds);
            } else if (description.contains("broken clouds")) {
                imageView.setImageResource(R.mipmap.ic_broken_clouds);
            } else if (description.contains("shower rain")) {
                imageView.setImageResource(R.mipmap.ic_shower_rain);
            } else if (description.contains("rain")) {
                imageView.setImageResource(R.mipmap.ic_rain);
            } else if (description.contains("thunderstorm")) {
                imageView.setImageResource(R.mipmap.ic_thunderstorm);
            } else if (description.contains("snow")) {
                imageView.setImageResource(R.mipmap.ic_snow);
            } else if (description.contains("mist")) {
                imageView.setImageResource(R.mipmap.ic_mist);
            }
        }
    }

    @BindingAdapter("shortDay")
    public static void setShortDay(TextView textView, String day) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dt = formatter.parseDateTime(day);
        textView.setText(dt.toString("dd/MM"));
    }

    @BindingAdapter("shortTime")
    public static void setShortTime(TextView textView, String day) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dt = formatter.parseDateTime(day);
        textView.setText(dt.toString("HH:mm"));
    }
}
