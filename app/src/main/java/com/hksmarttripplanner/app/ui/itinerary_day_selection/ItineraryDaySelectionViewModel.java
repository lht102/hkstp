package com.hksmarttripplanner.app.ui.itinerary_day_selection;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.util.List;

import io.reactivex.Single;

public class ItineraryDaySelectionViewModel extends BaseViewModel {
    private final int mItineraryId;

    public ItineraryDaySelectionViewModel(@NonNull Application application, int itineraryId) {
        super(application);
        mItineraryId = itineraryId;
    }

    public Single<Itinerary> getItinerary() {
        return getAppRepository().getItineraryById(mItineraryId);
    }

    public Single<Place> createActivity(int itineraryDayId, int placeId) {
        return getAppRepository().createActivity(itineraryDayId, placeId);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;
        private final int mItineraryId;

        public Factory(Application application, int itineraryId) {
            this.application = application;
            mItineraryId = itineraryId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ItineraryDaySelectionViewModel(application, mItineraryId);
        }
    }
}
