package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.remote.api.response.Step;
import com.hksmarttripplanner.app.databinding.ItemTrafficStepBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.StepViewHolder> {
    private final PublishSubject<Step> onClickItem = PublishSubject.create();
    private List<Step> itemList;

    public StepAdapter(List<Step> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public StepViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemTrafficStepBinding binding = ItemTrafficStepBinding.inflate(layoutInflater, viewGroup, false);
        return new StepViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull StepViewHolder viewHolder, int i) {
        Step itinerary = itemList.get(i);
        viewHolder.bind(itinerary);
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public PublishSubject<Step> getOnClickPos() {
        return onClickItem;
    }

    public void setItemList(List<Step> newItemList) {
        if (newItemList == null) {
            int oldSize = itemList.size();
            itemList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new StepDiffCallBack(itemList, newItemList));
            itemList.clear();
            itemList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    public void clear() {
        itemList.clear();
        notifyDataSetChanged();
    }

    public static class StepViewHolder extends RecyclerView.ViewHolder {
        private final ItemTrafficStepBinding binding;

        public StepViewHolder(ItemTrafficStepBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Step item) {
            binding.setStep(item);
            binding.executePendingBindings();
        }
    }

    private class StepDiffCallBack extends DiffUtil.Callback {
        private List<Step> oldList;
        private List<Step> newList;

        public StepDiffCallBack(List<Step> oldList, List<Step> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            Step oldStep = oldList.get(oldItemPos);
            Step newStep = newList.get(newItemPos);
            return Objects.equals(oldStep, newStep);
        }
    }

}
