package com.hksmarttripplanner.app.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.hksmarttripplanner.app.R;

public class EditDurationDialogFragment extends DialogFragment {

    public interface EditDurationDialogListener {
        void onDialogPositiveClick(DialogFragment dialogFragment, int duration);
        void onDialogNegativeClick(DialogFragment dialogFragment);
    }

    private EditDurationDialogListener editDurationDialogListener;

    public void setEditDurationDialogListener(EditDurationDialogListener editDurationDialogListener) {
        this.editDurationDialogListener = editDurationDialogListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_edit_duration, null);
        EditText editText = (EditText) view.findViewById(R.id.etEditDuration);
        builder.setView(view)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int duration = Integer.parseInt(editText.getText().toString());
                        editDurationDialogListener.onDialogPositiveClick(EditDurationDialogFragment.this, duration);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editDurationDialogListener.onDialogNegativeClick(EditDurationDialogFragment.this);
                    }
                });
        return builder.create();
    }
}
