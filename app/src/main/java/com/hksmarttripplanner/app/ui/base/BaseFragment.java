package com.hksmarttripplanner.app.ui.base;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import io.reactivex.disposables.CompositeDisposable;

public class BaseFragment extends Fragment {
    private CompositeDisposable mCompositeDisposable;

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCompositeDisposable = new CompositeDisposable();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for (int i = 0; i < fm.getFragments().size(); i++) {
            Log.d("onResume FragmentStack", "FragmentManager:" + fm.getFragments().get(i).getTag());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    protected void showSnackBar(@StringRes int text) {
        Snackbar.make(getView(), text, Snackbar.LENGTH_SHORT).show();
    }
}
