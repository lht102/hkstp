package com.hksmarttripplanner.app.ui.base;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.AppRepository;

import io.reactivex.subjects.PublishSubject;

public class BaseViewModel extends AndroidViewModel {
    private PublishSubject<Integer> snackbarText;
    private ObservableBoolean mIsLoading;
    private AppRepository mAppRepository;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        mAppRepository = AppRepository.getInstance(application.getApplicationContext());
        mIsLoading = new ObservableBoolean(false);
        snackbarText = PublishSubject.create();
    }

    public PublishSubject<Integer> getSnackbarText() {
        return snackbarText;
    }

    public AppRepository getAppRepository() {
        return mAppRepository;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.set(isLoading);
    }

    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }
}
