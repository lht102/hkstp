package com.hksmarttripplanner.app.ui.weather;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.hksmarttripplanner.app.data.source.remote.api.response.WeatherDayResponse;
import com.hksmarttripplanner.app.ui.base.BaseViewModel;

import java.util.List;

import io.reactivex.Single;

public class WeatherViewModel extends BaseViewModel {
    public WeatherViewModel(@NonNull Application application) {
        super(application);
    }

   public Single<List<WeatherDayResponse>> getWeatherReport() {
        return getAppRepository().getWeatherReport();
   }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;

        public Factory(Application application) {
            this.application = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new WeatherViewModel(application);
        }
    }
}
