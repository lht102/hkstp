package com.hksmarttripplanner.app.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentSearchPlaceBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;
import com.hksmarttripplanner.app.ui.itinerary_day_place_list.ItineraryDayPlaceListFragment;
import com.hksmarttripplanner.app.ui.search_result.SearchResultFragment;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SearchPlaceFragment extends BaseFragment {
    public static final String TAG = SearchPlaceFragment.class.getSimpleName();
    private SearchPlaceViewModel searchPlaceViewModel;
    private FragmentSearchPlaceBinding fragmentSearchPlaceBinding;
    private String keyword = "", district = "", type = "", category = "", budget = "9999";
    private List<String> categorySpinner = new LinkedList();
    private static final String [] districts = {"Central and Western", "Eastern", "Islands", "Kowloon City", "Kwai Tsing",
    "Kwun Tong", "North", "Sai Kung", "Sha Tin", "Sham Shui Po", "Southern", "Tai Po", "Tsuen Wan", "Tuen Mun", "Wan Chai",
    "Wong Tai Sin", "Yau Tsim Mong", "Yuen Long"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSearchPlaceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_place, container, false);
        return fragmentSearchPlaceBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SearchPlaceViewModel.Factory factory = new SearchPlaceViewModel.Factory(getActivity().getApplication());
        searchPlaceViewModel = ViewModelProviders.of(getActivity(), factory).get(SearchPlaceViewModel.class);
        fragmentSearchPlaceBinding.setSearchPlaceViewModel(searchPlaceViewModel);

        setSearchPlaceButton();
        setAutoCompleteButton();
        setRadioButton();
        setStyle();
        setSpinner(categorySpinner);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(searchPlaceViewModel.getSnackbarText()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::showSnackBar,
                throwable -> Log.e(getTag(), "Unable to display search place screen.")));
        setCurrency();
    }

    private void setAutoCompleteButton() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, districts);
        fragmentSearchPlaceBinding.actvDistrict.setAdapter(arrayAdapter);
        fragmentSearchPlaceBinding.actvDistrict.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                district = fragmentSearchPlaceBinding.actvDistrict.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                // nothing
            }
        });
    }

    private void setRadioButton() {
        fragmentSearchPlaceBinding.rgPlaceType.setOnCheckedChangeListener((radioGroup, i) -> {
            categorySpinner.clear();
            if (fragmentSearchPlaceBinding.rbAttraction.isChecked()) {
                type = "a";
                setStyle();
            } else if (fragmentSearchPlaceBinding.rbRestaurant.isChecked()) {
                type = "r";
                setCuisine();
            }
            setSpinner(categorySpinner);
        });
    }

    public void setSpinner(List<String> categoryList) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, categoryList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fragmentSearchPlaceBinding.spCategory.setAdapter(arrayAdapter);
        fragmentSearchPlaceBinding.spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    category = "";
                } else {
                    category = fragmentSearchPlaceBinding.spCategory.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //nothing
            }
        });
    }

    private void setStyle() {
        categorySpinner.add(0, "Select Category");
        categorySpinner.add("Art");
        categorySpinner.add("Culture");
        categorySpinner.add("Explore");
        categorySpinner.add("Food");
        categorySpinner.add("History");
        categorySpinner.add("Landscape");
        categorySpinner.add("Leisure");
        categorySpinner.add("Romantic");
        categorySpinner.add("Prosperity");
        categorySpinner.add("Shopping");
        categorySpinner.add("Sports");
        categorySpinner.add("Technology");
    }

    private void setCuisine() {
        categorySpinner.add(0, "Select Cuisine");
        categorySpinner.add("American");
        categorySpinner.add("Chinese");
        categorySpinner.add("French");
        categorySpinner.add("Italian");
        categorySpinner.add("Thai");
        categorySpinner.add("Japanese");
        categorySpinner.add("Korean");
        categorySpinner.add("Taiwan");
        categorySpinner.add("Thai");
        categorySpinner.add("Vegetarian");
        categorySpinner.add("Vietnamese");
        categorySpinner.add("Western");
    }

    private void setSearchPlaceButton() {
        fragmentSearchPlaceBinding.btnSearchPlace.setOnClickListener(v -> {
            keyword = fragmentSearchPlaceBinding.etSearchKeyword.getText().toString();
            district = fragmentSearchPlaceBinding.actvDistrict.getText().toString();
            if (!fragmentSearchPlaceBinding.etMaxBudget.getText().toString().equals("")) {
                budget = fragmentSearchPlaceBinding.etMaxBudget.getText().toString();
            }
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, SearchResultFragment
                            .newInstance(keyword, district, type, category, budget), SearchResultFragment.TAG)
                    .addToBackStack(TAG).commit();
        });
    }

    public void setCurrency() {
        getCompositeDisposable().add(searchPlaceViewModel.getCurrency()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    fragmentSearchPlaceBinding.tvCurrencyType.setText(user.getCurrency());
                }));
    }
}
