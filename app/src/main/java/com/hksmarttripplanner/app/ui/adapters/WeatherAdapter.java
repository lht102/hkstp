package com.hksmarttripplanner.app.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.remote.api.response.WeatherDayResponse;
import com.hksmarttripplanner.app.databinding.ItemWeatherDayBinding;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {
    private static final String TAG = WeatherAdapter.class.getSimpleName();

    private List<WeatherDayResponse> weatherList;

    public WeatherAdapter(List<WeatherDayResponse> weatherList) {
        this.weatherList = weatherList;
    }

    @NonNull
    @Override
    public WeatherAdapter.WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemWeatherDayBinding itemWeatherBinding = ItemWeatherDayBinding.inflate(layoutInflater, viewGroup, false);
        return new WeatherViewHolder(itemWeatherBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder WeatherViewHolder, int i) {
        WeatherDayResponse weatherDay = weatherList.get(i);
        WeatherViewHolder.bind(weatherDay);
    }

    @Override
    public int getItemCount() {
        return weatherList == null ? 0 : weatherList.size();
    }

    public static class WeatherViewHolder extends RecyclerView.ViewHolder {
        private final ItemWeatherDayBinding binding;

        public WeatherViewHolder(ItemWeatherDayBinding itemWeatherDayBinding) {
            super(itemWeatherDayBinding.getRoot());
            this.binding = itemWeatherDayBinding;
        }

        public void bind(WeatherDayResponse weatherDayResponse) {
            binding.setWeather(weatherDayResponse);
            binding.executePendingBindings();
        }
    }

    public void setItemList(List<WeatherDayResponse> newItemList) {
        weatherList.clear();
        weatherList.addAll(newItemList);
        notifyDataSetChanged();
        Log.d(TAG, "setItemList: " + weatherList.size());
    }
}
