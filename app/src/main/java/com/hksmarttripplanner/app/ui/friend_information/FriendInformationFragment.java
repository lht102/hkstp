package com.hksmarttripplanner.app.ui.friend_information;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.data.source.local.model.UserInvite;
import com.hksmarttripplanner.app.data.source.local.model.UserReceive;
import com.hksmarttripplanner.app.databinding.FragmentFriendInformationBinding;
import com.hksmarttripplanner.app.ui.base.BaseFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FriendInformationFragment extends BaseFragment {
    private Boolean booleans;
    private FriendInformationViewModel friendInformationViewModel;
    private FragmentFriendInformationBinding fragmentFriendInformationBinding;

    public static FriendInformationFragment newInstance(UserInvite userInvite) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", userInvite.getId());
        bundle.putString("name", userInvite.getName());
        bundle.putString("gender", userInvite.getGender());
        bundle.putString("email", userInvite.getEmail());
        bundle.putString("description", userInvite.getDescription());
        FriendInformationFragment friendInformationFragment = new FriendInformationFragment();
        friendInformationFragment.setArguments(bundle);
        return friendInformationFragment;
    }

    public static FriendInformationFragment newInstance(UserReceive userReceive) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", userReceive.getId());
        bundle.putString("name", userReceive.getName());
        bundle.putString("gender", userReceive.getGender());
        bundle.putString("email", userReceive.getEmail());
        bundle.putString("description", userReceive.getDescription());
        FriendInformationFragment friendInformationFragment = new FriendInformationFragment();
        friendInformationFragment.setArguments(bundle);
        return friendInformationFragment;
    }
    public static FriendInformationFragment newInstance(User user) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", user.getId());
        bundle.putString("name", user.getName());
        bundle.putString("gender", user.getGender());
        bundle.putString("email", user.getEmail());
        bundle.putString("description", user.getDescription());
        FriendInformationFragment friendInformationFragment = new FriendInformationFragment();
        friendInformationFragment.setArguments(bundle);
        return friendInformationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFriendInformationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_information, container, false);
        return fragmentFriendInformationBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FriendInformationViewModel.Factory factory = new FriendInformationViewModel.Factory(getActivity().getApplication());
        friendInformationViewModel = ViewModelProviders.of(getActivity(), factory).get(FriendInformationViewModel.class);
        fragmentFriendInformationBinding.setFriendInformationViewModel(friendInformationViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompositeDisposable().add(friendInformationViewModel.getSnackbarText()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showSnackBar,
                        throwable -> Log.e(getTag(), "Unable to display user information.")));
        setFragmentView();
        setToolbar();
        setbtnInviteOrRemoveFriend();
        getBoolean();
    }

    public void setFragmentView() {
        int id = getArguments().getInt("id");
        String name = getArguments().getString("name");
        String gender = getArguments().getString("gender");
        String email = getArguments().getString("email");
        String description = getArguments().getString("description");
        if (gender.equals("M")) {
            gender = "Male";
            fragmentFriendInformationBinding.ivUserIcon.setImageResource(R.drawable.male);
        } else {
            gender = "Female";
            fragmentFriendInformationBinding.ivUserIcon.setImageResource(R.drawable.female);
        }
        fragmentFriendInformationBinding.tvUserID.setText(String.valueOf(id));
        fragmentFriendInformationBinding.tvUserName.setText(name);
        fragmentFriendInformationBinding.tvUserGender.setText(gender);
        fragmentFriendInformationBinding.tvUserEmail.setText(email);
        fragmentFriendInformationBinding.tvUserDescription.setText(description);
    }

    public void setToolbar() {
        fragmentFriendInformationBinding.tbFriendInformation.setTitle(getArguments().getString("name"));
        fragmentFriendInformationBinding.tbFriendInformation.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    public void getBoolean() {
        getCompositeDisposable().add(friendInformationViewModel.
                isFriend(getArguments().getInt("id"),
                        friendInformationViewModel.getAppRepository().getMyUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((booleans) -> {
                    this.booleans = booleans;
                    if (booleans) {
                        fragmentFriendInformationBinding.btnInviteOrRemove.setHint("Remove Friend");
                    }
                }, throwable ->
                        throwable.printStackTrace()));
    }

    public void setbtnInviteOrRemoveFriend() {
        fragmentFriendInformationBinding.btnInviteOrRemove.setOnClickListener(v -> {
            if (!booleans) {
                getCompositeDisposable().add(friendInformationViewModel.addOrReceiveFriend(
                        friendInformationViewModel.getAppRepository().getMyUserId(),
                        getArguments().getInt("id"))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            fragmentFriendInformationBinding.btnInviteOrRemove.setHint("Remove Friend");
                            booleans = true;
                        }, throwable -> {
                            throwable.printStackTrace();
                        }));
            } else if (booleans) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Make sure delete this friend?").setCancelable(false)
                        .setNegativeButton("No", (dialog, which) -> dialog.cancel())
                        .setPositiveButton("Yes", (dialog, which) -> {
                            getCompositeDisposable().add(friendInformationViewModel.removeFriend(
                                    friendInformationViewModel.getAppRepository().getMyUserId(),
                                    getArguments().getInt("id")).subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> {
                                    }, throwable -> {
                                        throwable.printStackTrace();
                                    }));
                            getCompositeDisposable().add(friendInformationViewModel.removeFriend(
                                    getArguments().getInt("id"),
                                    friendInformationViewModel.getAppRepository().getMyUserId())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> {
                                    }, throwable -> {
                                        throwable.printStackTrace();
                                    }));
                            fragmentFriendInformationBinding.btnInviteOrRemove.setHint("Add Friend");
                            booleans = false;
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }
}
