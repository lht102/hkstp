package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.Budget;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.databinding.ItemItineraryDayBinding;
import com.hksmarttripplanner.app.utils.DetailRouteUtils;

import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class ItineraryDayAdapter extends RecyclerView.Adapter<ItineraryDayAdapter.ItineraryDayViewHolder> {
    private final PublishSubject<ItineraryDay> onClickDeleteItem = PublishSubject.create();
    private final PublishSubject<ItineraryDay> onClickItem = PublishSubject.create();
    private List<ItineraryDay> itemlist;
    private Hashtable<Integer, List<DetailRoute>> detailRouteTable = new Hashtable<>();
    private Hashtable<Integer, Duration> durationTable = new Hashtable<>();
    private Hashtable<Integer, Double> budgetTable = new Hashtable<>();
    private PublishSubject<Duration> durationPublishSubject = PublishSubject.create();
    private PublishSubject<Double> budgetPublishSubject = PublishSubject.create();

    public ItineraryDayAdapter(List<ItineraryDay> itemlist) {
        this.itemlist = itemlist;
    }

    @NonNull
    @Override
    public ItineraryDayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemItineraryDayBinding binding = ItemItineraryDayBinding.inflate(layoutInflater, viewGroup, false);
        return new ItineraryDayViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItineraryDayAdapter.ItineraryDayViewHolder itineraryDayViewHolader, int i) {
        ItineraryDay itineraryDay = itemlist.get(i);
        itineraryDayViewHolader.bind(itineraryDay);

        itineraryDayViewHolader.binding.btnItineraryDel.setOnClickListener(v -> {
            onClickDeleteItem.onNext(itineraryDay);
            int index = itemlist.indexOf(itineraryDay);
            itemlist.remove(index);
            notifyItemRemoved(index);
        });

        itineraryDayViewHolader.binding.cardView.setOnClickListener(v -> {
            onClickItem.onNext(itineraryDay);
        });

        List<DetailRoute> detailRoutes = detailRouteTable.get(itineraryDay.getId());
        if (detailRoutes != null) {
            double val = 0;
            String curr = "";
            Duration duration = new Duration(0);
            List<Place> itemList = itineraryDay.getPlaces();

            for (int j = 0; j < itemList.size(); j++) {
                Place place = itemList.get(j);
                duration = duration.plus(Duration.standardMinutes(place.getDuration()));
                Budget budget = place.getActualBudget();

                if (j != itemList.size() - 1 && detailRoutes.size() > 0) {
                    long sec = DetailRouteUtils.getDetailRouteDuration(detailRoutes.get(j));
                    if (sec % 60 >= 30) {
                        sec = sec - sec % 60 + 60;
                    } else {
                        sec = sec - sec % 60;
                    }
                    duration = duration.plus(Duration.standardSeconds(sec));
                    if (DetailRouteUtils.isRouteExist(detailRoutes.get(j))) {
                        val += detailRoutes.get(j).getDisplayfee().getAmount();
                    }
                }
                if (place.getActualBudget() != null && place.getActualBudget().getCurrency() != null) {
                    curr = place.getActualBudget().getCurrency();
                }
                if (budget != null) {
                    val += budget.getAmount();
                }
            }
            PeriodFormatter formatter = new PeriodFormatterBuilder()
                    .appendHours()
                    .appendSuffix("hrs")
                    .appendMinutes()
                    .appendSuffix("mins")
                    .toFormatter();
            DecimalFormat df = new DecimalFormat("#.##");

            String Durationstr = "Duration(with traffic time): " + formatter.print(duration.toPeriod());
            String Budgetstr = "Budget(with Fee): $ " + df.format(val) + " " + curr;

            itineraryDayViewHolader.binding.pbTrafficDetail.setVisibility(View.INVISIBLE);
            itineraryDayViewHolader.binding.tvDurationDes.setText(Durationstr);
            itineraryDayViewHolader.binding.tvBudgetDes.setText(Budgetstr);

            durationTable.put(itineraryDay.getId(), duration);
            budgetTable.put(itineraryDay.getId(), val);

            double totalbudget = 0;
            Duration totalDuration = new Duration(0);
            for (int j = 0; j < itemlist.size(); j++) {
                ItineraryDay itineraryDayKey = itemlist.get(j);
                Duration dur = durationTable.get(itineraryDayKey.getId());
                Double bud = budgetTable.get(itineraryDayKey.getId());
                if (bud != null) {
                    totalbudget += bud;
                }
                if (dur != null) {
                    totalDuration = totalDuration.withDurationAdded(dur, 1);
                }
            }
            budgetPublishSubject.onNext(totalbudget);
            durationPublishSubject.onNext(totalDuration);

        } else {
            itineraryDayViewHolader.binding.pbTrafficDetail.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return itemlist == null ? 0 : itemlist.size();
    }

    public PublishSubject<ItineraryDay> getOnClickDeletePos() {
        return onClickDeleteItem;
    }

    public PublishSubject<ItineraryDay> getOnClickPos() {
        return onClickItem;
    }

    public PublishSubject<Duration> getDurationPublishSubject() {
        return durationPublishSubject;
    }

    public PublishSubject<Double> getBudgetPublishSubject() {
        return budgetPublishSubject;
    }

    public void setItemlist(List<ItineraryDay> newItemList) {
        itemlist.clear();
        itemlist.addAll(newItemList);
        notifyDataSetChanged();
    }

    public void clear() {
        itemlist.clear();
        notifyDataSetChanged();
    }

    public void addItineraryDay(ItineraryDay itineraryDay) {
        itemlist.add(itineraryDay);
        updateDayOrder();
    }

    public void updateDayOrder() {
        List<ItineraryDay> itineraryDays = itemlist;
        Collections.sort(itineraryDays);
        for (int j = 0; j < itineraryDays.size(); j++) {
            ItineraryDay day = itineraryDays.get(j);
            day.setDay(j + 1);
        }
        notifyDataSetChanged();
    }

    public void setDetailRoute(List<DetailRoute> detailRoutes, int id, int pos) {
        detailRouteTable.put(id, detailRoutes);
        notifyItemChanged(pos);
    }

    public static class ItineraryDayViewHolder extends RecyclerView.ViewHolder {
        private final ItemItineraryDayBinding binding;

        public ItineraryDayViewHolder(ItemItineraryDayBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ItineraryDay itineraryDay) {
            binding.setItineraryDay(itineraryDay);
            binding.executePendingBindings();
        }
    }
}
