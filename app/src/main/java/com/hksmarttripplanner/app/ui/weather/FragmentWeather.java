package com.hksmarttripplanner.app.ui.weather;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.R;
import com.hksmarttripplanner.app.databinding.FragmentWeatherBinding;
import com.hksmarttripplanner.app.ui.adapters.WeatherAdapter;
import com.hksmarttripplanner.app.ui.base.BaseFragment;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FragmentWeather extends BaseFragment {
    public static final String TAG = FragmentWeather.class.getSimpleName();

    private FragmentWeatherBinding binding;
    private WeatherViewModel viewModel;
    private WeatherAdapter adapter = new WeatherAdapter(new ArrayList<>());

    public static FragmentWeather newInstance() {
        FragmentWeather fragment = new FragmentWeather();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        WeatherViewModel.Factory factory = new WeatherViewModel.Factory(getActivity().getApplication());
        viewModel = ViewModelProviders.of(this, factory).get(WeatherViewModel.class);
        binding.setViewModel(viewModel);

        binding.tbWeatherReport.setNavigationOnClickListener(v -> {
            getActivity().onBackPressed();
        });

        setupRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();

        getCompositeDisposable().add(viewModel.getWeatherReport()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((weatherDayResponses, throwable) -> {
                    adapter.setItemList(weatherDayResponses);
                }));
    }

    public void setupRecyclerView() {
        binding.rvWeather.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvWeather.setAdapter(adapter);
    }
}
