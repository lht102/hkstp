package com.hksmarttripplanner.app.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.databinding.ItemItineraryBinding;

import java.util.List;
import java.util.Objects;

import io.reactivex.subjects.PublishSubject;

public class ItineraryAdapter extends RecyclerView.Adapter<ItineraryAdapter.ItineraryViewHolder> {
    private final PublishSubject<Itinerary> onClickItem = PublishSubject.create();
    private final PublishSubject<Itinerary> onClickDeleteItem = PublishSubject.create();
    private final PublishSubject<Itinerary> onClickShareItem = PublishSubject.create();
    private List<Itinerary> itemList;

    public ItineraryAdapter(List<Itinerary> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ItineraryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ItemItineraryBinding binding = ItemItineraryBinding.inflate(layoutInflater, viewGroup, false);
        return new ItineraryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItineraryViewHolder itineraryViewHolder, int i) {
        Itinerary itinerary = itemList.get(i);
        itineraryViewHolder.bind(itinerary);

        itineraryViewHolder.binding.btnExpand.setOnClickListener(v -> {
            onClickItem.onNext(itinerary);
        });

        itineraryViewHolder.binding.btnDelete.setOnClickListener(v -> {
            onClickDeleteItem.onNext(itinerary);
            int index = itemList.indexOf(itinerary);
            itemList.remove(index);
            notifyItemRemoved(index);
        });

        itineraryViewHolder.binding.btnShare.setOnClickListener(v -> onClickShareItem.onNext(itinerary));
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public PublishSubject<Itinerary> getOnClickPos() {
        return onClickItem;
    }

    public PublishSubject<Itinerary> getOnClickDeleteItem() {
        return onClickDeleteItem;
    }

    public PublishSubject<Itinerary> getOnClickShareItem() { return onClickShareItem;}

    public void setItemList(List<Itinerary> newItemList) {
        if (newItemList == null) {
            int oldSize = itemList.size();
            itemList.clear();
            notifyItemRangeChanged(0, oldSize);
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new ItineraryDiffCallBack(itemList, newItemList));
            itemList.clear();
            itemList.addAll(newItemList);
            result.dispatchUpdatesTo(this);
        }
    }

    public void clear() {
        itemList.clear();
        notifyDataSetChanged();
    }

    public void addItinerary(Itinerary itinerary) {
        itemList.add(itinerary);
        notifyItemInserted(itemList.size() - 1);
    }

    public static class ItineraryViewHolder extends RecyclerView.ViewHolder {
        private final ItemItineraryBinding binding;

        public ItineraryViewHolder(ItemItineraryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Itinerary itinerary) {
            binding.setItinerary(itinerary);
            binding.executePendingBindings();
        }
    }

    private class ItineraryDiffCallBack extends DiffUtil.Callback {
        private List<Itinerary> oldList;
        private List<Itinerary> newList;

        public ItineraryDiffCallBack(List<Itinerary> oldList, List<Itinerary> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPos, int newItemPos) {
            int oldId = oldList.get(oldItemPos).getId();
            int newId = newList.get(newItemPos).getId();
            return Objects.equals(oldId, newId);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPos, int newItemPos) {
            Itinerary oldItinerary = oldList.get(oldItemPos);
            Itinerary newItinerary = newList.get(newItemPos);
            return Objects.equals(oldItinerary, newItinerary);
        }
    }
}
