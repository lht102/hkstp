package com.hksmarttripplanner.app.data.source.local.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.data.source.remote.api.response.Route;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class Place implements Parcelable {
    @Expose
    @SerializedName(value = "id", alternate = {"place_id"})
    private int id;

    @Expose
    @SerializedName("activity_id")
    private int activityId;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("district")
    private String district;

    @Expose
    @SerializedName("image")
    private String image;

    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("rating")
    private double rating;

    @Expose
    @SerializedName("latitude")
    private double latitude;

    @Expose
    @SerializedName("longitude")
    private double longitude;

    @Expose
    @SerializedName("budget")
    private Budget budget;

    @Expose
    @SerializedName("actBudget")
    private Budget actualBudget;

    @Expose
    @SerializedName("specialBudget")
    private Budget specialBudget;


    @Expose
    @SerializedName("startTime")
    private DateTime startTime;

    @Expose
    @SerializedName("duration")
    private int duration;

    @Expose
    @SerializedName("time")
    private List<Time> times;

    @Expose
    @SerializedName("routes")
    private DetailRoute detailRoute;

    @Expose
    @SerializedName("travelMode")
    private String travelMode;

    @Expose
    @SerializedName("transitMode")
    private List<String> transitMode;

    @Expose
    @SerializedName("routeAvoid")
    private List<String> routeAvoid;

    @Expose
    @SerializedName("routePref")
    private String routePref;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public Budget getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Budget actualBudget) {
        this.actualBudget = actualBudget;
    }

    public Budget getSpecialBudget() {
        return specialBudget;
    }

    public void setSpecialBudget(Budget specialBudget) {
        this.specialBudget = specialBudget;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<Time> getTimes() {
        return times;
    }

    public void setTimes(List<Time> times) {
        this.times = times;
    }

    public DetailRoute getDetailRoute() {
        return detailRoute;
    }

    public void setDetailRoute(DetailRoute detailRoute) {
        this.detailRoute = detailRoute;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public List<String> getTransitMode() {
        return transitMode;
    }

    public void setTransitMode(List<String> transitMode) {
        this.transitMode = transitMode;
    }

    public List<String> getRouteAvoid() {
        return routeAvoid;
    }

    public void setRouteAvoid(List<String> routeAvoid) {
        this.routeAvoid = routeAvoid;
    }

    public String getRoutePref() {
        return routePref;
    }

    public void setRoutePref(String routePref) {
        this.routePref = routePref;
    }

    public Place () {

    }

    public Place(String name, String address, String type, String description,
                 String district, String image, String phone, int id, double rating,
                 double latitude, double longitude, Budget budget, Budget specialBudget,
                 List<Time> time) {
        this.name = name;
        this.address = address;
        this.type = type;
        this.description = description;
        this.district = district;
        this.image = image;
        this.phone = phone;
        this.id = id;
        this.rating = rating;
        this.latitude = latitude;
        this.longitude = longitude;
        this.budget = budget;
        this.specialBudget = specialBudget;
        this.times = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.activityId);
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeString(this.type);
        dest.writeString(this.description);
        dest.writeString(this.district);
        dest.writeString(this.image);
        dest.writeString(this.phone);
        dest.writeDouble(this.rating);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeParcelable(this.budget, flags);
        dest.writeParcelable(this.actualBudget, flags);
        dest.writeParcelable(this.specialBudget, flags);
        dest.writeSerializable(this.startTime);
        dest.writeInt(this.duration);
        dest.writeTypedList(this.times);
        dest.writeParcelable(this.detailRoute, flags);
        dest.writeString(this.travelMode);
        dest.writeStringList(this.transitMode);
        dest.writeStringList(this.routeAvoid);
        dest.writeString(this.routePref);
    }

    protected Place(Parcel in) {
        this.id = in.readInt();
        this.activityId = in.readInt();
        this.name = in.readString();
        this.address = in.readString();
        this.type = in.readString();
        this.description = in.readString();
        this.district = in.readString();
        this.image = in.readString();
        this.phone = in.readString();
        this.rating = in.readDouble();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.budget = in.readParcelable(Budget.class.getClassLoader());
        this.actualBudget = in.readParcelable(Budget.class.getClassLoader());
        this.specialBudget = in.readParcelable(Budget.class.getClassLoader());
        this.startTime = (DateTime) in.readSerializable();
        this.duration = in.readInt();
        this.times = in.createTypedArrayList(Time.CREATOR);
        this.detailRoute = in.readParcelable(DetailRoute.class.getClassLoader());
        this.travelMode = in.readString();
        this.transitMode = in.createStringArrayList();
        this.routeAvoid = in.createStringArrayList();
        this.routePref = in.readString();
    }

    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
