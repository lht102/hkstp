package com.hksmarttripplanner.app.data.source.remote.api;

import javax.annotation.Nullable;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class TokenAuthenticator implements Authenticator {

    @Override
    public Request authenticate(Route route, Response response) {
        // implement refresh token ..
        // ...
        boolean isTokenRefreshed = false;

        if (isTokenRefreshed) {
            String newToken = "new Token";

            return response.request().newBuilder()
                    .header("Authorization", newToken)
                    .build();
        } else {
            return null;
        }
    }
}
