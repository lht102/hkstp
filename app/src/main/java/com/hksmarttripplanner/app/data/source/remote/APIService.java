package com.hksmarttripplanner.app.data.source.remote;

import com.hksmarttripplanner.app.data.source.local.model.Category;
import com.hksmarttripplanner.app.data.source.local.model.Comment;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.local.model.ReceivedItinerary;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.data.source.local.model.UserInvite;
import com.hksmarttripplanner.app.data.source.local.model.UserReceive;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.data.source.remote.api.response.LoginResponse;
import com.hksmarttripplanner.app.data.source.remote.api.response.WeatherDayResponse;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @FormUrlEncoded
    @POST("api/auth/login")
    Single<LoginResponse> loginCall(@Field("email") String email, @Field("password") String password);

    @GET("api/users/{id}/itineraries")
    Single<List<Itinerary>> getUserItinerariesCall(@Path("id") int userId);

    @GET("api/itineraries/{id}")
    Single<Itinerary> getItineraryByIdCall(@Path("id") int itineraryId);

    @DELETE("api/itinerary_days/{id}")
    Completable deleteItineraryDayByIdCall(@Path("id") int itineraryDayId);

    @DELETE("api/itineraries/{id}")
    Completable deleteItineraryByIdCall(@Path("id") int itineraryId);

    @GET("api/auth/me")
    Single<User> getMyUserInfoCall();

    @FormUrlEncoded
    @POST("api/users/{id}/itineraries/create")
    Single<Itinerary> createUserItineraryByIdCall(@Path("id") int userId, @Field("title") String title, @Field("budget") double budget, @Field("startDate")String startDate, @Field("endDate") String endDate);

    @GET("api/place")
    Single<List<Place>> getSearchPlacesCall(@Query("keyword") String keyword, @Query("district") String district, @Query("type") String type, @Query("category") String category, @Query("budget") String budget);

    @GET("api/itinerary_days/{id}")
    Single<ItineraryDay> getItineraryDayByIdCall(@Path("id") int id);

    @GET("api/place")
    Single<List<Category>> getCategory(@Query("type") String type);

    @GET("api/favorite/create")
    Completable addToFavorite(@Query("user_id") int userId, @Query("place_id") int placeId);

    @GET("api/favorite/remove")
    Completable removeFromFavorite(@Query("user_id") int userId, @Query("place_id") int placeId);

    @GET("api/favorite")
    Single<Boolean> isFavorite(@Query("user_id") int userId, @Query("place_id") int placeId);

    @GET("api/users/{userId}/favorites")
    Single<List<Place>> getUserFavoritesCall(@Path("userId") int userId);

    @GET("api/friend")
    Single<List<User>> getSentFriendListById(@Query("user_id") int userId);

    @GET("api/users")
    Single<List<User>> getSearchFriendResult(@Query("id") int userId);

    @GET("api/friend")
    Single<List<User>> getFirmFriendListById(@Query("friend_id") int friendId);

    @GET("api/friend")
    Single<List<UserInvite>> getSentFriendInviteListById(@Query("user_id") int userId);

    @GET("api/friend")
    Single<List<UserInvite>> getFirmFriendInviteListById(@Query("friend_id") int friendId);

    @GET("api/friend")
    Single<List<UserReceive>> getSentFriendReceiveListById(@Query("user_id") int userId);

    @GET("api/friend")
    Single<List<UserReceive>> getFirmFriendReceiveListById(@Query("friend_id") int friendId);

    @GET("api/friend/check")
    Single<Boolean> isFriend(@Query("friend_id") int friendId, @Query("user_id") int userId);

    @GET("api/friend/add")
    Completable addOrReceiveFriend(@Query("user_id") int userId, @Query("friend_id") int friendId);

    @GET("api/friend/remove")
    Completable removeFriend(@Query("user_id") int userId, @Query("friend_id") int friendId);

    @GET("api/write")
    Completable writeComment(@Query("user_id") int userId, @Query("place_id") int placeId, @Query("comment") String comment);

    @GET("api/comment")
    Single<List<Comment>> getCommentByPlaceId(@Query("place_id") int placeId);

    @GET("api/auto")
    Completable autoGenerate(@Query("userId") int userId, @Query("itineraryId") int itineraryId, @Query("lessWalking") boolean lessWalking,
                             @Query("fewerTransfers") boolean fewerTransfers, @Query("plan") String plan,
                             @Query("noBreakfast") boolean noBreakfast, @Query("maxBudget") String maxBudget);

    @FormUrlEncoded
    @POST("api/transportation")
    Single<DetailRoute> getTrafficDetailCall(@Field("activityIdA") int activityIdA, @Field("activityIdB") int activityIdB, @Field("departureTime") DateTime departureTime);

    @FormUrlEncoded
    @POST("api/transportation/temp")
    Single<DetailRoute> getTrafficDetailTempCall(@Field("activityIdA") int activityIdA, @Field("activityIdB") int activityIdB, @Field("departureTime") DateTime departureTime);

    @GET("api/itinerary_days/{id}/traffic")
    Single<List<DetailRoute>> getItineraryDayDetailRouteCall(@Path("id") int id);

    @FormUrlEncoded
    @POST("api/itinerary_days/{id}/activities")
    Single<Place> createActivityCall(@Path("id") int itineraryId, @Field("placeId") int placeId);

    @DELETE("api/activities/{id}")
    Completable deleteActivityByIdCall(@Path("id") int activityId);

    @FormUrlEncoded
    @PUT("api/activities/{id}")
    Completable updateActivityBudgetCall(@Path("id") int activityId, @Field("budget") double budget);

    @FormUrlEncoded
    @PUT("api/activities/{id}")
    Completable updateActivityDurationCall(@Path("id") int activityId, @Field("duration") int min);

    @FormUrlEncoded
    @PUT("api/itinerary_days/{id}")
    Completable updateItineraryDayStartTimeCall(@Path("id") int itineraryDayId, @Field("startTime") String startTime);

    @POST("api/itineraries/{id}/itinerary_days")
    Single<ItineraryDay> createItineraryDayByItineraryIdCall(@Path("id") int itineraryId);

    @FormUrlEncoded
    @PUT("api/activities/{id}")
    Completable updateActivityOrderCall(@Path("id") int activityId, @Field("prevId") int prevId, @Field("nextId") int nextId);

    @FormUrlEncoded
    @PUT("api/activities/{id}")
    Completable updateActivityOrderPrevIdCall(@Path("id") int activityId, @Field("prevId") int prevId);

    @FormUrlEncoded
    @PUT("api/activities/{id}")
    Completable updateActivityOrderNextIdCall(@Path("id") int activityId, @Field("nextId") int nextId);

    @FormUrlEncoded
    @PUT("api/activities/{id}")
    Completable updateActivityTravelModeCall(@Path("id") int activityId, @Field("travelMode") String travelMode);

    @FormUrlEncoded
    @PUT("api/itinerary_days/{id}")
    Completable updateActivityOrderCall(@Path("id") int itineraryDayId, @Field("activity_order[]") ArrayList<Integer> activityOrderList);

    @GET("api/itinerary_days/{id}/opt")
    Single<ItineraryDay> getOptItineraryDayByIdCall(@Path("id") int itineraryDayId);

    @GET("api/share")
    Completable shareItinerary(@Query("sender_id") int senderId, @Query("receiver_id") int receiverId,  @Query("itinerary_id") int itineraryId);

    @GET("api/checkIsExistSharedItinerary")
    Single<Boolean> checkIsExistSharedItinerary(@Query("receiver_id") int userId);

    @GET("api/shared")
    Single<List<ReceivedItinerary>> listReceivedItinerary(@Query("receiver_id") int userId);

    @GET("api/receive")
    Completable receiveItinerary(@Query("sharedItinerary_id") int sharedItineraryId, @Query("itinerary_id") int itineraryId, @Query("receiver_id") int userId);

    @GET("api/decline")
    Completable declineItinerary(@Query("sharedItinerary_id") int sharedItineraryId);

    @GET("/api/itineraries/{id}/opt")
    Single<Itinerary> optimizeItineraryByIdCall(@Path("id") int itineraryId);

    @GET("api/auth/logout")
    Completable logoutCall();

    @GET("api/weather")
    Single<List<WeatherDayResponse>> getWeatherReportCall();
}
