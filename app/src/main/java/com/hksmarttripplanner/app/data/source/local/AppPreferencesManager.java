package com.hksmarttripplanner.app.data.source.local;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferencesManager {
    private final static String PREF_NAME = "app";

    private final static String PREF_KEY_ACCOUNT = "account";
    private final static String PREF_KEY_PASSWORD = "password";
    private final static String PREF_KEY_TOKEN = "access_token";
    private final static String PREF_KEY_EXPIRE_TIME = "expireTime";
    private final static String PREF_KEY_USER_ID = "userId";
    private final static String PREF_KEY_CURRENCY = "currency";

    private static AppPreferencesManager INSTANCE;
    private final SharedPreferences mPref;

    private AppPreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized AppPreferencesManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new AppPreferencesManager(context);
        }
        return INSTANCE;
    }

    public boolean saveLoginInformation(String account, String password, String token , long expireTime) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(PREF_KEY_ACCOUNT, account);
        editor.putString(PREF_KEY_PASSWORD, password);
        editor.putString(PREF_KEY_TOKEN, token);
        editor.putLong(PREF_KEY_EXPIRE_TIME, expireTime);
        boolean result = editor.commit();
        return result;
    }

    public boolean saveMyUserId(int id) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putInt(PREF_KEY_USER_ID, id);
        boolean result = editor.commit();
        return result;
    }

    public boolean saveUserCurrency(String cur) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(PREF_KEY_CURRENCY, cur);
        boolean result = editor.commit();
        return result;
    }

    public long getExpireTime() {
        return mPref.getLong(PREF_KEY_EXPIRE_TIME, 0);
    }

    public String getToken() {
        return mPref.getString(PREF_KEY_TOKEN, "");
    }

    public int getMyUserId() {
        return mPref.getInt(PREF_KEY_USER_ID, 0);
    }

    public String getMyUserCurrency() {
        return mPref.getString(PREF_KEY_CURRENCY, "");
    }

    public boolean clear() {
        return mPref.edit().clear().commit();
    }
}
