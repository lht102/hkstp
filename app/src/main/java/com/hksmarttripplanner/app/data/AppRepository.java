package com.hksmarttripplanner.app.data;

import android.content.Context;

import com.hksmarttripplanner.app.data.source.SimulateSampleData;
import com.hksmarttripplanner.app.data.source.local.AppPreferencesManager;
import com.hksmarttripplanner.app.data.source.local.model.Comment;
import com.hksmarttripplanner.app.data.source.local.model.Itinerary;
import com.hksmarttripplanner.app.data.source.local.model.ItineraryDay;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.local.model.ReceivedItinerary;
import com.hksmarttripplanner.app.data.source.local.model.User;
import com.hksmarttripplanner.app.data.source.local.model.UserInvite;
import com.hksmarttripplanner.app.data.source.local.model.UserReceive;
import com.hksmarttripplanner.app.data.source.remote.APIService;
import com.hksmarttripplanner.app.data.source.remote.RetrofitClient;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.data.source.remote.api.response.LoginResponse;
import com.hksmarttripplanner.app.data.source.remote.api.response.WeatherDayResponse;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.Retrofit;

public class AppRepository {
    private static AppRepository INSTANCE;
    private APIService apiService;
    private AppPreferencesManager appPreferencesManager;

    private AppRepository(APIService apiService, AppPreferencesManager appPreferencesManager) {
        this.apiService = apiService;
        this.appPreferencesManager = appPreferencesManager;
    }

    public static AppRepository getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppRepository.class) {
                if (INSTANCE == null) {
                    AppPreferencesManager appPreferencesManager = AppPreferencesManager.getInstance(context);
                    Retrofit client = RetrofitClient.getInstance(context);
                    APIService apiService = client.create(APIService.class);
                    INSTANCE = new AppRepository(apiService, appPreferencesManager);
                }
            }
        }
        return INSTANCE;
    }

    public boolean saveLoginInformation(String account, String password, String token, long expireTime) {
        return appPreferencesManager.saveLoginInformation(account, password, token, expireTime);
    }

    public boolean saveMyUserId(int id) {
        return appPreferencesManager.saveMyUserId(id);
    }

    public boolean saveMyUserCurrency(String currency) {
        return appPreferencesManager.saveUserCurrency(currency);
    }

    public Single<LoginResponse> login(String email, String password) {
        return apiService.loginCall(email, password);
    }

    public Single<List<Itinerary>> getUserItineraries(int userId) {
        // simulate the network call and return result
//        List<Itinerary> itineraries = new ArrayList<>();
//        itineraries.add(new Itinerary(1, "https://www.telegraph.co.uk/content/dam/Travel/Cruise/worlds-most-beautiful-ports/hongkong-harbour-xlarge.jpg", "Hong Kong Plan" , new Date(2019, 1, 1), new Date(2019, 1, 3), 3567));
//        itineraries.add(new Itinerary(2, "http://static.asiawebdirect.com/m/phuket/portals/hong-kong-hotels-ws/homepage/nightlife/pagePropertiesImage/hong-kong-nightlife.jpg.jpg", "My Wonderful Plan" , new Date(2019, 6, 23), new Date(2019, 6, 27), 5123));
//        return Observable.fromIterable(itineraries).delay(1000, TimeUnit.MILLISECONDS).toList();
        return apiService.getUserItinerariesCall(userId);
    }

    public Single<Itinerary> getItineraryById(int itineraryId) {
//        List<ItineraryDay> itemList = new ArrayList<>();
//        itemList.add(new ItineraryDay(3, 1, new Date(2019, 2, 2)));
//        itemList.add(new ItineraryDay(2, 2, new Date(2019, 2, 3)));
//        return Observable.fromIterable(itemList).delay(1000, TimeUnit.MILLISECONDS).toList();
        return apiService.getItineraryByIdCall(itineraryId);
    }

    public Single<List<ItineraryDay>> getItineraryDaysByItineraryId(int itineraryId) {
        return getItineraryById(itineraryId).map(itinerary -> {
            List<ItineraryDay> itineraryDays = itinerary.getDays();
            Collections.sort(itineraryDays);
            for (int i = 0; i < itineraryDays.size(); i++) {
                ItineraryDay itineraryDay = itineraryDays.get(i);
                itineraryDay.setDay(i + 1);
            }
            return itineraryDays;
        });
    }

    public Completable deleteItineraryDayById(int itineraryDayId) {
//        return Completable.complete();

        return apiService.deleteItineraryDayByIdCall(itineraryDayId);
    }

    public Completable deleteItineraryById(int itineraryId) {
//        return Completable.complete();
        return apiService.deleteItineraryByIdCall(itineraryId);
    }

    public Single<User> getMyUserInfo() {
        return apiService.getMyUserInfoCall();
    }

    public int getMyUserId() {
        int id = appPreferencesManager.getMyUserId();
        if (id < 1) {
            throw new IllegalArgumentException("User id is null");
        }
        return id;
    }

    public String getMyCurrency() {
        String curr = appPreferencesManager.getMyUserCurrency();
        return curr;
    }

    public Single<Itinerary> createUserItineraryById(int userId, String title, double budget, String startDate, String endDate) {
        return apiService.createUserItineraryByIdCall(userId, title, budget, startDate, endDate);
    }

    public Single<List<Place>> getSearchPlacesCall(String keyword, String district, String type, String category, String budget) {
        return apiService.getSearchPlacesCall(keyword, district, type, category, budget);
    }

    public Completable addToFavorite(int userId, int placeId) {
        return apiService.addToFavorite(userId, placeId);
    }

    public Completable removeFromFavorite(int userId, int placeId) {
        return apiService.removeFromFavorite(userId, placeId);
    }

    public Single<Boolean> isFavorite(int userId, int placeId) {
        return apiService.isFavorite(userId, placeId);
    }

    public Single<ItineraryDay> getItineraryDayById(int id) {
//        ItineraryDay day = new ItineraryDay(3, 1, new Date(2019, 2, 2));
//        Place place = new Place("name", "address", "r", "des", "kowloon", "url", "98765432", 1, 4.5, 123, 321, 300, new ArrayList<Time>());
//        List<Place> places = new ArrayList<>();
//        places.add(place);
//        day.setPlaces(places);
//        return Single.just(day);
        return apiService.getItineraryDayByIdCall(id);
    }

    public  Single<List<User>> getSearchFriendResult(int userId) {
        return apiService.getSearchFriendResult(userId);
    }

    public Single<List<User>> getSentFriendListById(int userId) {
        return apiService.getSentFriendListById(userId);
    }

    public Single<List<User>> getFirmFriendListById(int friendId) {
        return apiService.getFirmFriendListById(friendId);
    }

    public Single<List<UserInvite>> getSentFriendInviteListById(int userId) {
        return apiService.getSentFriendInviteListById(userId);
    }

    public Single<List<UserInvite>> getFirmFriendInviteListById(int friendId) {
        return apiService.getFirmFriendInviteListById(friendId);
    }

    public Single<List<UserReceive>> getSentFriendReceiveListById(int userId) {
        return apiService.getSentFriendReceiveListById(userId);
    }

    public Single<List<UserReceive>> getFirmFriendReceiveListById(int friendId) {
        return apiService.getFirmFriendReceiveListById(friendId);
    }

    public Single<List<Place>> getUserFavorites(int userId) {
        List<Place> itemList = SimulateSampleData.getSampleListPlace();
//        return Observable.fromIterable(itemList).delay(1000, TimeUnit.MILLISECONDS).toList();
        return apiService.getUserFavoritesCall(userId);
    }

    public Single<Boolean> isFriend(int friendId, int userId) {
        return apiService.isFriend(friendId, userId);
    }

    public Completable addOrReceiveFriend(int userId, int friendId) {
        return apiService.addOrReceiveFriend(userId, friendId);
    }

    public Completable removeFriend(int userId, int friendId) {
        return apiService.removeFriend(userId, friendId);
    }

    public Completable writeComment(int userId, int placeId,String comment) {
        return apiService.writeComment(userId, placeId, comment);
    }

    public Single<List<Comment>> getCommentByPlaceId(int placeId) {
        return apiService.getCommentByPlaceId(placeId);
    }

    public Completable autoGenerate(int userId, int itineraryId, boolean lessWalking,
                                    boolean fewerTransfers, String plan, boolean noBreakfast, String maxBudget) {
        return apiService.autoGenerate(userId, itineraryId, lessWalking,
                fewerTransfers, plan, noBreakfast, maxBudget);
    }

    public Single<DetailRoute> getTrafficDetail(int activityIdA, int activityIdB, DateTime departureTime) {
//        return Single.just(SimulateSampleData.getSampleDetailRoute()).delay(1000, TimeUnit.MILLISECONDS);
        return apiService.getTrafficDetailCall(activityIdA, activityIdB, departureTime);
    }

    public Single<DetailRoute> getTrafficDetailTemp(int activityIdA, int activityIdB, DateTime departureTime) {
//        return Single.just(SimulateSampleData.getSampleDetailRoute()).delay(1000, TimeUnit.MILLISECONDS);
        return apiService.getTrafficDetailTempCall(activityIdA, activityIdB, departureTime);
    }
    public Single<List<DetailRoute>> getItineraryDayDetailRoute(int itineraryDayId) {
        return apiService.getItineraryDayDetailRouteCall(itineraryDayId);
    }

    public Single<Place> createActivity(int itineraryId, int placeId) {
        return apiService.createActivityCall(itineraryId, placeId);
    }

    public Completable deleteActivityById(int activityId) {
        return apiService.deleteActivityByIdCall(activityId);
    }

    public Completable updateActivityBudget(int activityId, double budget) {
        return apiService.updateActivityBudgetCall(activityId, budget);
    }

    public Completable updateActivityDuration(int activityId, int min) {
        return apiService.updateActivityDurationCall(activityId, min);
    }

    public Completable updateItineraryDayStartTime(int itineraryDayId, String startTime) {
        return apiService.updateItineraryDayStartTimeCall(itineraryDayId, startTime);
    }

    public Single<ItineraryDay> createItineraryDayByItineraryId(int itineraryId) {
        return apiService.createItineraryDayByItineraryIdCall(itineraryId);
    }

    public Completable updateActivityOrder(int activityId, int prevId, int nextId) {
        return apiService.updateActivityOrderCall(activityId, prevId, nextId);
    }

    public Completable updateActivityOrderPrevId(int activityId, int prevId) {
        return apiService.updateActivityOrderPrevIdCall(activityId, prevId);
    }

    public Completable updateActivityOrderNextId(int activityId, int nextId) {
        return apiService.updateActivityOrderNextIdCall(activityId, nextId);
    }

    public Completable updateActivityOrder(int itineraryDayId, ArrayList<Integer> activityOrderList) {
        return apiService.updateActivityOrderCall(itineraryDayId, activityOrderList);
    }

    public Completable updateActivityTravelMode(int activityId, String travelMode) {
        return apiService.updateActivityTravelModeCall(activityId, travelMode);
    }

    public Single<ItineraryDay> getOptItineraryDayById(int itineraryDayId) {
        return apiService.getOptItineraryDayByIdCall(itineraryDayId);
    }

    public Completable shareItinerary(int senderId, int receiverId, int itineraryId) {
        return  apiService.shareItinerary(senderId, receiverId, itineraryId);
    }

    public Single<Boolean> checkIsExistSharedItinerary(int userId) {
        return apiService.checkIsExistSharedItinerary(userId);
    }

    public Single<List<ReceivedItinerary>> listReceivedItinerary(int userId) {
        return apiService.listReceivedItinerary(userId);
    }

    public Completable receiveItinerary(int sharedItineraryId, int itineraryId, int userId){
        return apiService.receiveItinerary(sharedItineraryId, itineraryId, userId);
    }

    public Completable declineItinerary(int sharedItineraryId) {
        return apiService.declineItinerary(sharedItineraryId);
    }

    public Single<Itinerary> optimizeItineraryById(int itineraryId) {
        return apiService.optimizeItineraryByIdCall(itineraryId);
    }

    public Completable logout() {
        return apiService.logoutCall()
                .doOnComplete(() -> {
                    appPreferencesManager.clear();
                });
    }

    public Single<List<WeatherDayResponse>> getWeatherReport() {
        return apiService.getWeatherReportCall().map(weatherDayResponses -> {
            int j = 0;
            List<WeatherDayResponse> temp = new ArrayList<>();
            if (weatherDayResponses != null && weatherDayResponses.size() > 0) {
                temp.add(weatherDayResponses.get(0));
            }
            for (int i = 0; i < weatherDayResponses.size() - 1; i++) {
                 if (!weatherDayResponses.get(i).getDescription().equalsIgnoreCase(weatherDayResponses.get(i + 1).getDescription())) {
                     temp.add(weatherDayResponses.get(i + 1));
                 }
            }
            return temp;
        });
    }
}
