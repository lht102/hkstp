package com.hksmarttripplanner.app.data.source.local.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Hour implements Parcelable {
    @Expose
    @SerializedName("open")
    private String open;

    @Expose
    @SerializedName("close")
    private String close;

    public Hour(String open, String close) {
        this.open = open;
        this.close = close;
    }

    protected Hour(Parcel in) {
        open = in.readString();
        close = in.readString();
    }

    public static final Creator<Hour> CREATOR = new Creator<Hour>() {
        @Override
        public Hour createFromParcel(Parcel in) {
            return new Hour(in);
        }

        @Override
        public Hour[] newArray(int size) {
            return new Hour[size];
        }
    };

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(open);
        dest.writeString(close);
    }
}
