package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Step implements Parcelable {
    @Expose
    @SerializedName("distance")
    private Distance distance;

    @Expose
    @SerializedName("duration")
    private Duration duration;

    @Expose
    @SerializedName("end_location")
    private GLocation endLocation;

    @Expose
    @SerializedName("html_instructions")
    private String htmlInstructions;

    @Expose
    @SerializedName("maneuver")
    private String maneuver;

    @Expose
    @SerializedName("polyline")
    private EncodedPolyline polyline;

    @Expose
    @SerializedName("start_location")
    private GLocation startLocation;

    @Expose
    @SerializedName("transit_details")
    private TransitDetail transitDetail;

    @Expose
    @SerializedName("steps")
    private List<Step> steps;

    @Expose
    @SerializedName("travel_mode")
    private String travelMode;

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public GLocation getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(GLocation endLocation) {
        this.endLocation = endLocation;
    }

    public String getHtmlInstructions() {
        return htmlInstructions;
    }

    public void setHtmlInstructions(String htmlInstructions) {
        this.htmlInstructions = htmlInstructions;
    }

    public String getManeuver() {
        return maneuver;
    }

    public void setManeuver(String maneuver) {
        this.maneuver = maneuver;
    }

    public EncodedPolyline getPolyline() {
        return polyline;
    }

    public void setPolyline(EncodedPolyline polyline) {
        this.polyline = polyline;
    }

    public GLocation getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GLocation startLocation) {
        this.startLocation = startLocation;
    }

    public TransitDetail getTransitDetail() {
        return transitDetail;
    }

    public void setTransitDetail(TransitDetail transitDetail) {
        this.transitDetail = transitDetail;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.distance, flags);
        dest.writeParcelable(this.duration, flags);
        dest.writeParcelable(this.endLocation, flags);
        dest.writeString(this.htmlInstructions);
        dest.writeString(this.maneuver);
        dest.writeParcelable(this.polyline, flags);
        dest.writeParcelable(this.startLocation, flags);
        dest.writeParcelable(this.transitDetail, flags);
        dest.writeTypedList(this.steps);
        dest.writeString(this.travelMode);
    }

    public Step() {
    }

    protected Step(Parcel in) {
        this.distance = in.readParcelable(Distance.class.getClassLoader());
        this.duration = in.readParcelable(Duration.class.getClassLoader());
        this.endLocation = in.readParcelable(GLocation.class.getClassLoader());
        this.htmlInstructions = in.readString();
        this.maneuver = in.readString();
        this.polyline = in.readParcelable(EncodedPolyline.class.getClassLoader());
        this.startLocation = in.readParcelable(GLocation.class.getClassLoader());
        this.transitDetail = in.readParcelable(TransitDetail.class.getClassLoader());
        this.steps = in.createTypedArrayList(Step.CREATOR);
        this.travelMode = in.readString();
    }

    public static final Creator<Step> CREATOR = new Creator<Step>() {
        @Override
        public Step createFromParcel(Parcel source) {
            return new Step(source);
        }

        @Override
        public Step[] newArray(int size) {
            return new Step[size];
        }
    };
}
