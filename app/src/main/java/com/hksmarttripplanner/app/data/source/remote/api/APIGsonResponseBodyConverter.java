package com.hksmarttripplanner.app.data.source.remote.api;

import android.util.JsonReader;
import android.util.Log;

import com.bumptech.glide.load.HttpException;
import com.hksmarttripplanner.app.data.source.remote.api.response.APIResponse;

import java.io.IOException;

import javax.annotation.Nullable;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class APIGsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private Converter<ResponseBody, APIResponse<T>> converter;

    public APIGsonResponseBodyConverter(Converter<ResponseBody, APIResponse<T>> converter) {
        this.converter = converter;
    }

    @Nullable
    @Override
    public T convert(ResponseBody value) throws IOException {
        APIResponse<T> response = converter.convert(value);
        return response.getData();
    }
}
