package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Line implements Parcelable {
    @Expose
    @SerializedName("agencies")
    private List<Agency> agencies;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("short_name")
    private String shortName;

    @Expose
    @SerializedName("vehicle")
    private Vehicle vehicle;

    @Expose
    @SerializedName("num_stops")
    private int numStop;

    public List<Agency> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<Agency> agencies) {
        this.agencies = agencies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.agencies);
        dest.writeString(this.name);
        dest.writeString(this.shortName);
        dest.writeParcelable(this.vehicle, flags);
        dest.writeInt(this.numStop);
    }

    public Line() {
    }

    protected Line(Parcel in) {
        this.agencies = in.createTypedArrayList(Agency.CREATOR);
        this.name = in.readString();
        this.shortName = in.readString();
        this.vehicle = in.readParcelable(Vehicle.class.getClassLoader());
        this.numStop = in.readInt();
    }

    public static final Creator<Line> CREATOR = new Creator<Line>() {
        @Override
        public Line createFromParcel(Parcel source) {
            return new Line(source);
        }

        @Override
        public Line[] newArray(int size) {
            return new Line[size];
        }
    };
}
