package com.hksmarttripplanner.app.data.source.local.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Time implements Parcelable {
    @Expose
    @SerializedName("weekday")
    private String weekday;

    @Expose
    @SerializedName("hours")
    private List<Hour> hours;

    public Time(String weekday, List<Hour> hours) {
        this.weekday = weekday;
        this.hours = hours;
    }

    protected Time(Parcel in) {
        weekday = in.readString();
        hours = in.createTypedArrayList(Hour.CREATOR);
    }

    public static final Creator<Time> CREATOR = new Creator<Time>() {
        @Override
        public Time createFromParcel(Parcel in) {
            return new Time(in);
        }

        @Override
        public Time[] newArray(int size) {
            return new Time[size];
        }
    };

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public List<Hour> getHours() {
        return hours;
    }

    public void setHours(List<Hour> hours) {
        this.hours = hours;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(weekday);
        dest.writeList(hours);
    }
}
