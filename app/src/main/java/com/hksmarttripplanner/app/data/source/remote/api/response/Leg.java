package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Leg implements Parcelable {

    @Expose
    @SerializedName("arrival_time")
    private GTime arrivalTime;

    @Expose
    @SerializedName("departure_time")
    private GTime departureTime;

    @Expose
    @SerializedName("distance")
    private Distance distance;

    @Expose
    @SerializedName("duration")
    private Duration duration;

    @Expose
    @SerializedName("end_address")
    private String endAddress;

    @Expose
    @SerializedName("end_location")
    private GLocation endLocation;

    @Expose
    @SerializedName("start_address")
    private String startAddress;

    @Expose
    @SerializedName("start_location")
    private GLocation startLocation;

    @Expose
    @SerializedName("steps")
    private List<Step> steps;

    public GTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(GTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public GTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(GTime departureTime) {
        this.departureTime = departureTime;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public GLocation getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(GLocation endLocation) {
        this.endLocation = endLocation;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public GLocation getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(GLocation startLocation) {
        this.startLocation = startLocation;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.arrivalTime, flags);
        dest.writeParcelable(this.departureTime, flags);
        dest.writeParcelable(this.distance, flags);
        dest.writeParcelable(this.duration, flags);
        dest.writeString(this.endAddress);
        dest.writeParcelable(this.endLocation, flags);
        dest.writeString(this.startAddress);
        dest.writeParcelable(this.startLocation, flags);
        dest.writeList(this.steps);
    }

    public Leg() {
        steps = new ArrayList<>();
    }

    protected Leg(Parcel in) {
        this.arrivalTime = in.readParcelable(GTime.class.getClassLoader());
        this.departureTime = in.readParcelable(GTime.class.getClassLoader());
        this.distance = in.readParcelable(Distance.class.getClassLoader());
        this.duration = in.readParcelable(Duration.class.getClassLoader());
        this.endAddress = in.readString();
        this.endLocation = in.readParcelable(GLocation.class.getClassLoader());
        this.startAddress = in.readString();
        this.startLocation = in.readParcelable(GLocation.class.getClassLoader());
        this.steps = new ArrayList<Step>();
        in.readList(this.steps, Step.class.getClassLoader());
    }

    public static final Creator<Leg> CREATOR = new Creator<Leg>() {
        @Override
        public Leg createFromParcel(Parcel source) {
            return new Leg(source);
        }

        @Override
        public Leg[] newArray(int size) {
            return new Leg[size];
        }
    };
}
