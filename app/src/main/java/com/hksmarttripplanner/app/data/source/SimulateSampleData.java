package com.hksmarttripplanner.app.data.source;

import com.hksmarttripplanner.app.data.source.local.model.Budget;
import com.hksmarttripplanner.app.data.source.local.model.Hour;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.local.model.Time;
import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.data.source.remote.api.response.Distance;
import com.hksmarttripplanner.app.data.source.remote.api.response.Duration;
import com.hksmarttripplanner.app.data.source.remote.api.response.Leg;
import com.hksmarttripplanner.app.data.source.remote.api.response.Route;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;

import java.util.ArrayList;
import java.util.List;

public class SimulateSampleData {
    public static List<Place> getSampleListPlace() {
        Hour hour1 = new Hour("09:00:00", "20:00:00");
        Hour hour2 = new Hour("10:00:00", "21:00:00");
        Hour hour3 = new Hour("11:00:00", "22:00:00");
        Hour hour4 = new Hour("08:00:00", "20:00:00");
        Hour hour5 = new Hour("09:00:00", "17:00:00");

        List<Hour> hours1 = new ArrayList<>();
        hours1.add(hour1);

        List<Hour> hours2 = new ArrayList<>();
        hours2.add(hour3);

        Time time1 = new Time("Mon", hours1);
        Time time2 = new Time("Feb", hours2);
        Time time3 = new Time("Wed", hours2);
        Time time4 = new Time("Thu", hours2);
        Time time5 = new Time("Fri", hours1);

        List<Time> times1 = new ArrayList<>();
        times1.add(time1);
        times1.add(time2);
        times1.add(time3);
        times1.add(time4);
        times1.add(time5);

        Place place1 = new Place();
        place1.setId(1);
        place1.setName("Place ABC");
        place1.setType("Restaurant");
        place1.setAddress("This is a address");
        place1.setDescription("This is a description");
        place1.setDistrict("Kowloon");
        place1.setImage("url");
        place1.setPhone("98765432");
        place1.setLatitude(23.3421);
        place1.setLongitude(35.3435);
//        place1.setBudget(5500);
        place1.setTimes(times1);

        Place place2 = new Place();
        place2.setId(2);
        place2.setName("Place CBA");
        place2.setType("Attraction");
        place2.setAddress("This is a address 2");
        place2.setDescription("This is a description 2");
        place2.setDistrict("Sha Tin");
        place2.setImage("url2");
        place2.setPhone("34312567");
        place2.setLatitude(26.3421);
        place2.setLongitude(37.3435);
//        place2.setBudget(7000);
        place2.setTimes(times1);

        List<Place> places = new ArrayList<>();
        places.add(place1);
        places.add(place2);

        return places;
    }

    public static DetailRoute getSampleDetailRoute() {
        DetailRoute detailRoute = new DetailRoute();
        Budget budget = new Budget();
        budget.setAmount(10);
        budget.setCurrency("USD");
        Distance distance = new Distance();
        distance.setText("1 km");
        distance.setValue(1000);
        Duration duration = new Duration();
        duration.setText("1 hours 38 mins");
        duration.setValue(5894);
        Leg leg = new Leg();
        Step step = new Step();
        Route route = new Route();
        List<Route> routes = new ArrayList<>();
        List<Step> steps = new ArrayList<>();
        List<Leg> legs = new ArrayList<>();
        steps.add(step);
        legs.add(leg);
        routes.add(route);
        leg.setSteps(steps);
        leg.setDuration(duration);
        leg.setDistance(distance);
        route.setLegs(legs);
        detailRoute.setRoutes(routes);
        detailRoute.setDisplayfee(budget);
        detailRoute.setFee(77.7);
        detailRoute.setTime(100);
        return detailRoute;
    }
}
