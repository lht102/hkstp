package com.hksmarttripplanner.app.data.source.local.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceivedItinerary {
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("itinerary")
    private int itinerary;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("sender")
    private String sender;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItinerary() {
        return itinerary;
    }

    public void setItinerary(int itinerary) {
        this.itinerary = itinerary;
    }
}
