package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransitDetail implements Parcelable {
    @Expose
    @SerializedName("arrival_stop")
    private GStop arrivalStop;

    @Expose
    @SerializedName("arrival_time")
    private GTime arrivalTime;

    @Expose
    @SerializedName("departure_stop")
    private GStop departureStop;

    @Expose
    @SerializedName("departure_time")
    private GTime departureTime;

    @Expose
    @SerializedName("headsign")
    private String headsign;

    @Expose
    @SerializedName("headway")
    private long headway;

    @Expose
    @SerializedName("line")
    private Line line;

    @Expose
    @SerializedName("num_stops")
    private int numStop;

    public GStop getArrivalStop() {
        return arrivalStop;
    }

    public void setArrivalStop(GStop arrivalStop) {
        this.arrivalStop = arrivalStop;
    }

    public GTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(GTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public GStop getDepartureStop() {
        return departureStop;
    }

    public void setDepartureStop(GStop departureStop) {
        this.departureStop = departureStop;
    }

    public GTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(GTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

    public long getHeadway() {
        return headway;
    }

    public void setHeadway(long headway) {
        this.headway = headway;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public int getNumStop() {
        return numStop;
    }

    public void setNumStop(int numStop) {
        this.numStop = numStop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.arrivalStop, flags);
        dest.writeParcelable(this.arrivalTime, flags);
        dest.writeParcelable(this.departureStop, flags);
        dest.writeParcelable(this.departureTime, flags);
        dest.writeString(this.headsign);
        dest.writeLong(this.headway);
        dest.writeParcelable(this.line, flags);
        dest.writeInt(this.numStop);
    }

    public TransitDetail() {
    }

    protected TransitDetail(Parcel in) {
        this.arrivalStop = in.readParcelable(GStop.class.getClassLoader());
        this.arrivalTime = in.readParcelable(GTime.class.getClassLoader());
        this.departureStop = in.readParcelable(GStop.class.getClassLoader());
        this.departureTime = in.readParcelable(GTime.class.getClassLoader());
        this.headsign = in.readString();
        this.headway = in.readLong();
        this.line = in.readParcelable(Line.class.getClassLoader());
        this.numStop = in.readInt();
    }

    public static final Creator<TransitDetail> CREATOR = new Creator<TransitDetail>() {
        @Override
        public TransitDetail createFromParcel(Parcel source) {
            return new TransitDetail(source);
        }

        @Override
        public TransitDetail[] newArray(int size) {
            return new TransitDetail[size];
        }
    };
}
