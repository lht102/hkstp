package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Route implements Parcelable {
    @Expose
    @SerializedName("bounds")
    private Bound bound;

    @Expose
    @SerializedName("copyrights")
    private String copyrights;

    @Expose
    @SerializedName("legs")
    private List<Leg> legs;

    @Expose
    @SerializedName("overview_polyline")
    private EncodedPolyline overviewPolyline;

    @Expose
    @SerializedName("summary")
    private String summary;

    @Expose
    @SerializedName("warnings")
    private List<String> warnings;

    @Expose
    @SerializedName("waypoint_order")
    private List<Integer> waypointOrder;

    public Bound getBound() {
        return bound;
    }

    public void setBound(Bound bound) {
        this.bound = bound;
    }

    public String getCopyrights() {
        return copyrights;
    }

    public void setCopyrights(String copyrights) {
        this.copyrights = copyrights;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public EncodedPolyline getOverviewPolyline() {
        return overviewPolyline;
    }

    public void setOverviewPolyline(EncodedPolyline overviewPolyline) {
        this.overviewPolyline = overviewPolyline;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }

    public List<Integer> getWaypointOrder() {
        return waypointOrder;
    }

    public void setWaypointOrder(List<Integer> waypointOrder) {
        this.waypointOrder = waypointOrder;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.bound, flags);
        dest.writeString(this.copyrights);
        dest.writeList(this.legs);
        dest.writeParcelable(this.overviewPolyline, flags);
        dest.writeString(this.summary);
        dest.writeStringList(this.warnings);
        dest.writeList(this.waypointOrder);
    }

    public Route() {
        legs = new ArrayList<>();
        warnings = new ArrayList<>();
        waypointOrder = new ArrayList<>();
    }

    protected Route(Parcel in) {
        this.bound = in.readParcelable(Bound.class.getClassLoader());
        this.copyrights = in.readString();
        this.legs = new ArrayList<Leg>();
        in.readList(this.legs, Leg.class.getClassLoader());
        this.overviewPolyline = in.readParcelable(EncodedPolyline.class.getClassLoader());
        this.summary = in.readString();
        this.warnings = in.createStringArrayList();
        this.waypointOrder = new ArrayList<Integer>();
        in.readList(this.waypointOrder, Integer.class.getClassLoader());
    }

    public static final Creator<Route> CREATOR = new Creator<Route>() {
        @Override
        public Route createFromParcel(Parcel source) {
            return new Route(source);
        }

        @Override
        public Route[] newArray(int size) {
            return new Route[size];
        }
    };
}
