package com.hksmarttripplanner.app.data.source.remote.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.hksmarttripplanner.app.data.source.local.AppPreferencesManager;

import org.checkerframework.checker.units.qual.A;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {
    private Context context;
    private AppPreferencesManager appPreferencesManager;

    public TokenInterceptor(Context context) {
        this.context = context;
        appPreferencesManager = AppPreferencesManager.getInstance(context);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder requestBuilder = chain.request().newBuilder();
        requestBuilder.addHeader("Accept", "application/json");

        String token = appPreferencesManager.getToken();
        long expireTime = appPreferencesManager.getExpireTime();
        Calendar calendar = Calendar.getInstance();
        Date nowDate = calendar.getTime();
        calendar.setTimeInMillis(expireTime);
        Date expireDate = calendar.getTime();

        int result = nowDate.compareTo(expireDate);

        if (result == -1) {
            // refresh token
            // ...
            String newToken = "newToken";
            Request request = requestBuilder
                    .header("Authorization", newToken)
                    .build();
        } else if (token.isEmpty()) {
            return chain.proceed(requestBuilder.build());
        }

        return chain.proceed(requestBuilder.header("Authorization", token)
                .build()).newBuilder()
                .build();
    }
}
