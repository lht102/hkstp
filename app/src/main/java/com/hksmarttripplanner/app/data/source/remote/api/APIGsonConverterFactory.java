package com.hksmarttripplanner.app.data.source.remote.api;

import com.hksmarttripplanner.app.data.source.remote.api.response.APIResponse;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIGsonConverterFactory extends Converter.Factory {
    private GsonConverterFactory factory;

    public APIGsonConverterFactory(GsonConverterFactory factory) {
        this.factory = factory;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        Type wrappedType = new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[] { type };
            }

            @Override
            public Type getRawType() {
                return APIResponse.class;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }
        };
        Converter<ResponseBody, ?> gsonConverter = factory.responseBodyConverter(wrappedType, annotations, retrofit);
        return new APIGsonResponseBodyConverter(gsonConverter);
    }
}
