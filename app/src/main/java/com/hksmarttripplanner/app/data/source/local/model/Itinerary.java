package com.hksmarttripplanner.app.data.source.local.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.List;

public class Itinerary {
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("imagePath")
    private String imagePath;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("startDate")
    private DateTime startDate;

    @Expose
    @SerializedName("endDate")
    private DateTime endDate;

    @Expose
    @SerializedName("budget")
    private Budget budget;

    @Expose
    @SerializedName("itinerary_days")
    private List<ItineraryDay> days;

    public Itinerary() {}

    public Itinerary(int id, String imagePath, String title, DateTime startDate, DateTime endDate, Budget budget) {
        this.id = id;
        this.imagePath = imagePath;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.budget = budget;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public List<ItineraryDay> getDays() {
        return days;
    }

    public void setDays(List<ItineraryDay> days) {
        this.days = days;
    }
}
