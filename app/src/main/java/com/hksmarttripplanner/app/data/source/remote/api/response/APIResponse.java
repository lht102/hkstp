package com.hksmarttripplanner.app.data.source.remote.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIResponse<T> {
    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("data")
    @Expose
    private T data;

    @Expose
    @SerializedName("error")
    private APIError error;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public APIError getError() {
        return error;
    }

    public void setError(APIError error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
