package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GStop implements Parcelable {
    @Expose
    @SerializedName("location")
    private GLocation location;

    @Expose
    @SerializedName("name")
    private String name;

    public GLocation getLocation() {
        return location;
    }

    public void setLocation(GLocation location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.location, flags);
        dest.writeString(this.name);
    }

    public GStop() {
    }

    protected GStop(Parcel in) {
        this.location = in.readParcelable(GLocation.class.getClassLoader());
        this.name = in.readString();
    }

    public static final Creator<GStop> CREATOR = new Creator<GStop>() {
        @Override
        public GStop createFromParcel(Parcel source) {
            return new GStop(source);
        }

        @Override
        public GStop[] newArray(int size) {
            return new GStop[size];
        }
    };
}
