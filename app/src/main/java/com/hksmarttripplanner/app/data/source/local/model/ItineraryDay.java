package com.hksmarttripplanner.app.data.source.local.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.List;

public class ItineraryDay implements Comparable<ItineraryDay> {
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("day")
    private int day;

    @Expose
    @SerializedName("date")
    private DateTime date;

    @Expose
    @SerializedName("startTime")
    private LocalTime startTime;

    @Expose
    @SerializedName("places")
    private List<Place> places;

    public ItineraryDay() {
    }

    public ItineraryDay(int id, int day, DateTime date) {
        this.id = id;
        this.day = day;
        this.date = date;
//        places = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    @Override
    public int compareTo(ItineraryDay other) {
        return Integer.valueOf(day).compareTo(other.day);
    }
}
