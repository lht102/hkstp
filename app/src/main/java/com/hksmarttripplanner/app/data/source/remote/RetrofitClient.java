package com.hksmarttripplanner.app.data.source.remote;

import android.content.Context;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hksmarttripplanner.app.data.source.remote.api.APIGsonConverterFactory;
import com.hksmarttripplanner.app.data.source.remote.api.TokenAuthenticator;
import com.hksmarttripplanner.app.data.source.remote.api.TokenInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;

    private RetrofitClient() {
    }

    public static  Retrofit getInstance(Context context) {
        if (retrofit == null) {
            TokenAuthenticator tokenAuthenticator = new TokenAuthenticator();

            TokenInterceptor tokenInterceptor = new TokenInterceptor(context);

//            Dispatcher dispatcher = new Dispatcher();
//            dispatcher.setMaxRequests(1);

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .authenticator(tokenAuthenticator)
                    .addInterceptor(tokenInterceptor)
                    .addInterceptor(logging)
//                    .dispatcher(dispatcher)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            GsonBuilder builder = new GsonBuilder()
                    .setLenient()
                    .setDateFormat("yyyy-MM-dd");

            Converters.registerLocalTime(builder);
            Gson gson = Converters.registerDateTime(builder).create();

            retrofit = new Retrofit.Builder()
                    .baseUrl("http://10.0.2.2:8000")
                    .addConverterFactory(new APIGsonConverterFactory(GsonConverterFactory.create(gson)))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
}
