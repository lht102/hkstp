package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GTime implements Parcelable {
    @Expose
    @SerializedName("text")
    private String text;

    @Expose
    @SerializedName("time_zone")
    private String timeZone;

    @Expose
    @SerializedName("value")
    private long value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.text);
        dest.writeString(this.timeZone);
        dest.writeLong(this.value);
    }

    public GTime() {
    }

    protected GTime(Parcel in) {
        this.text = in.readString();
        this.timeZone = in.readString();
        this.value = in.readLong();
    }

    public static final Creator<GTime> CREATOR = new Creator<GTime>() {
        @Override
        public GTime createFromParcel(Parcel source) {
            return new GTime(source);
        }

        @Override
        public GTime[] newArray(int size) {
            return new GTime[size];
        }
    };
}
