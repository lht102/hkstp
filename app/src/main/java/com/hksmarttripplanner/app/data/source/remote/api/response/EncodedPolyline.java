package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EncodedPolyline implements Parcelable {

    @Expose
    @SerializedName("points")
    private String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.points);
    }

    public EncodedPolyline() {
    }

    protected EncodedPolyline(Parcel in) {
        this.points = in.readString();
    }

    public static final Creator<EncodedPolyline> CREATOR = new Creator<EncodedPolyline>() {
        @Override
        public EncodedPolyline createFromParcel(Parcel source) {
            return new EncodedPolyline(source);
        }

        @Override
        public EncodedPolyline[] newArray(int size) {
            return new EncodedPolyline[size];
        }
    };
}
