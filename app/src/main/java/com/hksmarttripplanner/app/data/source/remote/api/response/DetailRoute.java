package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hksmarttripplanner.app.data.source.local.model.Budget;

import java.util.ArrayList;
import java.util.List;

public class DetailRoute implements Parcelable {
    @Expose
    @SerializedName("fee")
    private double fee;

    @Expose
    @SerializedName("displayFee")
    private Budget displayfee;

    @Expose
    @SerializedName("time")
    private long time;

    @Expose
    @SerializedName("routes")
    private List<Route> routes;

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public Budget getDisplayfee() {
        return displayfee;
    }

    public void setDisplayfee(Budget displayfee) {
        this.displayfee = displayfee;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.fee);
        dest.writeParcelable(this.displayfee, flags);
        dest.writeLong(this.time);
        dest.writeTypedList(this.routes);
    }

    public DetailRoute() {
    }

    protected DetailRoute(Parcel in) {
        this.fee = in.readDouble();
        this.displayfee = in.readParcelable(Budget.class.getClassLoader());
        this.time = in.readLong();
        this.routes = in.createTypedArrayList(Route.CREATOR);
    }

    public static final Creator<DetailRoute> CREATOR = new Creator<DetailRoute>() {
        @Override
        public DetailRoute createFromParcel(Parcel source) {
            return new DetailRoute(source);
        }

        @Override
        public DetailRoute[] newArray(int size) {
            return new DetailRoute[size];
        }
    };
}
