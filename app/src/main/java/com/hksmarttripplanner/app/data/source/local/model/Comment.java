package com.hksmarttripplanner.app.data.source.local.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("rating")
    private int rating;

    @Expose
    @SerializedName("comment")
    private String comment;

    @Expose
    @SerializedName("user_id")
    private int userId;

    @Expose
    @SerializedName("place_id")
    private int placeId;

    @Expose
    @SerializedName("created_at")
    private String created_at;

    @Expose
    @SerializedName("email")
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

