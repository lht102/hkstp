package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

public class WeatherDayResponse implements Parcelable {
    @Expose
    @SerializedName("day")
    private String day;

    @Expose
    @SerializedName("weather")
    private String weather;

    @Expose
    @SerializedName("description")
    private String description;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.day);
        dest.writeString(this.weather);
        dest.writeString(this.description);
    }

    public WeatherDayResponse() {
    }

    protected WeatherDayResponse(Parcel in) {
        this.day = in.readString();
        this.weather = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<WeatherDayResponse> CREATOR = new Parcelable.Creator<WeatherDayResponse>() {
        @Override
        public WeatherDayResponse createFromParcel(Parcel source) {
            return new WeatherDayResponse(source);
        }

        @Override
        public WeatherDayResponse[] newArray(int size) {
            return new WeatherDayResponse[size];
        }
    };
}
