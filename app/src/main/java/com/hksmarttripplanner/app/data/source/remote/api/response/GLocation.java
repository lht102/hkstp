package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GLocation implements Parcelable {
    @Expose
    @SerializedName("lat")
    private double latitude;

    @Expose
    @SerializedName("lng")
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    public GLocation() {
    }

    protected GLocation(Parcel in) {
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public static final Parcelable.Creator<GLocation> CREATOR = new Parcelable.Creator<GLocation>() {
        @Override
        public GLocation createFromParcel(Parcel source) {
            return new GLocation(source);
        }

        @Override
        public GLocation[] newArray(int size) {
            return new GLocation[size];
        }
    };
}
