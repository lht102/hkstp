package com.hksmarttripplanner.app.data.source.remote.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bound implements Parcelable {

    @Expose
    @SerializedName("northeast")
    private GLocation northeast;

    @Expose
    @SerializedName("southwest")
    private GLocation southwest;

    public GLocation getNortheast() {
        return northeast;
    }

    public void setNortheast(GLocation northeast) {
        this.northeast = northeast;
    }

    public GLocation getSouthwest() {
        return southwest;
    }

    public void setSouthwest(GLocation southwest) {
        this.southwest = southwest;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.northeast, flags);
        dest.writeParcelable(this.southwest, flags);
    }

    public Bound() {
    }

    protected Bound(Parcel in) {
        this.northeast = in.readParcelable(GLocation.class.getClassLoader());
        this.southwest = in.readParcelable(GLocation.class.getClassLoader());
    }

    public static final Creator<Bound> CREATOR = new Creator<Bound>() {
        @Override
        public Bound createFromParcel(Parcel source) {
            return new Bound(source);
        }

        @Override
        public Bound[] newArray(int size) {
            return new Bound[size];
        }
    };
}
