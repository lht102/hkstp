package com.hksmarttripplanner.app.utils;

public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();
}
