package com.hksmarttripplanner.app.utils;

import android.support.v7.widget.RecyclerView;

public interface ItemTouchHelperAdapter {
    boolean onItemMove(RecyclerView.ViewHolder from, RecyclerView.ViewHolder to, int fromPosition, int toPosition);

    void onItemDismiss(int position);

    void clearView(RecyclerView.ViewHolder viewHolder, int selectedPos, int endPos);
}
