package com.hksmarttripplanner.app.utils;

import android.graphics.Color;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class MyColorUtils {
    private static final int[] colorList = new int[] {
            Color.BLACK, Color.BLUE, Color.CYAN, Color.GRAY, Color.GREEN, Color.MAGENTA, Color.RED
    };

    private static final float[] bitmapColor = new float[] {
            BitmapDescriptorFactory.HUE_AZURE, BitmapDescriptorFactory.HUE_BLUE, BitmapDescriptorFactory.HUE_CYAN,
            BitmapDescriptorFactory.HUE_GREEN, BitmapDescriptorFactory.HUE_MAGENTA, BitmapDescriptorFactory.HUE_ORANGE,
            BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_ROSE, BitmapDescriptorFactory.HUE_VIOLET, BitmapDescriptorFactory.HUE_YELLOW
    };

    public static int[] getMyColorList() {
        return colorList;
    }

    public static float[] getMyBitmapColorList() {
        return bitmapColor;
    }
}
