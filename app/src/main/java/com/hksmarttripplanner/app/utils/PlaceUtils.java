package com.hksmarttripplanner.app.utils;

import com.hksmarttripplanner.app.data.source.local.model.Hour;
import com.hksmarttripplanner.app.data.source.local.model.Place;
import com.hksmarttripplanner.app.data.source.local.model.Time;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

public class PlaceUtils {
    public static boolean checkWithinOpeningTime(Place place, DateTime dateTime) {
        List<Time> times = place.getTimes();
        if (times == null || times.size() == 0) return true;

        boolean flag = false;
        DateTimeFormatter fmt = DateTimeFormat.forPattern("EEEE"); // use 'E' for short abbreviation (Mon, Tues, etc)
        String weekdayStr = fmt.print(dateTime).substring(0, 3);
        for (int i = 0; i < times.size(); i++) {
            Time time = times.get(i);
            if (time.getWeekday().equalsIgnoreCase(weekdayStr)) {
                List<Hour> hours = time.getHours();
                for (int j = 0; j < hours.size(); j++) {
                    Hour hour = hours.get(j);
                    LocalTime open = LocalTime.parse(hour.getOpen());
                    LocalTime close = LocalTime.parse(hour.getClose());
                    if (MyDateUtils.isWithinInterval(open, close, dateTime.toLocalTime())) {
                        flag = true;
                    }
                }
            }
        }
        return flag;
    }
}
