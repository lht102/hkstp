package com.hksmarttripplanner.app.utils;

import com.hksmarttripplanner.app.data.source.remote.api.response.DetailRoute;
import com.hksmarttripplanner.app.data.source.remote.api.response.Step;

import java.util.List;

public class DetailRouteUtils {
    public static boolean isRouteExist(DetailRoute detailRoute) {
        return detailRoute != null && detailRoute.getRoutes() != null && detailRoute.getRoutes().size() > 0;
    }

    public static long getDetailRouteDuration(DetailRoute detailRoute) {
        long duration = 0;
        if (isRouteExist(detailRoute)) {
            duration = detailRoute.getRoutes().get(0).getLegs().get(0).getDuration().getValue();
        }
        return duration;
    }

    public static boolean isTransitModeExist(DetailRoute detailRoute) throws NullPointerException {
        List<Step> steps = detailRoute.getRoutes().get(0).getLegs().get(0).getSteps();
        for (int i = 0; i < steps.size(); i++) {
            Step step = steps.get(i);
            if (step.getTravelMode().equalsIgnoreCase("transit")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isOnlyWalking(DetailRoute detailRoute) {
        try {
            List<Step> steps = detailRoute.getRoutes().get(0).getLegs().get(0).getSteps();
            for (int i = 0; i < steps.size(); i++) {
                Step step = steps.get(i);
                if (!"walking".equalsIgnoreCase(step.getTravelMode())) {
                    return false;
                }
                List<Step> steps1 = step.getSteps();
                for (Step step1 : steps1) {
                    if ("ferry".equalsIgnoreCase(step1.getManeuver())) {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
