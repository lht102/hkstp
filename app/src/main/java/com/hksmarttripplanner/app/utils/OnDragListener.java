package com.hksmarttripplanner.app.utils;

import android.support.v7.widget.RecyclerView;

public interface OnDragListener {

    void onStartDrag(RecyclerView.ViewHolder viewHolder);

    void onEndDrag(RecyclerView.ViewHolder viewHolder);
}
