package com.hksmarttripplanner.app.utils;

import android.support.v4.app.FragmentManager;

public class FragmentUtils {
    public static void popBackStackUntilTagExclusive(FragmentManager fm, String tag) {
        for (int i = fm.getBackStackEntryCount() - 2; i > 0; i--) {
            if (!tag.equalsIgnoreCase(fm.getBackStackEntryAt(i + 1).getName())) {
                fm.popBackStack();
            }
            else
            {
                break;
            }
        }
    }
}
