package com.hksmarttripplanner.app.utils;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class MyDateUtils {

    public static String getFormattedDate(DateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        return formatter.print(dateTime);
    }

    public static DateTime roundMinute(DateTime dateTime) {
        if (dateTime.getSecondOfMinute() > 30) {
            return dateTime.plusMinutes(1).withSecondOfMinute(0);
        } else {
            return dateTime.withSecondOfMinute(0);
        }
    }

    public static boolean isWithinInterval(LocalTime start, LocalTime end, LocalTime time) {
        if (start.isAfter(end)) {
            // Return true if the time is after (or at) start, *or* it's before end
            return time.compareTo(start) >= 0 ||
                    time.compareTo(end) < 0;
        } else {
            return start.compareTo(time) <= 0 &&
                    time.compareTo(end) < 0;
        }
    }
}
